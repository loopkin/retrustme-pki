#!/usr/bin/perl


my $TYPE='__DIST_TYPE__';
my $tmpdir='.';
my $tmpfile="/tmp/NsPKITmpInstall.tmp";
my $tsaonly=0;
if ($ARGV[0] eq '--tsa-only') {
	$tsaonly=1;
}
my $installdir='/opt/rtmpki';

system("cp index.cgi $installdir/") unless ($tsaonly);
system("cp tsa.cgi $installdir/");
system("cp crmp.cgi $installdir/") unless ($tsaonly);
system("cp acme.cgi $installdir/") unless ($tsaonly);
system("cp pkiclient.exe $installdir/") unless ($tsaonly);
system("cp EnrollmentClientInstaller.exe $installdir/public/") unless ($tsaonly);
chmod(0755,"$installdir/index.cgi") unless ($tsaonly);
chmod(0755,"$installdir/tsa.cgi");
chmod(0755,"$installdir/crmp.cgi") unless ($tsaonly);
system("cp $tmpdir/lib/* $installdir/lib/NikoSoft/PKI/");
system("cp $tmpdir/slib/* $installdir/lib/NikoSoft/Service/") if ($TYPE eq 'enterprise');
system("cp $tmpdir/i18n/* $installdir/lib/NikoSoft/I18N/");
system("cp $tmpdir/bin/* $installdir/bin/");
system("cp $tmpdir/www/inc/* $installdir/www/inc/");
