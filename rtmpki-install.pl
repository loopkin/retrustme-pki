#!/usr/bin/perl

BEGIN {
	print "Welcome to RTPKI installation script.\n";
	print "\nWARNING: this script should be launched as root\n\n" unless ($ENV{'USER'} eq 'root');
	if (-f "/etc/redhat-release") {
		print "Detected RedHat/CentOS, trying to install dependencies\n";
		system("yum install -y httpd perl-Time-Piece perl-Config-Tiny perl-DBD-SQLite perl-CGI perl-Convert-ASN1 perl-Text-Unidecode perl-Crypt-OpenSSL-RSA perl-Digest-SHA perl-JSON perl-libwww-perl perl-Module-Runtime sqlite postfix dialog ntp libtool-ltdl pcsc-lite system-config-firewall-tui mod_ssl yum-cron sudo nmap");
	}
	if (-f "/etc/debian_version") {
		print "Detected Debian/Ubuntu, trying to install dependencies\n";
		system("apt-get -y install apache2 libtime-piece-perl libconfig-tiny-perl libjson-perl libconvert-asn1-perl libdbd-sqlite3-perl dialog perl-modules sqlite3 nmap");
	}
}

# Following "use" aren't needed per se, but they will blip upon missing dependency
# This is useful when installing on non-RedHat/CentOS systems
use Config::Tiny;
use DBI;
use DBD::SQLite;
use Sys::Syslog;
use Time::Piece;

open LOG, ">>/var/log/rtmpki-install.log";

my $TYPE='__DIST_TYPE__';
my $tmpdir='.';
my $tmpfile="/tmp/RTPKITmpInstall.tmp";
my $tsaonly=0;
my $rhversion=6;
if (-f "/etc/redhat-release") {
	$rhversion=`cat /etc/redhat-release`;
	$rhversion=~s/^.*release ([0-9]+).*$/$1/;
	$rhversion=~s/\n//;
	system("yum install -y perl-DBD-Pg postgresql-server") if ($TYPE eq 'enterprise');
	system("yum install -y perl-Sys-Syslog") if ($rhversion == 7);
}

unless (-d "$tmpdir/lib") {
	print "Could not find installation packages. Exiting.\n";
	exit;
}

if ($ARGV[0] eq '--tsa-only') {
	$tsaonly=1;
	print LOG "Installing only TSA.\n";
}

my $default_scsecret='000000000000000000000000000000000000000000000000';
my $new_scsecret='';
for (my $i=0;$i<48;$i++) {
	$new_scsecret.=''.int(rand(9));
}

system('dialog --title "RTPKI Installation" --no-cancel --msgbox "\nWelcome to the RTPKI Installation system.\n\nPlease setup postfix and ntp correctly: postfix should be able to send/relay emails, and ntp should be able to synchronize to NTP source.\n\nYou will now be asked a few questions to complete the installation of RTPKI." 20 61');

my $installdir='/opt/rtmpki';

my $adminpwd="";
my $auth_mode='PWD';
system('dialog --title "Administrator Password" --insecure --no-cancel --visit-items --separator "|" --trim  --passwordform "\nPlease enter the password for the PKI top level administrator, aka. admin.\nMinimal length is 6 characters, be sure to use both Latin letters and numbers.\n\nIf you do not enter a password, certificate based authentication will be used for operators.\n\n" 20 61 2 "Admin Password:" 1 1 "" 1 20 16 0 "Confirm Password:" 2 1 "" 2 20 16 0 2>'.$tmpfile);
$adminpwd=`cat $tmpfile`;
unlink($tmpfile);
my @adminpwds=split(/\|/,$adminpwd);
if ($adminpwds[0] eq '') {
	$auth_mode='CERT';
} elsif ($adminpwds[0] eq $adminpwds[1]) {
	if (($adminpwds[0]=~m/[a-zA-Z0-9\@\.\_\-\+]{6,16}/) && ($adminpwds[0]=~m/[a-z]/i) && ($adminpwds[0]=~m/[0-9]/i)) {
		$adminpwd=$adminpwds[0];
	} else {
		system('dialog --title "Error" --msgbox "\nPasswords do not match or password is too short or too weak\n" 10 61');
		print LOG "\nPasswords do not match or password is too short or too weak\n";
		exit;
	}
} else {
	system('dialog --title "Error" --msgbox "\nPasswords do not match or password is too short or too weak\n" 10 61');
	print LOG "\nPasswords do not match or password is too short or too weak\n";
	exit;
}

my $hostname=`hostname --fqdn`;
$hostname=~s/\n//g;
my $hostwarn='';
$hostwarn="WARNING: the current hostname is '$hostname'. You NEED to put a 'real' FQDN, like 'pki.example.com' and have a working DNS or /etc/hosts for the PKI to work properly" if ($hostname=~ m/localhost/);
system('dialog --title "PKI Hostname" --no-cancel --inputbox "\nPlease enter the hostname of the PKI server.\n'.$hostwarn.'\n" 20 61 "'.$hostname.'" 2>'.$tmpfile);
$hostname=`cat $tmpfile`;
unlink($tmpfile);

my $apacheuser='apache';
my $apacheuserguess=`grep www-data /etc/passwd 2>/dev/null`;
$apacheuser='www-data' if (length($apacheuserguess)>1);

my $apacheconf='/etc/httpd';
$apacheconf='/etc/apache2' if (-d '/etc/apache2');

my $cryptosystem='rsa';
my $hardware='"rsaetoken" "RSA Cryptosystem with SafeNet eToken" off "rsaps" "RSA with SafeNet ProtectServer HSM" off "rsaluna" "RSA with SafeNet Luna HSM" off';
system('dialog --title "Choose you Cryptosystem" --no-cancel --radiolist "\nChoose the cryptographic system you wish to use for the PKI.\nIf unsure, select the default (rsa).\n\n" 20 61 6 "rsa" "RSA Cryptosystem" ON "gost" "GOST Russian Cryptosystem" off "ecdsa" "ECDSA Cryptosystem" off '.$hardware.' 2>'.$tmpfile);
$cryptosystem=`cat $tmpfile`;
unlink($tmpfile);

my $p11pin='unused';
my $p11slot='0';
if (($cryptosystem eq 'rsaetoken')||($cryptosystem eq 'rsaps')||($cryptosystem eq 'rsaluna')) {
	system('dialog --title "HSM PKCS11 Password and SlotPassword" --insecure --no-cancel --visit-items --separator "|" --trim  --form "\nPlease enter the PKCS11 User PIN and slot number of the connected SafeNet device.\n" 20 61 2 "Slot Number:" 1 1 "" 1 20 24 0 "Password:" 2 1 "" 2 20 24 0 2>'.$tmpfile);
	my $p11cred=`cat $tmpfile`;
	unlink($tmpfile);
	($p11slot, $p11pin)=split(/\|/,$p11cred);
	# PTK v 5
	if ( -d "/opt/safenet/protecttoolkit5/ptk" ) {
		system("rm -f /opt/PTK");
		system("ln -s /opt/safenet/protecttoolkit5/ptk /opt/PTK");
	}
	system("echo '/opt/PTK/lib' > /etc/ld.so.conf.d/safenet-ps.conf");
	system("ldconfig");
}

print LOG "\nInstalling..";
mkdir("$installdir",0755);
mkdir("$installdir/CA",0750);
system("chown $apacheuser $installdir/CA");
mkdir("$installdir/CA/private",0750);
system("chown $apacheuser $installdir/CA/private");
mkdir("$installdir/tmp",0750);
system("chown $apacheuser $installdir/tmp");
mkdir("$installdir/public",0755);
system("chown $apacheuser $installdir/public");
mkdir("$installdir/lib",0755);
mkdir("$installdir/lib/NikoSoft",0755);
mkdir("$installdir/lib/NikoSoft/PKI",0755);
mkdir("$installdir/lib/NikoSoft/I18N",0755);
mkdir("$installdir/lib/NikoSoft/Service",0755);
mkdir("$installdir/lib/JSON",0755);
mkdir("$installdir/lib/JSON/WebToken",0755);
mkdir("$installdir/lib/JSON/WebToken/Crypt",0755);
mkdir("$installdir/etc",0755);
mkdir("$installdir/bin",0755);
mkdir("$installdir/www",0755);
mkdir("$installdir/www/inc",0755);

# generating log signing key / cert
system("openssl req -new -newkey rsa:2048 -keyout \"$installdir/CA/private/sign.key\" -subj /CN=SignLogs -days 7300 -nodes -out \"$installdir/CA/sign.crt\" >/dev/null 2>&1");
chmod(0440, "$installdir/CA/private/sign.key");
system("chown $apacheuser $installdir/CA/private/sign.key");

print LOG ".";
sub substitute {
	my $fileorg=shift;
	my $filedst=shift;
	open(IN, "$tmpdir/$fileorg");
	my @lines=<IN>;
	close IN;
	open(OUT, ">$filedst");
	foreach my $line (@lines) {
		$line=~s/__INSTALLDIR__/$installdir/g;
		$line=~s/__CA_PATH__/$installdir\/CA/g;
		$line=~s/__CA_PUBLIC_PATH__/$installdir\/public/g;
		$line=~s/__CA_TMP_PATH__/$installdir\/tmp/g;
		$line=~s/__CA_HOSTNAME__/$hostname/g;
		$line=~s/__CA_PUBLIC_URI__/\/CA/g;
		$line=~s/__APACHE_ROOT_CONF__/$apacheconf/g;
		$line=~s/__APACHE_USER__/$apacheuser/g;
		$line=~s/__CRYPTOSYSTEM__/$cryptosystem/g;
		$line=~s/__P11_PIN__/$p11pin/g;
		$line=~s/__P11_SLOT__/$p11slot/g;
		$line=~s/__TYPE__/$TYPE/g;
		$line=~s/__AUTH_MODE__/$auth_mode/g;
		$line=~s/$default_scsecret/$new_scsecret/g;
		print OUT $line;
	}
	close OUT;
	print LOG ".";
}

substitute('etc/NSConfig.pm',"$installdir/etc/NSConfig.pm");
substitute('etc/pki.initd',"/etc/init.d/pki") if ($TYPE eq 'enterprise');
system("cp etc/custom_profile.cnf $installdir/etc/");
system("cp index.cgi $installdir/") unless ($tsaonly);
system("cp tsa.cgi $installdir/");
system("cp crmp.cgi $installdir/") unless ($tsaonly);
system("cp acme.cgi $installdir/") unless ($tsaonly);
system("cp pkiclient.exe $installdir/") unless ($tsaonly);
system("cp EnrollmentClientInstaller.exe $installdir/public/") unless ($tsaonly);
chmod(0755,"$installdir/index.cgi") unless ($tsaonly);
chmod(0755,"$installdir/tsa.cgi");
chmod(0755,"$installdir/crmp.cgi") unless ($tsaonly);
chmod(0755,"$installdir/acme.cgi") unless ($tsaonly);
chmod(0755,"$installdir/pkiclient.exe") unless ($tsaonly);
chmod(0755,"/etc/init.d/pki") if ($TYPE eq 'enterprise');
system("cp $tmpdir/lib/* $installdir/lib/NikoSoft/PKI/");
system("cp $tmpdir/slib/* $installdir/lib/NikoSoft/Service/") if ($TYPE eq 'enterprise');
system("cp -r $tmpdir/dlib/JSON/* $installdir/lib/JSON/");
system("cp $tmpdir/i18n/* $installdir/lib/NikoSoft/I18N/");
system("cp $tmpdir/bin/* $installdir/bin/");
system("cp $tmpdir/www/inc/* $installdir/www/inc/");

if ($tsaonly) {
	system("cd $installdir/bin/ && rm -f installcacert.pl  managepkicerts.pl");
}

if (-d "$apacheconf/conf.d") {
	if ($auth_mode eq 'CERT') {
		substitute('etc/retrustmepki_cert.conf',"$apacheconf/conf.d/retrustmepki.conf");
		substitute('etc/retrustmepki_cert.conf.rhel7',"$apacheconf/conf.d/retrustmepki.conf") if ($rhversion == 7);
	} else {
		substitute('etc/retrustmepki.conf',"$apacheconf/conf.d/retrustmepki.conf");
	}
	system("cp $tmpdir/ssl.conf $apacheconf/conf.d/ssl.conf");
	substitute('etc/ssl.conf.rhel7',"$apacheconf/conf.d/ssl.conf") if ($rhversion == 7);
} else {
	substitute('etc/retrustmepki.conf',"$installdir/retrustmepki.conf");
	print LOG "\nWARNING: your Apache install dir does not contain conf.d. Please finish install by hand.\n";
}

if (-d '/etc/cron.d/') {
	substitute('etc/retrustmepki.crond','/etc/cron.d/retrustmepki') unless ($tsaonly);
} else {
	substitute('etc/retrustmepki.crond',,"$installdir/retrustmepki.crond");
	print LOG "\nWARNING: could not find /etc/cron.d. Please finish install by hand.\n";
}

if (-f "/usr/sbin/a2enmod") {
	print LOG "\na2enmod detected, forcing activation of CGI Apache module\n";
	system("/usr/sbin/a2enmod cgi ssl");
}

if (-f "/usr/bin/chcon") {
	print LOG "\nSELinux detected, deactivating it.\n";
	system("/usr/sbin/setenforce Permissive");
	system("perl -pi -e's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config");
}

if (-f "/etc/redhat-release") {
	print LOG "\nDetected RedHat/Centos: autoconfiguring syslog.\n";
	system("echo \"local0.*\t/var/log/RTPKI.log\">/etc/rsyslog.d/RTPKI.conf");
	system("/sbin/service rsyslog restart");
	print LOG "\nInstalling PKI support RPMS.\n";
	system("rpm --import $tmpdir/RPMS.$rhversion/RPM-GPG-*");
	system("yum localinstall -y $tmpdir/RPMS.$rhversion/*.rpm");
	print LOG "\nActivating NTPD and PKI autostart.\n";
	if ($TYPE eq 'enterprise') {
		system("/sbin/chkconfig pki on");
		system("/sbin/chkconfig httpd off");
		system("perl -pi -e 's/Timeout 60/Timeout 120/' $apacheconf/conf/httpd.conf");
	} else {
		system("/sbin/chkconfig httpd on");
	}
	if ($auth_mode eq 'CERT') {
		system("echo \"#!/bin/sh\n\n/sbin/service httpd reload >/dev/null 2>&1\" > /etc/cron.hourly/crl_reload");
		chmod(0755,"/etc/cron.hourly/crl_reload");
	}
	system("/sbin/chkconfig ntpd on");
	system("/sbin/chkconfig ntpdate on");
	system("/sbin/service ntpdate start");
	print LOG "\nNow hardening the OS.\n";
	system("/sbin/chkconfig iptables off");
	system("/sbin/chkconfig ip6tables on");
	system("/sbin/service iptables stop");
	system("/bin/rm -f /etc/sysconfig/system-config-firewall");
	system('/usr/bin/perl -pi -e "s/-A INPUT -i lo -j ACCEPT/-A INPUT -i lo -j ACCEPT\n-A INPUT -m state --state NEW -m tcp -p tcp --dport 443 -j ACCEPT/" /etc/sysconfig/iptables');
	system('/usr/bin/perl -pi -e "s/-A INPUT -i lo -j ACCEPT/-A INPUT -i lo -j ACCEPT\n-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT/" /etc/sysconfig/iptables');
	system("/sbin/service iptables start");
	system("/sbin/chkconfig iptables on");
	system("/sbin/chkconfig yum-cron on");
	system("echo 'PermitRootLogin no'>>/etc/ssh/sshd_config");
}

system("echo -n \"retrust123\" | $installdir/bin/manageuser.pl add admin >/dev/null 2>&1");
system("echo -n \"$adminpwd\" | $installdir/bin/manageuser.pl modify admin >/dev/null 2>&1") if ($adminpwd);
chmod(0640,"$apacheconf/pkiusers");
system("chgrp $apacheuser $apacheconf/pkiusers");
print LOG "Starting PKI/Apache.\n";
if ($TYPE eq 'enterprise') {
	system("/sbin/service pki start");
} else {
	system("/sbin/service httpd start");
}
system("chown $apacheuser $installdir/tmp/*");
system('dialog --title "PKI Installation" --no-cancel --msgbox "\nPKI installation is now complete.\nThe installation logs are in /var/log/RTPKI-install.log\n\nPlease point your browser at https://'.$hostname.'/pki/\nThen, log on with user admin and perform the Key Ceremony\n\nAfter the Key Ceremony, it is strongly advised to reboot the server.\n" 20 61');

exit;
