#!/usr/bin/perl

use strict;

open FL, $ARGV[0];
my @lines=<FL>;
close FL;

my $text="";
foreach (@lines) { $text.=$_; }

my $title=$lines[0];
$title=~ s/\=//;
$title=~ s/\n//;

my $html=wiki2html($text);
print "<html><head><title>$title</title>";
print <<__CSS__;
<style type="text/css">
<!--/* <![CDATA[ */
body { font-family: Verdana, Arial, Sans; font-size: 11pt; } 
pre { background-color: #f2f2f2; margin: 0; padding: 0}
table { width: 100%; }
table.formtable { width: auto; }
table.result { border: 1px solid #ccc; }
table.result th, table.result td { border: 1px solid #ccc; }
table.result th { background-color: #f2f2f2; color: #000; }
table.result td { font-size:0.9em }
th { background: #f2f2f2; }
h1 { font-size: 1.5em; color: #fff; background-color: #000; padding: 1em; } 
h2 { font-size: 1.2em; color:#000;}
h4 { font-size: 1.0em; color:#000; margin-top: 6px; margin-bottom: 2px; }
hr { width: 100%; height: 0px; margin-top: 15px; margin-bottom: 15px; border-top: 1px solid #ccc; border-bottom: none; } 
div.menubar { width: 100%; float: left; margin: 0 0 2em 0; padding: 0; list-style: none; background-color: #f2f2f2; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc; color: #069; }
div.menubar a.menubar { float:left; display: block; padding: 6px 12px; text-decoration: none; font-weight: bold; color: #069; border-right: 1px solid #ccc; }
div.menubar a.menubar:hover { color: #c00; background-color: #fff; }
div.footerbar { width: 100%; margin: 0; padding: 4px 0; list-style: none; background-color: #f2f2f2; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc; color: #000; font-size: 0.7em;}
/* ]]> */-->
</style>
__CSS__
print "</head><body>\n";
print $html;
print "</body></html>\n";
exit;
#############
sub wiki2html{
	my $str=shift || return;
	#http://wiki.beyondunreal.com/wiki/Wiki_Markup
	#----  <hr>
	#At the beginning, - <h1>, -- <h2>, --- <h3>
	#Unorderd lists *, **
	#Ordered lists #, ##
	#''sdfs'' <i>, ''''sdfs'''' <b>, ''''''sddfs'''''' <b><i>
	my @lines=split(/[\n]/,$str);
	my $linecnt=@lines;
	my $dot='&#' . '9679' . ';';
	my $space='&' . 'nbsp' . ';';
	my %Marker=(
		ul => 0,
		ol => 0,
		pre => 0,
		);
	for(my $x=0;$x<$linecnt;$x++){
		$lines[$x]=strip($lines[$x]);
		my $original=$lines[$x];
		my $break=1;
		#----  <hr>
		$lines[$x]=~s/\-{4,4}/\<hr size\=\"1\"\>/sig;
		#< &lt; 
		$lines[$x]=~s/\</\&lt;/sig;
		#pre
		$Marker{pre}=1 if ($lines[$x]=~/^\{\{\{/);
		$lines[$x]=~s/\{\{\{/<pre>/sig;
		$Marker{pre}=0 if ($lines[$x]=~/^\}\}\}/);
		$lines[$x]=~s/\}\}\}/<\/pre>/sig;
		#At the beginning, - <h1>, -- <h2>, --- <h3>
		if($lines[$x]=~/^(\={1,3})(.+)/s){
			my $dashes=$1;
			my $txt=$2;
			my $count = $dashes =~ tr/\=//;
			$lines[$x]=qq|<h$count>$txt</h$count>|;
			$break=0;
		   	}
		if($lines[$x]=~/^Copyright/s){
			$lines[$x]=qq|<div class="footerbar">$lines[$x]</div>|;
			}
		#Unorderd lists *, **
		if($lines[$x]=~/^([\*\.\0]+)(.+)/s){
			my $stars=$1;
			my $txt=$2;
			my $count = $stars =~ tr/[\*\.\0]//;
			#$txt .= qq|<!-- ul: $Marker{ul} ,$count -->|;
			if($count > $Marker{ul}){
				$lines[$x]="<ul><li>" . $txt;
				$Marker{ul}=$count;
	                        }
			elsif($count == $Marker{ul}){
				$lines[$x]="<li>" . $txt;
	                        }
	                else{
				$lines[$x] = "</ul>";
				if(length($txt)){$lines[$x] .= "<li>" . $txt ;}
				$Marker{ul}=$count;
				}
			$break=0;
		   	}
		elsif($Marker{ul}){
			for(my $u=0;$u<$Marker{ul};$u++){
				$lines[$x] = "</ul>" . $lines[$x];
	                        }
	                $Marker{ul}=0;
	                }
	        #Orderd lists #, ##
		if($lines[$x]=~/^([\#]+)(.+)/s){
			my $stars=$1;
			my $txt=$2;
			my $count = $stars =~ tr/[\#]//;
			#$txt .= qq|<!-- ul: $Marker{ul} ,$count -->|;
			if($count > $Marker{ol}){
				$lines[$x]="<ol><li>" . $txt;
				$Marker{ol}=$count;
	                        }
			elsif($count == $Marker{ol}){
				$lines[$x]="<li>" . $txt;
	                        }
	                else{
				$lines[$x] = "</ol>";
				if(length($txt)){$lines[$x] .= "<li>" . $txt ;}
				$Marker{ol}=$count;
				}
			$break=0;
		   	}
		elsif($Marker{ol}){
			for(my $u=0;$u<$Marker{ol};$u++){
				$lines[$x] = "</ol>" . $lines[$x];
	                        }
	                $Marker{ol}=0;
	                }
		if(length($lines[$x])==0){$break=0;}
		#''sdfs'' <i>, ''''sdfs'''' <b>, ''''''sddfs'''''' <b><i>
                $lines[$x]=~s/\'{6,6}(.+?)\'{6,6}/<b><i>\1<\/i><\/b>/sig;
                $lines[$x]=~s/\'{4,4}(.+?)\'{4,4}/<b>\1<\/b>/sig;
                $lines[$x]=~s/\'{2,2}(.+?)\'{2,2}/<i>\1<\/i>/sig;

		#links [[ ]]
		if($lines[$x]=~/\[\[(.+?)\]\]/s){
			$lines[$x]=~s/\[\[(.+?)\]\]/\<a href=\"\1\"\ target="_new">\1<\/a>/sg;
			}

		$break=0 if ($Marker{pre});
		$break=1 if ($lines[$x]=~/^\s*$/s);
		#$lines[$x]=qq|<!--colcnt:$colcnt, table:$Marker{table}, Original: $original -->\n| . $lines[$x];
		if($break){$lines[$x] .= "<br>";}
		}
	if($Marker{ul}){
		for(my $u=0;$u<$Marker{ul};$u++){
			push(@lines,"</ul>");
	                }
		$Marker{ul}=0;
	        }
	if($Marker{ol}){
		for(my $u=0;$u<$Marker{ol};$u++){
			push(@lines,"</ol>");
	                }
		$Marker{ol}=0;
	        }
	return join("\n",@lines);
	}
###############
sub strip{
	#usage: $str=strip($str);
	#info: strips off beginning and endings returns, newlines, tabs, and spaces
	my $str=shift;
	if(length($str)==0){return;}
	$str=~s/^[\r\n\s\t]+//s;
	$str=~s/[\r\n\s\t]+$//s;
	return $str;
	}
