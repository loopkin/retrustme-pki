#!/bin/sh

./wiki2html.pl $1 > tmp.html
perl -pi -e "s/Alexandre Aufrere/$3/g" tmp.html
weasyprint tmp.html $2/$1.pdf
rm tmp.html
