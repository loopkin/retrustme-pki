##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##

=head1 NAME

NSConfig

=cut

package NSConfig;

use strict;
use Exporter 'import';
our @EXPORT=qw($INSTALLDIR $TYPE $CAPATH $CAPUBLICPATH $CAPUBLICURI $TMPDIR $CAHOSTNAME $APACHECONF $APACHEUSER $CRYPTO $OPENSSL $OPENCASCEP $DEBUG $SYSLOGFACILITY $AUTH_MODE $PRODUCT $VERSION $SCSECRET %cryptosystems);

# The installation directory
our $INSTALLDIR='__INSTALLDIR__';
# The CA Path. Should not be accessible from web server, but rw by web server's user
our $CAPATH='__CA_PATH__';
# Tha CA Public Path. Should be available from web server
our $CAPUBLICPATH='__CA_PUBLIC_PATH__';
# The CA Public URI. Alias to CA Public Path in web server configuration
# For instance, with Apache: Alias $CAPUBLICURI $CAPUBLICPATH
our $CAPUBLICURI='__CA_PUBLIC_URI__';
# TMP Directory. should be rw by web server's user
our $TMPDIR='__CA_TMP_PATH__';
# PKI FQDN hostname
our $CAHOSTNAME='__CA_HOSTNAME__';
# Path to apacheconf (where pkiusers file is)
our $APACHECONF='__APACHE_ROOT_CONF__';
# Apache user (that is, Linux user launching apache)
our $APACHEUSER='__APACHE_USER__';

# Cryptosystem used (rsa or gost)
# If unsure, use 'rsa' (which is cryptosystem composed of RSA algorithm, 3DES and SHA-1)
# gost cryptosystem refer to "euro-asian" (ie. russian) cryptosystem, composed of 
#  GOST R 34.10-2001, GOST 28147-89 and GOST R 34.11-94
our $CRYPTO='__CRYPTOSYSTEM__';

# Normally you don't need to change this
# WARNING: if you use GOST cryptosystem, this should point to an openssl 1.0+ executable
#our $OPENSSL='__INSTALLDIR__/bin/openssl-nspki';
our $OPENSSL='LD_LIBRARY_PATH=/opt/nkssl/lib /opt/nkssl/bin/openssl';
our $OPENCASCEP='LD_LIBRARY_PATH=/opt/nkssl/lib OPENSSL_CONF=__CA_TMP_PATH__/lpkiscep.cnf /opt/nkssl/bin/openca-scep';

# Debug mode
our $DEBUG=0;

# Syslog facility
our $SYSLOGFACILITY="local0";

#### END OF CONFIGURATION

####
#### Do not modify anything below this line ####
####

our $PRODUCT='__DIST_PRODUCT__';
our $VERSION='0.11';
our $TYPE='__TYPE__';
our $AUTH_MODE='__AUTH_MODE__';

our $SCSECRET='000000000000000000000000000000000000000000000000';

sub _rsa_hsm() {
	my $p11=shift;
	my $res={ 'gencsr_ca' => '-engine pkcs11 -keyform engine -key __P11_SLOT__:1121070105',
                'genkey_ca' => '/usr/bin/pkcs11-tool --module '.$p11.' -l --pin "__P11_PIN__" -k --key-type rsa:4096 -d 1121070105 --slot __P11_SLOT__ --private --label nspkica -m RSA-X9-31-KEY-PAIR-GEN',
                'genkey_tsa' => '/usr/bin/pkcs11-tool --module '.$p11.' -l --pin "__P11_PIN__" -k --key-type rsa:2048 -d 1121070106 --slot __P11_SLOT__ --private --label nspkitsa -m RSA-X9-31-KEY-PAIR-GEN',
                'delkey_tsa' => '/usr/bin/pkcs11-tool --module '.$p11.' -l --pin "__P11_PIN__" -b -d 1121070106 --slot __P11_SLOT__ -y privkey',
                'req_genkey_ca'=> '-engine pkcs11 -keyform engine -key __P11_SLOT__:1121070105 -sha256 -days 5475',
                'req_genkey_tsa'=> '-engine pkcs11 -keyform engine -key __P11_SLOT__:1121070106 -sha256',
                'req_genkey_client'=> '-newkey rsa:2048',
                'req_genkey_server'=> '-newkey rsa:2048',
                'ca_sign' => '-engine pkcs11 -keyform engine -keyfile __P11_SLOT__:1121070105 -md sha256',
                'tsa_sign' => '-engine pkcs11 -keyform engine -inkey __P11_SLOT__:1121070106',
                'create_pkcs12' => '',
                'ca_revoke' => '-engine pkcs11 -keyform engine -keyfile __P11_SLOT__:1121070105 -md sha256',
                'openssl_cnf_sect' => '
openssl_conf            = openssl_def

[openssl_def]
engines = engine_section

[engine_section]
pkcs11 = pkcs11_section

[pkcs11_section]
engine_id = pkcs11
dynamic_path = /opt/nkssl/lib/engines/engine_pkcs11.so
MODULE_PATH = '.$p11.'
PIN = "__P11_PIN__"
init = 0
',
		'user_key_property' => '2048' };
		return $res;
}

our %cryptosystems=(
        'rsa'=>{ 'gencsr_ca' => "-key \"$CAPATH/private/ca.key\"",
                'genkey_ca' => '',
                'genkey_tsa' => '',
                'req_genkey_ca'=> "-newkey rsa:4096 -sha256 -keyout \"$CAPATH/private/ca.key\" -nodes -days 3650",
                'req_genkey_tsa'=> "-newkey rsa:2048 -sha256 -keyout \"$CAPATH/private/tsa.key\" -nodes",
                'req_genkey_client'=> '-newkey rsa:2048',
                'req_genkey_server'=> '-newkey rsa:2048',
                'ca_sign' => "-md sha256 -keyfile \"$CAPATH/private/ca.key\"",
                'tsa_sign' => "-inkey \"$CAPATH/private/tsa.key\"",
                'create_pkcs12' => '',
                'ca_revoke' => "-md sha256 -keyfile \"$CAPATH/private/ca.key\"",
                'openssl_cnf_sect' => '', 
		'user_key_property' => '2048' } ,
	'ecdsa'=>{ 'gencsr_ca' => "-key \"$CAPATH/private/ca.key\"",
		'genkey_ca' => "$OPENSSL ecparam -name prime256v1 -out \"$CAPATH/ec_param.pem\"",
		'genkey_tsa' => "",
		'req_genkey_ca'=> "-newkey ec:\"$CAPATH/ec_param.pem\" -keyout \"$CAPATH/private/ca.key\" -sha256 -nodes",
		'req_genkey_tsa'=> "-newkey ec:\"$CAPATH/ec_param.pem\" -keyout \"$CAPATH/private/tsa.key\" -sha256 -nodes",
		'req_genkey_client'=> "-newkey ec:\"$CAPATH/ec_param.pem\"",
		'req_genkey_server'=> "-newkey ec:\"$CAPATH/ec_param.pem\"",
		'ca_sign' => "-md sha256 -keyfile \"$CAPATH/private/ca.key\"",
		'tsa_sign' => "-inkey \"$CAPATH/private/tsa.key\"",
		'create_pkcs12' => '',
		'ca_revoke' => "-md sha256 -keyfile \"$CAPATH/private/ca.key\"",
		'mscapi_csptype' => '16',
		'openssl_cnf_sect' => '', 
		'user_key_property' => 'nistp256' } ,
        'gost'=>{ 'gencsr_ca' => "-engine gost -key \"$CAPATH/private/ca.key\"",
                'genkey_ca' => '',
                'genkey_tsa' => '',
                'req_genkey_ca'=> "-engine gost -newkey gost2001 -pkeyopt paramset:A -keyout \"$CAPATH/private/ca.key\" -nodes -days 3650",
                'req_genkey_tsa'=> "-engine gost -newkey gost2001 -pkeyopt paramset:A -keyout \"$CAPATH/private/tsa.key\" -nodes",
                'req_genkey_client'=> '-engine gost -newkey gost2001 -pkeyopt paramset:A',
                'req_genkey_server'=> '-engine gost -newkey gost2001 -pkeyopt paramset:XA',
                'ca_sign' => "-engine gost -md md_gost94 -keyfile \"$CAPATH/private/ca.key\"",
                'tsa_sign' => "-engine gost -inkey \"$CAPATH/private/tsa.key\"",
                'create_pkcs12' => '-engine gost -certpbe gost89 -keypbe gost89 -macalg md_gost94',
                'ca_revoke' => "-engine gost -md md_gost94 -keyfile \"$CAPATH/private/ca.key\"",
		'mscapi_csptype' => '2',
                'openssl_cnf_sect' => '
openssl_conf = openssl_def

[openssl_def]
engines = engine_section

[engine_section]
gost = gost_section

[gost_section]
engine_id = gost
default_algorithms = ALL
CRYPT_PARAMS = id-Gost28147-89-CryptoPro-A-ParamSet
',
		'user_key_property' => '2001'} ,
       'rsaetoken'=>&_rsa_hsm('/usr/lib64/libeTPkcs11.so'), 
       'rsaps'=>&_rsa_hsm('/opt/PTK/lib/libcryptoki.so'),        
       'rsaluna'=>&_rsa_hsm('/usr/lunasa/lib/libCryptoki2_64.so')
);

1;
