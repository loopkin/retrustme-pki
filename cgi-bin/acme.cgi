#!/usr/bin/perl -w

##
## (c) 2010 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: acme.cgi 428 2018-03-21 08:00:33Z loopkin $
##

$|=1;

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=$ENV{SCRIPT_FILENAME} unless ($NSPATH);
	$NSPATH=~s/acme.cgi//;
	our $LANG=$ENV{HTTP_ACCEPT_LANGUAGE};
	$LANG='en' unless ($LANG);
	$LANG=~s/^([a-z]+)[\,\;\-].*$/$1/i;
	$LANG='en' unless (grep(/^$LANG$/,('en','fr','ru')));
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	eval  "use NikoSoft::I18N::$LANG;";
}

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";

use strict;
use utf8;
use CGI qw(:standard -utf8);
use MIME::Base64;
use NSConfig;
use MIME::Base64 qw(encode_base64 decode_base64 decode_base64url);
use NikoSoft::PKI::ACME;
use NikoSoft::PKI::JSONView;

my $cgi = CGI->new();
my $vars = $cgi->Vars();
my $keywords=$vars->{keywords};
$keywords=~s/\x{0}//g;
my $request = $ENV{REQUEST_URI};

my $config=NikoSoft::PKI::Config->new();
my $log=NikoSoft::PKI::Log->new();
my $acme=NikoSoft::PKI::ACME->new($config,$log);
my $jsonview=NikoSoft::PKI::JSONView->new($cgi,$config);

sub _error {
	my $code=shift || 'error:serverInternal';
	my $error= shift || $code;
  print $cgi->header(-type=>'application/problem+json',-status=>'400 Bad Request');
	$jsonview->json_start_empty();
	$jsonview->json_name_value('type',"urn:ietf:params:acme:$code",1);
	$jsonview->json_name_value('title',$error,0);
	$jsonview->json_end();
	exit;
}

sub _cert_transcode {
	my $cert=shift;
	if($cert=~m/CERTIFICATE-----/ ) {
		$cert=~s/-----BEGIN CERTIFICATE-----//;
		$cert=~s/-----END CERTIFICATE-----//;
		$cert=~s/^\n//g;
		$cert = decode_base64($cert);
	}
	return $cert;
}

sub _fixheader {
        my $str=shift;
        $str=~s/nonce/Nonce/g;
        return $str;
}

if ($request=~m/directory/) {
	print _fixheader($cgi->header(-type=>'application/json',-"Replay-Nonce"=>$acme->get_nonce()));
	print <<__EOF__;
{
  "key-change": "https://$CAHOSTNAME/acme/keychange",
  "new-authz": "https://$CAHOSTNAME/acme/newauthz",
  "new-cert": "https://$CAHOSTNAME/acme/newcert",
  "new-reg": "https://$CAHOSTNAME/acme/newreg",
  "revoke-cert": "https://$CAHOSTNAME/acme/revokecert"
}
__EOF__
} elsif ($request=~ m/newreg/) {
	my ($reg,$key,$email)=$acme->create_account($keywords);
	_error($reg) if ($reg=~m/^error/);
	print $cgi->header( -type=>'application/json', -status=>'201 Created', -location=>"https://$CAHOSTNAME/acme/account/$reg" );
	$jsonview->json_start('valid',1);
	$jsonview->json_name_value('key',$key,1);
	$jsonview->json_name_object('contact',"[\"mailto:$email\"]",0);
	$jsonview->json_end();
} elsif ($request =~ m/newauthz/) {
	my ($key,$token,$domain)=$acme->create_authz($keywords);
	_error($key) if ($key=~m/^error/);
	print $cgi->header( -type=>'application/json', -status=>'201 Created', -location=>"https://$CAHOSTNAME/acme/authz/$key");
	$jsonview->json_start('pending',1);
	$jsonview->json_name_object('identifier',"{\"type\":\"dns\",\"value\":\"$domain\"}",1);
	$jsonview->json_name_object('challenges',"[ { \"type\":\"http-01\",\"token\":\"$token\",\"uri\":\"https://$CAHOSTNAME/acme/authz/$key\" } ]",0);
	$jsonview->json_end();
} elsif ($request =~ m/authz/) {
	$request=~s/^.*authz\/([a-zA-Z0-9\=]+)$/$1/;
	my ($key,$token,$domain)=$acme->authz($request,$keywords);
	_error($key,$token) if ($key=~m/^error/);
	print $cgi->header( -type=>'application/json', -status=>'202 OK', -location=>"https://$CAHOSTNAME/acme/authz/$key");
	$jsonview->json_start('valid',1);
	$jsonview->json_name_object('identifier',"{\"type\":\"dns\",\"value\":\"$domain\"}",1);
	$jsonview->json_name_object('challenges',"[ { \"type\":\"http-01\",\"token\":\"$token\",\"uri\":\"https://$CAHOSTNAME/acme/authz/$key\" } ]",0);
	$jsonview->json_end();
} elsif ($request =~ m/newcert/) {
	my ($serial,$cert)=$acme->new_cert($keywords);
	_error($serial,$cert) if ($serial=~m/^error/);
	binmode STDOUT, ':raw';
	print $cgi->header( -type=>'application/pkix-cert', -status=>'201 Created', -location=>"https://$CAHOSTNAME/acme/cert/$serial");
	print _cert_transcode($cert);
} elsif ($request =~ m/cert/) {
	$request=~s/^.*cert\/([a-zA-Z0-9\=]+)$/$1/;
	my $cert=$acme->get_cert($request);
	_error($cert) if ($cert=~m/^error/);
	binmode STDOUT, ':raw';
	print $cgi->header( -type=>'application/pkix-cert', -status=>'200 OK', -location=>"https://$CAHOSTNAME/acme/cert/$request");
	print _cert_transcode($cert);
} else {
	_error();
}

exit;
