#!/usr/bin/perl
##
## (c) 2010 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: tsa.cgi 358 2015-08-21 21:58:50Z loopkin $
##

$|=1;

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=$ENV{SCRIPT_FILENAME} unless ($NSPATH);
	$NSPATH=~s/tsa.cgi//;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
}

use NikoSoft::PKI::TSA;
use NikoSoft::PKI::Config;
use NSConfig;

my $config=NikoSoft::PKI::Config->new();
my $tsa=NikoSoft::PKI::TSA->new($config);

sub _error_exit {
	print "Content-Type: application/timestamp-reply\n";
	my $tsr=$tsa->error_tsr();
	print "Content-Length: ".length($tsr)."\n\n";
	print $tsr;
	exit;
}

my $rnd='';
for (my $i=0;$i<4;$i++) {
	$rnd.=''.int(rand(9));
}
# time seed...
my @time=localtime();
my $sec=$time[0];
my $filename="tsa_tmp_$rnd$sec.tsq";

open OUT, ">$TMPDIR/$filename" or _error_exit();
while (<>) {
	print OUT $_;
}
close OUT;

print "Content-Type: application/timestamp-reply\n";
$tsa->initiate_ops();
my $tsr=$tsa->sign_tsq($filename);
print "Content-Length: ".length($tsr)."\n\n";
print $tsr;

unlink ("$TMPDIR/$filename");