#!/usr/bin/perl -w

##
## (c) 2010 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: index.cgi 433 2018-03-22 22:19:27Z loopkin $
##

$|=1;

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=$ENV{SCRIPT_FILENAME} unless ($NSPATH);
	$NSPATH=~s/index.cgi//;
	our $LANG=$ENV{HTTP_ACCEPT_LANGUAGE};
	$LANG='en' unless ($LANG);
	$LANG=~s/^([a-z]+)[\,\;\-].*$/$1/i;
	$LANG='en' unless (grep(/^$LANG$/,('en','fr','ru')));
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	eval  "use NikoSoft::I18N::$LANG;";
}

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";

use strict;
use utf8;
use CGI qw(:standard -utf8);
use Encode;
use MIME::Base64;
use NSConfig;
use NikoSoft::PKI::Utils qw(asn1_time utf8_print print_show_err dumpfile sendmail validate_data);
use NikoSoft::PKI::CryptoTools qw(openssl_cnf gen_friendly_name compute_response_from_challenge);
use NikoSoft::PKI::HTMLView;
use NikoSoft::PKI::JSONView;
use NikoSoft::PKI::CA;
use NikoSoft::PKI::Log;
use NikoSoft::PKI::MRTD;
use NikoSoft::PKI::InternalEntities;
use NikoSoft::PKI::Config;
use NikoSoft::PKI::AccessControl;

#
# _protect
# Input: string
# Output: safe string
#
sub _protect {
	my $string=scalar shift || '';
	$string=~ s/(\'|\$|\"|!)/\\$1/g;
	$string=~ s/\n/\\n/g;
	$string=~ s/\r//g;
	return $string;
}

sub _exit {
	 my $htmlview=shift;
	 my $direct=shift;
	 $htmlview->print_end_spinner() unless ($direct);
	 print "<br/>".$htmlview->end_html unless ($direct);
	 exit;
}

# Here starts the CGI stuff
my $cgi=CGI->new();
my $config=NikoSoft::PKI::Config->new();
my $log=NikoSoft::PKI::Log->new();
my $accesscontrol = NikoSoft::PKI::AccessControl->new();
my $htmlview=NikoSoft::PKI::HTMLView->new($cgi,$config);
my $jsonview=NikoSoft::PKI::JSONView->new($cgi,$config);
my $direct=$cgi->param('direct') || 0;
my $output_style=$direct;
$output_style=2 if (($direct==0)&&($DEBUG==0));
my $ca=NikoSoft::PKI::CA->new($config,$log);
$ca->set_output_mode($output_style);

$cgi->charset('utf-8');
my $org=$config->data()->{organization} || '';
if (($cgi->param('action'))&&($cgi->param('action') eq 'download_logs')) {
	$direct=1;
	print $cgi->header(-type=>'text/plain',-charset=>'utf-8', -attachment=>"pki_$org.log");
} else {
	print $cgi->header(-type=>'text/html',-charset=>'utf-8') unless ($direct);
	print $cgi->header(-type=>'application/json',-charset=>'utf-8') if ($direct);
}

my $logout=$cgi->url();
$logout=~s/https:\/\//https:\/\/log\:out\@/;
my $relurl=$cgi->url(-absolute=>1).'/';
$relurl=~s/\/\//\//g;

print $htmlview->start_html() unless ($direct);
if (!$direct) {
	$htmlview->print_menu_bar($cgi->param('action')||'none',$relurl,$logout);
}


# Testing if we should start KC
if (! -f "$CAPATH/private/scep.key") {
	if ($direct) { print "ERROR: Bootstrapping cannot be performed in API mode."; exit; }
	# Starting KC by displaying form
	if (length($cgi->param('cacn').'') < 1) {
		$htmlview->print_kc_form();
	# Executing CA creation
	} else {
		my $cn=_protect($cgi->param('cacn'));
		my $o=_protect($cgi->param('cao'));
		my $c=_protect($cgi->param('cac'));
		my $caemail=_protect($cgi->param('caemail'));
		if (!validate_data($cn)) { print_show_err($output_style, "error: "._T('ERR_MISSING')." cn (common name)"); _exit($htmlview,$direct); }
		if (!validate_data($o)) { print_show_err($output_style, "error: "._T('ERR_MISSING')."organization"); _exit($htmlview,$direct); }
		if (!validate_data($caemail,'email')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." email"); _exit($htmlview,$direct); }
		if (!validate_data($c,'country')) { print_show_err($output_style, "error: "._T('ERR_MISSING')."country"); _exit($htmlview,$direct); }
		print "Generating CA with Subject DN /C=$c/O=$o/CN=$cn.<br/>This may take a while generating key of type/size $cryptosystems{$CRYPTO}{req_genkey_ca}.<br/>";
		$ca->initiate_ops();
		$ca->gen_ca($c,$o,$cn,$caemail);
		print "<br/>Generating Internal Entities Certificates<br/><pre>";
		my $entities=NikoSoft::PKI::InternalEntities->new();
		$entities->gen_internal_entities('1.3.6.1.4.1.4146.2.2');
		print "</pre><br/>";
		if ($AUTH_MODE eq 'CERT') {
			my $crt_type='op_cert';
			my ($key,$csr)=$ca->gen_key_and_csr($crt_type,'Initial Administrator',$caemail);
			unless ($key) { print_show_err($output_style, "error: could not generate private key"); _exit($htmlview,$direct); }
			print _T('CERTIFICATE_SIGN')."<br>\n" if ($DEBUG);
			my %dn_params;
			$dn_params{cn}='Initial Administrator';
			$dn_params{email}=$caemail;
			$dn_params{uid}='admin';
			my ($crt,$crtserial)=$ca->sign_csr($csr,$crt_type,\%dn_params);
			unless ($crt) { print_show_err($output_style, "error: could not sign certificate"); _exit($htmlview,$direct); }
			print "PKCS#12 creation<br>\n" if ($DEBUG);
			my ($p12file,$fname)=$ca->gen_p12_as_file($key,$crt,'Initial Administrator','retrust123');
			unless ($p12file) { print_show_err($output_style, "error: could not generate PKCS#12"); _exit($htmlview,$direct); }
			print "<a href=\"$CAPUBLICURI/$p12file\" class=\"button\" >&nbsp; "._T('DOWNLOAD_PKCS12')." $cn &nbsp;</a><br/>";
			print "<br/>Warning: you MUST download this file.<br/><br/>";
		}
		print "Bootstrapping done. Please reboot the server.";
	}
	_exit($htmlview,$direct);
# If we don't need KC, then regular operation
} else {
	if (!$accesscontrol->is_connected_user_operator()) { print_show_err($output_style, "error: no operator connected"); _exit($htmlview,$direct); }
	# Nothing to do: print main page
	if ((!$cgi->param('cn')) && (!$cgi->param('rev_serial'))&&
		(!$cgi->param('view_serial')) && (!$cgi->param('search_dn'))&&
		(!$cgi->param('action'))) {
		$htmlview->print_cert_search_form($ca);
		$htmlview->print_cert_form($ca);
	# Certificate Sign Request
	} elsif  ($cgi->param('action') eq 'enroll_pkcs10') {
		my $csr=$cgi->param('pkcs10');
		# This parameter can be either a PKCS10 or a file upload item to a file containing the PKCS10
		# Hence the logic below to manage that.
		if (!validate_data($csr,'pkcs10')) {
			my $tmpFile=$cgi->upload('pkcs10');
			$csr='';
			if (defined $tmpFile) {
				my $io_handle = $tmpFile->handle;
				while ($io_handle->read(my $buffer,1024)) { $csr.=$buffer; }
			}
		}
		$csr = scalar $csr;
		# PKCS10 Validation also tests for consistency with PKI requirements
		if (!validate_data($csr,'pkcs10')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." pkcs10 (Malformed PKCS#10 or invalid key parameters or size)"); _exit($htmlview,$direct); }
		my $cn=_protect(scalar $cgi->param('cn'));
		if (!validate_data($cn,'utfstring')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." cn (common name)"); _exit($htmlview,$direct); }
		my $email=_protect(scalar $cgi->param('email'));
		if (!validate_data($email,'email')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." email"); _exit($htmlview,$direct);}
		my $crt_type=_protect(scalar $cgi->param('crt_type'));
		if (!validate_data($crt_type)) { print_show_err($output_style, "error: "._T('ERR_MISSING_CRT_TYPE')); _exit($htmlview,$direct); }
		# Only in decentralized: optional lifetime
		my $lifetime=_protect(scalar $cgi->param('lifetime'));
		if (($lifetime)&&(!validate_data($lifetime,'number'))) { print_show_err($output_style, "error: "._T('ERR_MISSING')." lifetime"); _exit($htmlview,$direct); }
		$lifetime=0 if (!$lifetime);
		# Only in decentralized: optional device serial number
		my $device_sn=_protect(scalar $cgi->param('device_sn'));
		if (($device_sn)&&(!validate_data($device_sn,'serial'))) { print_show_err($output_style, "error: "._T('ERR_MISSING')." device_sn"); _exit($htmlview,$direct); }

		$ca->initiate_ops();
		print _T('CERTIFICATE_SIGN')."<pre>\n" unless ($direct);
		my %dn_params;
		$dn_params{cn}=$cn;
		$dn_params{email}=$email;
		$dn_params{device_serial_number}=$device_sn;
		my ($crt,$crtserial)=$ca->sign_csr($csr,$crt_type,\%dn_params,0,$lifetime);
		unless ($crt) { print_show_err($output_style, "error: could not sign certificate"); _exit($htmlview,$direct); }
		if ($direct) {
			$jsonview->json_start('OK',1);
			$jsonview->json_name_value('serial',uc($crtserial),1);
			$jsonview->json_name_value('pem',$crt,0);
			$jsonview->json_end();
		} else {
			if (($cgi->param('RequestType'))&&($cgi->param('RequestType') eq 'CSP')) {
				$htmlview->print_smartcard_javascript($crt);
				print "<script>InstallCert();</script>";
			}
			print $crt;
			print "</pre>";
		}
	# enroll (P12 mode)
	} elsif ($cgi->param('action') eq 'enroll') {
		my $cn=_protect(scalar $cgi->param('cn'));
		if (!validate_data($cn,'utfstring')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." cn (common name)"); _exit($htmlview,$direct); }
		my $email=_protect(scalar $cgi->param('email'));
		if (!validate_data($email,'email')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." email"); _exit($htmlview,$direct); }
		my $pwd=_protect(scalar $cgi->param('pwd'));
		if (!validate_data($pwd,'password')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." pwd"); _exit($htmlview,$direct); }
		my $crt_type=_protect(scalar $cgi->param('crt_type'));
		if (!validate_data($crt_type)) { print_show_err($output_style, "error: "._T('ERR_MISSING_CRT_TYPE')); _exit($htmlview,$direct); }
		# Only for operators: uid
		my $uid=_protect(scalar $cgi->param('uid'));
		if (($uid)&&(!validate_data($uid,'uid'))) { print_show_err($output_style, "error: "._T('ERR_MISSING')." uid"); _exit($htmlview,$direct); }
		if (($uid)&&(!$accesscontrol->is_connected_user_admin())) { print_show_err($output_style, "error: operators certificate can only be issued by 'admin' users"); _exit($htmlview,$direct); }

		$htmlview->print_start_spinner() unless ($direct);
		print "Key Generation<br/>\n" if ($DEBUG);
		$ca->initiate_ops();
		my ($key,$csr)=$ca->gen_key_and_csr($crt_type,$cn,$email);
		unless ($key) { print_show_err($output_style, "error: could not generate private key"); _exit($htmlview,$direct); }
		print _T('CERTIFICATE_SIGN')."<br>\n" if ($DEBUG);
		my %dn_params;
		$dn_params{cn}=$cn;
		$dn_params{email}=$email;
		$dn_params{uid}=$uid;
		my ($crt,$crtserial)=$ca->sign_csr($csr,$crt_type,\%dn_params);
		unless ($crt) { print_show_err($output_style, "error: could not sign certificate"); _exit($htmlview,$direct); }
		print "PKCS#12 creation<br>\n" if ($DEBUG);
		my ($p12file,$fname)=$ca->gen_p12_as_file($key,$crt,$cn,$pwd);
		unless ($p12file) { print_show_err($output_style, "error: could not generate PKCS#12"); _exit($htmlview,$direct); }
		my $p12mailsubject=$config->data('p12mailsubject');
		my $p12mailtext=$config->data('p12mailtext');
		$p12mailtext=~s/\[CN\]/$cn/g;
		sendmail($email,$p12mailsubject,$p12mailtext, $config,"$CAPUBLICPATH/$p12file") if ($config->data()->{p12mail});
		print $cgi->br()."</pre>" unless ($direct);
		print "<a href=\"$CAPUBLICURI/$p12file\" class=\"button\" >&nbsp; "._T('DOWNLOAD_PKCS12')." $cn &nbsp;</a>" if (($config->data()->{p12link})&&(!$direct));
		$htmlview->print_end_spinner(_T('SUCCESS')) unless ($direct);

		if ($direct) {
			$jsonview->json_start('OK',1);
			$jsonview->json_name_value('serial',uc($crtserial),1);
			$jsonview->json_name_value('friendly_name',$fname,1);
			$jsonview->json_name_value('pkcs12',$ca->get_p12_as_base64("$CAPUBLICPATH/$p12file"),0);
			$jsonview->json_end();
			unlink("$CAPUBLICPATH/$p12file");
		}
	# Certificate Revocation Request
	} elsif ($cgi->param('rev_serial') || ($cgi->param('action') eq 'revoke')) {
		my @list_serials=$cgi->param('rev_serial');
		if (scalar(@list_serials)==0) { print_show_err($output_style, "error: "._T('ERR_MISSING')." rev_serial"); _exit($htmlview,$direct); }
		$ca->initiate_ops();
		$htmlview->print_start_spinner() unless ($direct);
		foreach my $serial (@list_serials) {
			$serial=~s/[^0-9a-f]//ig;
			if (!validate_data($serial)) { print_show_err($output_style, "error: "._T('ERR_MISSING')." rev_serial"); _exit($htmlview,$direct); }
			print "Revoking Certificate with Serial #$serial".$cgi->br()."\n" if ($DEBUG);
			$ca->revoke_cert($serial);
		}
		print _T("GENERATING_CRL")."<br/>\n" if ($DEBUG);
		# CRL is generated only if set up...
		$ca->generate_crl() if ($config->data('crlonrevoke'));
		$htmlview->print_end_spinner(_T('SUCCESS')) unless ($direct);
		if ($direct) {
			$jsonview->json_start('OK',0);
			$jsonview->json_end();
		}
	# Viewing Certificate
	} elsif (($cgi->param('view_serial')) || ($cgi->param('action') eq 'viewcert')) {
		my $serial=_protect(scalar $cgi->param('view_serial'));
		$serial=~s/[^0-9a-f]//ig;
		my $show_rev=_protect(scalar $cgi->param('show_rev'));
		if (!validate_data($serial)) { print_show_err($output_style, "error: "._T('ERR_MISSING')." view_serial"); _exit($htmlview,$direct); }
		# in non-JSON mode, we display the certificate using OpenSSL for the "pretty print"
		$ca->initiate_ops() unless ($direct);
		my $crt=$ca->get_certificate_by_serial($serial,!$direct);
		if (!$crt) { print_show_err($output_style, "error: "._T('ERR_CRT_NOT_FOUND')." $serial"); _exit($htmlview,$direct); }
		$htmlview->print_cert($serial,$crt,$show_rev) unless ($direct);
		if ($direct) {
			$jsonview->json_start('OK',1);
			$jsonview->json_name_value('serial', $serial, 1);
			$jsonview->json_name_value('pem', $crt, 0);
			$jsonview->json_end();
		}
	# Searching for certificate
	} elsif ($cgi->param('search_dn') || ($cgi->param('action') eq 'search')) {
		my $search_dn=_protect(scalar $cgi->param('search_dn'));
		if (!validate_data($search_dn)) { print_show_err($output_style, "error: "._T('ERR_MISSING')." search_dn"); _exit($htmlview,$direct); }
		my $search_profile=_protect(scalar $cgi->param('search_profile'));
		if (($search_profile)&&(!validate_data($search_profile,'profile'))) { print_show_err($output_style, "error: "._T('ERR_MISSING')." search_profile"); _exit($htmlview,$direct); }
		my $search_revoked=_protect(scalar $cgi->param('search_revoked')) || 'V';
		$search_revoked=~s/[^A-Z0-9]//g;
		$search_dn=~s/\*/\%/g;
		$search_dn=~s/\;//g;
		$search_revoked='any' if ($search_revoked eq '1');
		$htmlview->print_start_spinner() unless ($direct);
		$htmlview->print_cert_list($output_style,$jsonview,$search_revoked,$search_dn,$ca,$search_profile);
		$htmlview->print_end_spinner() unless ($direct);
	# Displaying and working with settings form
	} elsif ($cgi->param('action') eq 'settings') {
		if (!$accesscontrol->is_connected_user_admin()) { print_show_err($output_style, "error: settings can be seen only by 'admin' users"); _exit($htmlview,$direct); }
		if ($cgi->param('do')) {
			# Forcing JSON output
			$output_style=1;
			$direct=1;
			if (!validate_data($cgi->param('adminmail'),'email'))
				{ print_show_err($output_style, "error: "._T('ERR_MISSING')." email"); _exit($htmlview,$direct); }
			if (!validate_data($cgi->param('crldp')))
				{ print_show_err($output_style, "error: "._T('ERR_MISSING')." CRLDP"); _exit($htmlview,$direct); }
			if (!validate_data($cgi->param('aiaca')))
				{ print_show_err($output_style, "error: "._T('ERR_MISSING')." AIA CA"); _exit($htmlview,$direct); }
			if (!validate_data($cgi->param('cpoid'),'oid'))
				{ print_show_err($output_style, "error: "._T('ERR_MISSING')." CP OID"); _exit($htmlview,$direct); }

			$config->set_data('mail_in_dn',$cgi->param('opt_mail_in_dn') || 0, 0);
			$config->set_data('guess_dn_from_mail',$cgi->param('opt_guess_dn_from_mail') || 0, 0);

			$config->set_data('p12mail',$cgi->param('opt_p12mail') || 0, 0);
			$config->set_data('p12link',$cgi->param('opt_p12link') || 0 , 0);
			$config->set_data('static_scep',$cgi->param('opt_static_scep') || 0 , 0);
			$config->set_data('scep_crt_type',$cgi->param('opt_scep_crt_type') || 'serv_cert' , 'serv_cert');
			$config->set_data('acme_crt_type',$cgi->param('opt_acme_crt_type') || 'serv_cert' , 'serv_cert');

			my %profiles=$ca->cert_profiles_list(1);
			my @profileslist=sort(keys(%profiles));
			foreach my $profid (@profileslist) {
				if (!validate_data($cgi->param('opt_prof_'.$profid.'_crtlifetime'),'number'))
					{ print_show_err($output_style, "error: "._T('ERR_MISSING')." crtlifetime"); _exit($htmlview,$direct); }
				if (!validate_data($cgi->param('opt_prof_'.$profid.'_maxnbcert'),'number'))
					{ print_show_err($output_style, "error: "._T('ERR_MISSING')." maxnbcert"); _exit($htmlview,$direct); }
				$config->set_data('prof_'.$profid,$cgi->param('opt_prof_'.$profid) || 0, 0);
				$config->set_data('prof_'.$profid.'_crtlifetime',$cgi->param('opt_prof_'.$profid.'_crtlifetime') || 0, 0);
				$config->set_data('prof_'.$profid.'_maxnbcert',$cgi->param('opt_prof_'.$profid.'_maxnbcert') || 0, 1);
				$config->set_data('prof_'.$profid.'_ou',_protect($cgi->param('opt_prof_'.$profid.'_ou')), '');
			}

			$config->set_data('adminmail',_protect($cgi->param('adminmail')),'noname@example.com');
			$config->set_data('p12mailsubject',_protect($cgi->param('p12mailsubject')),'Your PKCS12');
			$config->set_data('p12mailtext',_protect($cgi->param('p12mailtext')),'Here is your PKCS12');
			$config->set_data('renewmailsubject',_protect($cgi->param('renewmailsubject')),'Expiration notice');
			$config->set_data('renewmailtext',_protect($cgi->param('renewmailtext')),'The certificate [DN] is about to expire');
			$config->set_data('mscapicsp',_protect($cgi->param('mscapicsp')),'Microsoft Base Smart Card Crypto Provider');
			$config->set_data('crldp',_protect($cgi->param('crldp')),'http://www.example.com/');
			$config->set_data('aiaca',_protect($cgi->param('aiaca')),'http://www.example.com/');
			$config->set_data('cpoid',_protect($cgi->param('cpoid')),'2.5.29.32.0');
			$config->set_data('autopurge',$cgi->param('opt_autopurge') || 0 , 0);
			$config->set_data('dailycron',$cgi->param('opt_dailycron') || 0 , 0);
			$config->set_data('crlonrevoke',$cgi->param('opt_crlonrevoke') || 0 , 0);
			$config->set_data('renewholder',$cgi->param('opt_renewholder') || 0 , 0);
			$config->set_data('renewadmin',$cgi->param('opt_renewadmin') || 0 , 0);
			$config->save();
			if ($direct) {
				$jsonview->json_start('OK',0);
				$jsonview->json_end();
			}
		} else {
			$htmlview->print_settings_form($ca);
		}
	} elsif ($cgi->param('action') eq 'about') {
		$ca->initiate_ops();
		$htmlview->print_about();
	} elsif ($cgi->param('action') eq 'camgt') {
		$htmlview->print_ca_management($log);
		print '<br/';
		$htmlview->print_sc_management();
	} elsif ($cgi->param('action') eq 'scepinfo') {
		$htmlview->print_scep_info();
	} elsif ($cgi->param('action') eq 'opcert') {
		$htmlview->print_op_cert_form($ca);
		$htmlview->print_op_cert_list('V',$ca);
	} elsif ($cgi->param('action') eq 'crlgen') {
		$htmlview->print_start_spinner() unless ($direct);
		$ca->initiate_ops();
		print _T("GENERATING_CRL")."<br/>\n" if ($DEBUG);
		$ca->generate_crl();
		$htmlview->print_end_spinner(_T('SUCCESS')) unless ($direct);
		if ($direct) {
			$jsonview->json_start('OK',0);
			$jsonview->json_end();
		}
	} elsif ($cgi->param('action') eq 'list_profiles') {
		my %labels=$ca->cert_profiles_list();
		my @labelslist=sort(keys(%labels));
		if ($direct) {
			$jsonview->json_start('OK',1);
			my $i=0;
			foreach (@labelslist) { $jsonview->json_name_value("profile$i",$_,1); $i++; }
			$jsonview->json_name_value('count',$i,0);
			$jsonview->json_end();
		}
	} elsif ($cgi->param('action') eq 'version') {
		if ($direct) {
			$jsonview->json_start('OK',1);
			$jsonview->json_name_value('version',$VERSION,1);
			$jsonview->json_name_value('description',"$PRODUCT v$VERSION",0);
			$jsonview->json_end();
		}
	} elsif ($cgi->param('action') eq 'download_logs') {
		if (!$accesscontrol->is_connected_user_admin()) { print_show_err($output_style, "error: logs can be seen only by 'admin' users"); _exit($htmlview,$direct); }
		print $log->read_logs(1);
	} elsif ($cgi->param('action') eq 'challenge_response') {
		# We force JSON output for that one
		$output_style=1;
		$direct=1;
		if (!$accesscontrol->is_connected_user_operator()) { print_show_err($output_style, "error: challenge/response can be done only by 'admin' users"); _exit($htmlview,$direct); }
		my $challenge=_protect(scalar $cgi->param('challenge'));
		$jsonview->json_start('OK',1);
		$jsonview->json_name_value('response',compute_response_from_challenge($challenge),0);
		$jsonview->json_end();
	} elsif ($cgi->param('action') eq 'search_logs') {
		# We force JSON output for that one
		$output_style=1;
		$direct=1;
		if (!$accesscontrol->is_connected_user_admin()) { print_show_err($output_style, "error: logs can be seen only by 'admin' users"); _exit($htmlview,$direct); }
		my %params;
		my $action_log=_protect(scalar $cgi->param('action_log'));
		if (($action_log)&&(!validate_data($action_log,'string'))) { print_show_err($output_style, "error: "._T('ERR_MISSING')." action_log"); _exit($htmlview,$direct); }
		$params{action}=$action_log if ($action_log);
		my $outcome=_protect( scalar $cgi->param('outcome'));
		$params{outcome}='SUCCESS' if ($outcome eq 'S');
		$params{outcome}='ERROR' if ($outcome eq 'E');
		my $ids=$log->get_logs_by_action_date(\%params);
		$jsonview->json_start('OK',1);
		my $count=0;
		foreach my $id (sort { $b <=> $a } keys(%{$ids})) {
			my $lcomment=$ids->{$id}->{comment};
			$lcomment=~s/\|//g;
			$jsonview->json_name_value("res$count",
					"$id|".$ids->{$id}->{ts}.'|'.$ids->{$id}->{action}.'|'.$ids->{$id}->{actor}.'|'.$ids->{$id}->{entity}.'|'.$ids->{$id}->{outcome}
					.'|'.$lcomment ,1);
			$count++;
		}
		$jsonview->json_name_value("count",$count,0);
		$jsonview->json_end();
	} elsif ($cgi->param('action') eq 'mrtd') {
		# We force JSON output for that one
		$output_style=1;
		$direct=1;
		# cn and serial are mandatory
		my $cn=_protect(scalar $cgi->param('cn'));
		if (!validate_data($cn)) { print_show_err($output_style, "error: "._T('ERR_MISSING')." cn (common name)"); _exit($htmlview,$direct); }
		my $serial=_protect(scalar $cgi->param('serial'));
		if (!validate_data($serial,'serial')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." serial"); _exit($htmlview,$direct);}
		my $doe=_protect(scalar $cgi->param('doe'));
		if (($doe)&&(!validate_data($doe,'number'))) { print_show_err($output_style, "error: "._T('ERR_MISSING')." doe"); _exit($htmlview,$direct); }
		# now generating the MRTD stuff
		my $mrtd=NikoSoft::PKI::MRTD->new($log);
		my $dg1=$mrtd->gen_dg1($cn,$serial,$doe);
		my $dg2=$mrtd->gen_dg2();
		my $efcom=$mrtd->gen_ef_com();
		# Signing the SOD requires openssl, hence the initiate_ops
		$ca->initiate_ops();
		my $sod=$mrtd->gen_sign_sod($dg1,$dg2,$cn,$serial);
		# and returning in case all ok
		if ($sod) {
				$jsonview->json_start('OK',1);
				$jsonview->json_name_value('dg1',encode_base64($dg1,''),1);
				$jsonview->json_name_value('dg2',encode_base64($dg2,''),1);
				$jsonview->json_name_value('efcom',encode_base64($efcom,''),1);
				$jsonview->json_name_value('sod',encode_base64($sod,''),0);
				$jsonview->json_end();
		} else {
				$jsonview->json_start('error',0);
				$jsonview->json_end();
		}
	} elsif ($cgi->param('action') eq 'scep_challenge') {
		if (!$accesscontrol->is_connected_user_operator()) { print_show_err($output_style, "error: scep challenge can be seen only by 'admin' users"); _exit($htmlview,$direct); }
		# We force JSON output for that one
		$output_style=1;
		$direct=1;
		# we just get the challenge, and display it
		my $challenge= $config->data('scep_challenge');
		$jsonview->json_start('OK',1);
		$jsonview->json_name_value('scep_challenge',$challenge,0);
		$jsonview->json_end();
	} else {
		print_show_err($output_style,"Error: unknown action");
	}
}

print "<br/>".$htmlview->end_html unless ($direct);
exit;
