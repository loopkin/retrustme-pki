#!/usr/bin/perl -w

##
## (c) 2010 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: crmp.cgi 410 2017-08-05 18:06:19Z loopkin $
##

$|=1;

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=$ENV{SCRIPT_FILENAME} unless ($NSPATH);
	$NSPATH=~s/crmp.cgi//;
	our $LANG=$ENV{HTTP_ACCEPT_LANGUAGE};
	$LANG='en' unless ($LANG);
	$LANG=~s/^([a-z]+)[\,\;\-].*$/$1/i;
	$LANG='en' unless (grep(/^$LANG$/,('en','fr','ru')));
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	eval  "use NikoSoft::I18N::$LANG;";
}

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";

use strict;
use utf8;
use CGI qw(:standard -utf8);
use Encode;
use MIME::Base64;
use NSConfig;
use NikoSoft::PKI::Utils qw(asn1_time utf8_print print_show_err dumpfile sendmail validate_data);
use NikoSoft::PKI::CryptoTools qw(openssl_cnf gen_friendly_name parse_x509 get_random);
use NikoSoft::PKI::HTMLView;
use NikoSoft::PKI::JSONView;
use NikoSoft::PKI::CA;
use NikoSoft::PKI::Log;
use NikoSoft::PKI::MRTD;
use NikoSoft::PKI::InternalEntities;
use NikoSoft::PKI::Config;
use NikoSoft::PKI::AccessControl;

#
# _protect
# Input: string
# Output: safe string
#
sub _protect {
	my $string=scalar shift || '';
	$string=~ s/(\'|\$|\"|!)/\\$1/g;
	$string=~ s/\n/\\n/g;
	$string=~ s/\r//g;
	return $string;
}

sub _exit {
	my $jsonview=shift;
	my $msg=shift || "Unknown error";
	$jsonview->json_start('error',1);
	$jsonview->json_name_value('code','500',1);
	$jsonview->json_name_value('msg',$msg,0);
	exit;
}

# Here starts the CGI stuff
my $cgi=CGI->new();
my $config=NikoSoft::PKI::Config->new();
my $log=NikoSoft::PKI::Log->new();
my $accesscontrol = NikoSoft::PKI::AccessControl->new();
my $jsonview=NikoSoft::PKI::JSONView->new($cgi,$config);
my $direct=1;
my $output_style=$direct;
$output_style=2 if (($direct==0)&&($DEBUG==0));
my $ca=NikoSoft::PKI::CA->new($config,$log);
$ca->set_output_mode($output_style);

$cgi->charset('utf-8');
my $org=$config->data()->{organization} || '';
print $cgi->header(-type=>'application/json',-charset=>'utf-8') if ($direct);

# Testing if we should start KC
if (! -f "$CAPATH/private/scep.key") {
	_exit($jsonview,"ERROR: Bootstrapping cannot be performed in API mode.");
# If we don't need KC, then regular operation
} else {
	if (!$accesscontrol->is_connected_user_operator()) { 
		print_show_err($output_style, "error: no operator connected"); _exit($jsonview);	
	# Certificate Sign Request
	} elsif  ($cgi->param('action') eq 'enroll') {
		my $crt_type=_protect(scalar $cgi->param('profile'));
		if (!validate_data($crt_type)) { print_show_err($output_style, "error: "._T('ERR_MISSING_CRT_TYPE')); _exit($jsonview); }	
		my $profile=$crt_type;
		my $csr=scalar $cgi->param('pkcs10');
		my $key='';
		my $cn=_protect(scalar $cgi->param('cn'));
		if (!validate_data($cn,'utfstring')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." cn (common name)"); _exit($jsonview); }
		my $email=_protect(scalar $cgi->param('email'));
		if (!validate_data($email,'email')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." email"); _exit($jsonview);}
		# Only in decentralized: optional lifetime
		my $lifetime=_protect(scalar $cgi->param('lifetime'));
		if (($lifetime)&&(!validate_data($lifetime,'number'))) { print_show_err($output_style, "error: "._T('ERR_MISSING')." lifetime"); _exit($jsonview); }
		$lifetime=0 if (!$lifetime);
		# Only in decentralized: optional device serial number
		my $device_sn=_protect(scalar $cgi->param('serialNumber'));
		if (($device_sn)&&(!validate_data($device_sn,'serial'))) { print_show_err($output_style, "error: "._T('ERR_MISSING')." serialNumber"); _exit($jsonview); }		
		$ca->initiate_ops();
		if ($profile=~m/\_p12/) {
				($key,$csr)=$ca->gen_key_and_csr($crt_type,$cn,$email);
				unless ($key) { print_show_err($output_style, "error: could not generate private key"); _exit($jsonview); }
				$crt_type=~s/\_p12//;
		} else {
			# PKCS10 Validation also tests for consistency with PKI requirements
			if (!validate_data($csr,'pkcs10')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." pkcs10 (Malformed PKCS#10 or invalid key parameters or size)"); _exit($jsonview); }
		}
		my %dn_params;
		$dn_params{cn}=$cn;
		$dn_params{email}=$email;
		$dn_params{device_serial_number}=$device_sn;		
		my ($crt,$crtserial)=$ca->sign_csr($csr,$crt_type,\%dn_params,0,$lifetime);
		unless ($crt) { print_show_err($output_style, "error: could not sign certificate"); _exit($jsonview); }
		my ($p12file,$fname)=();
		my $pwd='';
		if ($profile=~m/\_p12/) {
				$pwd=get_random(14);
				($p12file,$fname)=$ca->gen_p12_as_file($key,$crt,$cn,$pwd);
				unless ($p12file) { print_show_err($output_style, "error: could not generate PKCS#12"); _exit($jsonview); }
		}
		$jsonview->json_start('OK',1);
		$jsonview->json_name_value('serial',uc($crtserial),1);
		if ($profile=~m/\_p12/) {
			$jsonview->json_name_value('pkcs12',$ca->get_p12_as_base64("$CAPUBLICPATH/$p12file"),1);
			$jsonview->json_name_value('password',$pwd,0);
		} else {
			$jsonview->json_name_value('certificate',$crt,0);
		}
		$jsonview->json_end();
	# Certificate Revocation Request
	} elsif ($cgi->param('action') eq 'revoke') {
		my $certificate=scalar $cgi->param('certificate');
		if (!$certificate) { print_show_err($output_style, "error: "._T('ERR_MISSING')." certificate"); _exit($jsonview); }
		my ($serial,$issuer,$subject,$error)=parse_x509($certificate);
		if (!validate_data($serial)) { print_show_err($output_style, "error: "._T('ERR_MISSING')." certificate. $error "); _exit($jsonview); }		
		$ca->initiate_ops();
		$ca->revoke_cert($serial);			
		# CRL is generated only if set up...
		$ca->generate_crl() if ($config->data('crlonrevoke'));
		$jsonview->json_start('OK',0);
		$jsonview->json_end();
	# Searching for certificate
	} elsif ($cgi->param('action') eq 'retrieve') {
		my $email=_protect(scalar $cgi->param('email'));
		if (!validate_data($email,'email')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." email"); _exit($jsonview); }
		my $profile=_protect(scalar $cgi->param('profile'));
		if (!validate_data($profile,'profile')) { print_show_err($output_style, "error: "._T('ERR_MISSING')." profile"); _exit($jsonview); }
		my $search_revoked=_protect(scalar $cgi->param('status')) || 'V';
		$search_revoked=~s/[^A-Z]//g;
		my @pems=$ca->retrieve_certificates($search_revoked,$email,$profile);
		my $arr='[';
		foreach my $pem (@pems) {  $pem=~s/\n/\\n/g; $arr.="\"$pem\","; }
		$arr=~s/\,$/\]/;
		$arr.=']' unless ($arr=~m/\]$/);
		$jsonview->json_start('OK',1);
		$jsonview->json_name_object('certificate',$arr,0);
		$jsonview->json_end();		
	} elsif ($cgi->param('action') eq 'crlgen') {
		$ca->initiate_ops();
		$ca->generate_crl();
		$jsonview->json_start('OK',0);
		$jsonview->json_end();
	} elsif ($cgi->param('action') eq 'list') {
		my %labels=$ca->cert_profiles_list();
		my @labelslist=sort(keys(%labels));
		$jsonview->json_start('OK',1);
		my $arr='[';
		foreach my $profile (@labelslist) { 
			$arr.="\"$profile\",";  
			$arr.="\"$profile"."_p12\"," if (($profile eq 'auth_cert')||($profile eq 'scl_cert')||($profile eq 'serv_cert'));
		}
		$arr=~s/\,$/\]/;
		$jsonview->json_name_object('profiles',$arr,0);
		$jsonview->json_end();
	} elsif ($cgi->param('action') eq 'properties') {
		my $profile=_protect(scalar $cgi->param('profile'));
		my $rp10='true';
		$rp10='false' if ($profile=~m/\_p12/);
		$profile=~s/\_p12//;
		my %labels=$ca->cert_profiles_list();
		my $description=$labels{$profile};
		print_show_err($output_style, "error: incorrect profile $profile") unless ($description); 
		$jsonview->json_start('OK',1);
		$jsonview->json_name_value('description',$description,1);
		$jsonview->json_name_value('require-pkcs10',$rp10,1);
		$jsonview->json_name_value('key-escrow','false',1);
		$jsonview->json_name_value('can-suspend','false',1);
		$jsonview->json_name_object('key-properties',"{ \"type\" : \"$CRYPTO\", \"key-size\" : \"$cryptosystems{$CRYPTO}{user_key_property}\", \"property\" : \"$cryptosystems{$CRYPTO}{user_key_property}\" }",1);
		$jsonview->json_name_value('identifier','email',1);
		$jsonview->json_name_object('mandatory','[ "cn", "email" ]',1);
		$jsonview->json_name_object('optional','[ "lifetime", "serialNumber" ]',1);
		$jsonview->json_name_value('version',$VERSION,0);
		$jsonview->json_end();
	} elsif ($cgi->param('action') eq 'version') {
		$jsonview->json_start('OK',1);
		$jsonview->json_name_value('protocol','1.0',1);
		$jsonview->json_name_value('application',$VERSION,1);
		$jsonview->json_name_value('description',"$PRODUCT v$VERSION",0);
		$jsonview->json_end();
	} elsif ($cgi->param('action') eq 'scsecret') {
		$jsonview->json_start('OK',1);
		$jsonview->json_name_value('secret',$SCSECRET,0);
		$jsonview->json_end();
	} else {
		print_show_err($output_style,"Error: unknown action");
	}
}

exit;
