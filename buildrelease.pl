#!/usr/bin/perl


my $release=$ARGV[0];
my $version=$ARGV[1];

my $copy="Alexandre Aufrere";
if ($release eq 'retrustme') {
	$release='enterprise';
	$copy='Alexandre Aufrere - http:\/\/ReTrust.me';
}

my $target = "/tmp/nslightpki-$version";

$target = "/tmp/nktrustpki-$version" if ($release eq 'enterprise');
$target = "/tmp/retrustme-pki-$version" if ($copy =~ m/retrust/i);

system("mkdir -p $target/dist");
system("mkdir -p $target/dist/lib");
system("mkdir -p $target/dist/dlib/JSON");
system("mkdir -p $target/dist/bin");
system("mkdir -p $target/dist/www/inc");
system("mkdir -p $target/dist/slib");
system("mkdir -p $target/dist/i18n");
system("mkdir -p $target/dist/etc");
system("mkdir -p $target/dist/RPMS.6");
system("mkdir -p $target/dist/RPMS.7");
system("cp ./cgi-bin/index.cgi $target/dist/");
system("cp ./cgi-bin/tsa.cgi $target/dist/");
system("cp ./cgi-bin/crmp.cgi $target/dist/");
system("cp ./cgi-bin/acme.cgi $target/dist/");
system("cp ./cgi-bin/pkiclient.exe $target/dist/");
system("cp ./EnrollmentClientInstaller.exe $target/dist/");
system("cp ./etc/custom_profile.cnf $target/dist/etc/");
system("cp ./etc/NSConfig.pm $target/dist/etc/");
system("cp ./rtmpki-install.pl $target/dist/");
system("cp ./rtmpki-upgrade.pl $target/dist/");
system("cp ./etc/retrustmepki.conf $target/dist/etc/");
system("cp ./etc/retrustmepki_cert.conf $target/dist/etc/");
system("cp ./etc/retrustmepki_cert.conf.rhel7 $target/dist/etc/");
system("cp ./etc/ssl.conf $target/dist/etc/");
system("cp ./etc/ssl.conf.rhel7 $target/dist/etc/");
system("cp ./etc/retrustmepki.crond $target/dist/etc/");
system("cp ./bin/*.pl $target/dist/bin/");
system("cp ./www/inc/* $target/dist/www/inc/");
system("cp ./lib/NikoSoft/PKI/* $target/dist/lib/");
system("cp ./lib/NikoSoft/I18N/* $target/dist/i18n/");
system("cp -r ./lib/JSON/* $target/dist/dlib/JSON/");
system("cp ./lib/NikoSoft/Service/* $target/dist/slib/") if ($release eq 'enterprise');
system("cp ./etc/pki.initd $target/dist/etc/") if ($release eq 'enterprise');
system("cp ./RPMS/6/* $target/dist/RPMS.6/");
system("cp ./RPMS/7/* $target/dist/RPMS.7/");

system("perl -pi -e 's/Alexandre Aufrere/$copy/g' $target/dist/* 2>/dev/null");
system("perl -pi -e 's/Alexandre Aufrere/$copy/g' $target/dist/bin/*");
system("perl -pi -e 's/Alexandre Aufrere/$copy/g' $target/dist/lib/*");
system("perl -pi -e 's/Alexandre Aufrere/$copy/g' $target/dist/slib/*");
system("perl -pi -e 's/Alexandre Aufrere/$copy/g' $target/dist/i18n/*");

my @readmes=glob("README*");
my $manlist='';
foreach my $readme (@readmes) {
	system("cp $readme ./build/ && cd ./build && ./gendoc.sh $readme $target '$copy' && rm $readme");
	my $manual = $readme;
	$manual =~ s/README/MANUAL/;
	$manlist.="$manual\n";
	system("mv $target/$readme.pdf $target/$manual.pdf");
}
system("rm -f $target/MANUAL.tmp.pdf");
$manlist.="\n\nCopyright (c) 2014-2018 Alexandre Aufrere. All Rights Reserved.";
system("cp -f README README.tmp");
system("echo \"$manlist\" >> README.tmp");
system("./gendoc.sh README.tmp $target '$copy'");
system("mv -f $target/README.tmp.pdf $target/MANUAL.pdf");

system("rm $target/dist/bin/services.pl") unless ($release eq 'enterprise');
system("rm $target/dist/bin/support-tools.pl") unless ($release eq 'enterprise');
system("rm $target/MANUAL.PROTECTSERVER.pdf") unless ($release eq 'enterprise');

my $installername='nspki-';
$installername='nktrustpki-' if ($release eq 'enterprise');
$installername='retrustme-pki-' if ($copy =~ m/retrust/i);

open INST, ">$target/$installername"."install";
print INST<<__EOF__;
#!/bin/sh

yum install -y -q perl perl-Sys-Syslog
cd dist
./rtmpki-install.pl \$*
__EOF__
close INST;
chmod (0755, "$target/$installername"."install");

open INST, ">$target/$installername"."uninstall";
print INST<<__EOF__;
#!/bin/sh

rm -rf /opt/rtmpki
yum remove nkssl nkssl-libp11
__EOF__
close INST;
chmod (0755, "$target/$installername"."uninstall");


open INST, ">$target/$installername"."upgrade";
print INST<<__EOF__;
#!/bin/sh

cd dist
./rtmpki-upgrade.pl \$*
__EOF__
close INST;
chmod (0755, "$target/$installername"."upgrade");

system("perl -pi -e 's/__DIST_TYPE__/$release/g' $target/dist/rtmpki-install.pl");
system("perl -pi -e 's/__DIST_TYPE__/$release/g' $target/dist/rtmpki-upgrade.pl");
system("perl -pi -e 's/__DIST_PRODUCT__/ReTrustMe PKI/g' $target/dist/etc/NSConfig.pm") if ($copy =~ m/retrust/i);
system("perl -pi -e 's/__DIST_PRODUCT__/NKTrust PKI/g' $target/dist/etc/NSConfig.pm") if ($release eq 'enterprise');
system("perl -pi -e 's/__DIST_PRODUCT__/NikoSoft PKI/g' $target/dist/etc/NSConfig.pm");

$target=~s/\/tmp\///g;
chdir("/tmp");
#system("tar cvzf $target.tar.gz $target");
system("mkisofs -o $target.iso -r $target");
