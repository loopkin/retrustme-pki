BEGIN {
	push @INC, "../lib";
	push @INC, "../";
	push @INC, "../etc";
}

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";

use Test::More;

chdir('..');
my $cwd=`pwd`;
$cwd=~s/\n//g;
chdir('./lib/NikoSoft/PKI');
my @pms=glob('*.pm');

plan tests => scalar(@pms);


foreach $pm (@pms) {
	$pm=~s/\.pm//g;
	system("PERL5LIB=$cwd/lib:$cwd /usr/bin/perl -e \"use NikoSoft::PKI::$pm\"  2>&1");
	$ret = $?;
	ok ( $ret == 0, "Testing loading of $pm");
}

done_testing();
