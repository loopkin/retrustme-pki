BEGIN {
	push @INC, "../lib";
	push @INC, "../";
	push @INC, "../etc";
}

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";

use Test::More;
use NikoSoft::PKI::AccessControl;
use utf8;

my $ac=NikoSoft::PKI::AccessControl->new();

plan tests => 2;

ok ( 1 == 1, "Dummy Test" );
ok ( $ac->is_connected_user_admin() == 0, "Connected user isn't administrator");


done_testing();
