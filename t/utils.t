BEGIN {
	push @INC, "../lib";
	push @INC, "../";
	push @INC, "../etc";
}

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";

use Test::More;
use NikoSoft::PKI::Utils qw(asn1_time utf8_print print_show_err dumpfile sendmail validate_data time_to_asn1_time);
use utf8;

plan tests => 9;

ok ( 1 == 1, "Dummy Test" );
ok ( asn1_time("160829094210Z") eq '2016-08-29 09:42', "ASN1 Time to Human Readable String");
ok ( time_to_asn1_time((10,42,9,29,7,116,1,155,0)) eq '160829094210Z', "Perl gmtime() to ASN1 Time");
ok ( utf8_print('\xD0\x98\xD0\xB2\xD0\xB0\xD0\xBD') eq 'Иван', "OpenSSL encoded UTF8 to UTF8");
ok ( validate_data('alexandre@example.com','email'), "Validating valid email");
ok ( !validate_data('alexandre(plop)@example.com','email'), "Validating invalid email");
ok ( validate_data('P@ssw0rd','password'), "Validating valid password");
ok ( !validate_data('pwd','password'), "Validating invalid password");
ok ( !validate_data('pwd','pkcs10'), "Validating invalid PKCS10");


done_testing();
