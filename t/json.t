BEGIN {
	push @INC, "../lib";
	push @INC, "../";
	push @INC, "../etc";
}

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";

use Test::More;
use NikoSoft::PKI::JSONView;
use utf8;

my $output='';
sub capture {
	open TOOUTPUT, '>', \$output or die "Can't open TOOUTPUT: $!";
	select TOOUTPUT;
}

sub end_capture {
	close TOOUTPUT;
	select STDOUT;
}

plan tests => 3;

my $json=NikoSoft::PKI::JSONView->new();

ok ( 1 == 1, "Dummy Test" );

capture();
$json->json_start('OK',0);
$json->json_end();
end_capture;
$output=~ s/\n//g;
$output=~ s/\t//g;
ok ( $output eq '{"status":"OK"}', "JSON simple output: OK");


capture();
$json->json_start('OK',1);
$json->json_name_value('name','value',0);
$json->json_end();
end_capture;
$output=~ s/\n//g;
$output=~ s/\t//g;
ok ( $output eq '{"status":"OK","name":"value"}', "JSON complex output: OK");

done_testing();
