BEGIN {
	push @INC, "../lib";
	push @INC, "../";
	push @INC, "../etc";
}

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";

use Test::More;

chdir('..');
my @bins=glob('./bin/*.pl');

plan tests => scalar(@bins);

foreach $bin (@bins) {
	system("$bin --help >/dev/null 2>&1");
	$ret = $?;
	ok ( $ret == 0, "Testing exec of $bin");
}
