#!/usr/bin/perl

use Test::More;
use MIME::Base64;

use_ok('JSON::XS');

$password='password';
$uri='devpki';

$json_xs_version=`perl -MJSON::XS -e 'print "$JSON::XS::VERSION";'`;

$version=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=version" 2>/dev/null | json_xs -f json -t json-pretty`;
print "PKI Version: $version\n";
$list_profiles=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=list_profiles" 2>/dev/null | json_xs -f json -t json-pretty`;
print "Profiles available: $list_profiles\n";
if ($json_xs_version>3) {
	$serial=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=enroll&cn=TestConnector&email=alexandre\@aufrere.org&pwd=test123&crt_type=auth_cert" 2>/dev/null | json_xs -f json -t none -e 'print \$_->{serial}'`;
	print "Generated cert as P12 with serial $serial\n";
	$ptenserial=`wget --no-check-certificate --user=admin --password=$password -O - --post-file=./params.post "https://localhost/$uri/" 2>/dev/null | json_xs -f json -t none -e 'print \$_->{serial}'`;
	unless ($ptenserial) {
		$ptenserial='\n#### ERROR NO CERT GENERATED ####\n';
		$temp=`wget --no-check-certificate --user=admin --password=$password -O - --post-file=./params.post "https://localhost/$uri/" 2>/dev/null`;
		print $temp;
	}
	print "Generated cert as P7 with serial $ptenserial\n";
}
$incorrect_profile=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=enroll&cn=TestConnector&email=alexandre\@aufrere.org&pwd=test123&crt_type=ath_cert" 2>/dev/null | json_xs -f json -t json-pretty`;
print "Incorrect profile try: $incorrect_profile";
$list_certs=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=search&search_dn=*&search_revoked=0" 2>/dev/null | json_xs -f json -t json-pretty`;
print "Certificates valid: $list_certs";
if ($json_xs_version>3) {
	$result=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=revoke&rev_serial=$serial" 2>/dev/null | json_xs -f json -t none -e 'print \$_->{status}'`;
	print "Revocation: $result\n";
	$result=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=revoke&rev_serial=$ptenserial" 2>/dev/null | json_xs -f json -t none -e 'print \$_->{status}'`;
	print "Revocation: $result\n";
	$result=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=crlgen" 2>/dev/null | json_xs -f json -t none -e 'print \$_->{status}'`;
	print "CRL generation: $result\n";
}

`perl -MMIME::Base64 -e "print decode_base64('MEECAQEwMTANBglghkgBZQMEAgEFAAQg24FdxrD0h7cX/rxYsV2e/try4OGDZoI9RTM7lDwDeq8CCQCxOCQyVLXFBA==');" >/tmp/tmp.tsq`;
`curl --data-binary @/tmp/tmp.tsq  http://localhost/devtsa/ 2>/dev/null >/tmp/tmp.tsr`;
$tsa_test=`openssl ts -reply -in /tmp/tmp.tsr -text`;
print $tsa_test;

$mrtd=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=mrtd&cn=Aufrere%20Alexandre&serial=1234567890" 2>/dev/null | json_xs -f json -t json-pretty`;
print "MRTD: $mrtd";

$log=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=search_logs&outcome=E" 2>/dev/null | json_xs -f json -t json-pretty`;
print "LOG: $log";

$scep=`wget --no-check-certificate --user=admin --password=$password -O - "https://localhost/$uri/?direct=1&action=scep_challenge&outcome=E" 2>/dev/null | json_xs -f json -t json-pretty`;
print "SCEP Challenge: $scep";

done_testing();
