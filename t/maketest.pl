#!/usr/bin/perl -w

sub _exit_error {
	print "\nERROR WHILE TESTING.\n";
	exit;
}

system("cp ../etc/NSConfig.pm ../");
print "Making unitary tests\n";
system("perl load_modules.t");
_exit_error if ($?!=0);
system("perl crypto.t");
_exit_error if ($?!=0);
system("perl utils.t");
_exit_error if ($?!=0);
system("perl json.t");
_exit_error if ($?!=0);
system("perl bins.t");
_exit_error if ($?!=0);
system("perl ac.t");
_exit_error if ($?!=0);
print "Making API tests\n";
system("perl apitest.t");
system("rm -f ../NSConfig.pm");
