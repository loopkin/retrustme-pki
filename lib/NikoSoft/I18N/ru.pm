package NikoSoft::I18N::ru;

# Translation (c) 2014 Anastasia Ditenkova, published under the GPL v3

BEGIN { use Exporter 'import'; @EXPORT = qw(_T); @EXPORT_OK = qw(_T); }
use strict;
use utf8;
our %L10N=(

HOME=>"Главная страница",
CA_MANAGEMENT=>"Управление УЦ",
SETTINGS=>"Настройки",
ABOUT=>"О продукте",
LOGGED_USER=>"Пользователь",

SEARCH_CERT=>"Поиск сертификата",
DN=>'DN (имя пользователя, сервера или адрес эл. почты)',
SHOW_REVOKED=>'Показать отозванные сертификаты',
SUBMIT=>'Подтвердить',

REQUEST_CERTIFICATE=>"Запросить сертификат",
CN=>'CN (имя пользователя или сервера)',
EMAIL=>'Эл. почта владельца сертификата',
PKCS12_PASSWORD=>'Пароль для PKCS#12',
CONFIRM_PASSWORD=>"Подтвердить",
CERTIFICATE_TYPE=>"Тип сертификата",
DOWNLOAD_PKCS12=>"Скачать файл PKCS#12 для",
CERTIFICATE_SIGN=>"Подпись сертификата",

LIST_CERTIFICATES=>"Список сертификатов",
SEARCH_STRING=>"Строка поиска",
STATUS=>"Статус",
SERIAL=>"Серийный номер",
EXPIRATION_DATE=>"Дата истечения срока действия",
REVOCATION_DATE=>"Дата отзыва",
SUBJECT_DN=>"Subject DN",

VIEW_CERT=>"Просмотреть сертификат #",
REVOKE_CERT=>"Отозвать сертификат",
GENERATION_CRL=>"Сгенерировать CRL",

SERVER_PROFILE=>'Аутентификация сервера(SSL/TLS)',
CLIENT_SIGN_PROFILE=>'Аутентификация клиента и подпись',
SIGN_PROFILE=>'Подпись',
TIMESTAMP_PROFILE=>'Метка времени',
CODE_SIGN_PROFILE=>'Подпись кода',
CA_PROFILE=>'Удостоверяющий Центр',
ENCRYPTION_PROFILE=>'Шифрование',

CA_CERTIFICATE=>'Сертификат УЦ',
CA_CRL=>'CRL УЦ',
CA_CSR=>'CSR УЦ',
CRL_GENERATE=>'Сгенерировать новый CRL',
PLEASE_CLICK_TO_DOWNLOAD=>"Пожалуйста, нажмите ниже для скачивания",

TIP_CERT_SEARCH=>"Используйте данную форму поиска для поиска сертификата.<br/>Вы можете использовать символ \"*\" для упрощения поиска.<br/>Чтобы отозвать сертификат, необходимо его найти.",
TIP_CERT_REQ=>"Используйте данную форму для запроса сертификата. Сертификат будет выпущен в формате PKCS#12.",
TIP_CERT_SEARCH_RESULT=>"Чтобы просмотреть детали сертификата, пожалуйста, нажмите на него.",
TIP_CERT_VIEW=>"Дойдите до конца страницы, чтобы отозвать сертификат.",
TIP_CRLDP=>"Вы можете скопировать ссылку на CRL, чтобы использовать его в приложении, которое будет получать CRL через HTTP",

ERROR=>'Ошибка',
ERR_MISSING_CRT_TYPE=>"пропущен параметр crt_type",
ERR_MISSING=>"пропущен или некорректен параметр",
ERR_MAX_NB_CERT=>"Максимальное количество сертификатов для этого DN достигнуто",
ERR_INCORRECT_PROFILE=>"Некорректный идентификатор профиля сертификата",
ERR_CRT_NOT_FOUND=>"Не может быть найден сертификат с серийным номером:"

);
sub _T{ my $s=shift; return $s unless (%L10N); my $r=$L10N{$s} || $s; return $r; }
1;

