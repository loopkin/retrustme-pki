package NikoSoft::I18N::fr;

BEGIN { use Exporter 'import'; @EXPORT = qw(_T); @EXPORT_OK = qw(_T); }
use strict;
use utf8;
our %L10N=(

HOME=>"Accueil",
CA_MANAGEMENT=>"Gestion d'AC",
SETTINGS=>"Paramètres",
ABOUT=>"A Propos",
LOGGED_USER=>"Utilisateur",

SEARCH_CERT=>"Recherche de certificats",
DN=>'DN (nom, nom DNS, ou email)',
SHOW_REVOKED=>'Montrer les certificats révoqués',
SUBMIT=>'Soumettre',

REQUEST_CERTIFICATE=>"Demande de Certificat",
CN=>'CN (prénom nom, ou nom DNS)',
EMAIL=>'Email (ou MSUPN) du porteur',
PKCS10=>'CSR (PKCS#10)',
PKCS12_PASSWORD=>'Mot de Passe PKCS#12',
CONFIRM_PASSWORD=>"Confirmer le Mot de Passe",
CERTIFICATE_TYPE=>"Type de Certificat",
DOWNLOAD_PKCS12=>"Télécharger le fichier PKCS#12 pour",
CERTIFICATE_SIGN=>"Signature du Certificat",

LIST_CERTIFICATES=>"Liste des Certificats",
SEARCH_STRING=>"Recherche",
STATUS=>"Etat",
SERIAL=>"Numéro de Série",
EXPIRATION_DATE=>"Date d'Expiration",
REVOCATION_DATE=>"Date de Révocation",
SUBJECT_DN=>"Sujet",

VIEW_CERT=>"Affichage du Certificat N°",
REVOKE_CERT=>"Revoquer le(s) Certificat(s)",
REVOKE_CERT_CONFIRM=>"Etes vous sûr de vouloir révoquer ce(s) certiciat(s) ?",
GENERATING_CRL=>"Génération de la CRL",

SERVER_PROFILE=>'Authentification Serveur (SSL/TLS)',
CLIENT_SIGN_PROFILE=>'Authentification et Signature Client',
SIGN_PROFILE=>'Signature',
TIMESTAMP_PROFILE=>'Horodatage',
CODE_SIGN_PROFILE=>'Signature de Code',
CA_PROFILE=>'Autorité de Certification',
ENCRYPTION_PROFILE=>'Chiffrement',

CA_CERTIFICATE=>"Certificat d'AC",
CA_CRL=>"LCR de l'AC (CRL)",
CA_CSR=>"CSR de l'AC",
CRL_GENERATE=>"Générer une nouvelle CRL",
PLEASE_CLICK_TO_DOWNLOAD=>"Cliquez ci-dessous pour télécharger",

TIP_CERT_SEARCH=>"Utilisez ce formulaire pour rechercher un certificat.<br/>Vous pouvez utiliser le caractère joker \"*\" pour faciliter votre recherche.<br/>Pour révoquer un certificat, commencez par le chercher.",
TIP_CERT_REQ=>"Utilisez ce formulaire pour émettre un certificat. Il sera émis au format PKCS#12, sauf si vous cochez la cas 'PKCS#10'. Si vous cochez cette case, une CSR au format PEM/Base64 doit être fournie.<br/>NB: le champ 'email' est interprété en tant que 'MSUPN' pour le profil 'MS SmartCard Logon'.<br/>NB : Le mot de passe doit faire 6 caractères minimum et contenir des chiffres et des lettres.",
TIP_CERT_SEARCH_RESULT=>"Pour voir les détails d'un certificat, cliquez sur son sujet.",
TIP_CERT_VIEW=>"Descendez à la fin de la page si vous voulez révoquer ce certificat.",
TIP_CRLDP=>"Vous pouvez copier le lien vers la LCR de l'AC afin de l'utiliser dans une application, si nécessaire.",

SUCCESS=>'Opération Réussie',

ERROR=>'Erreur',
ERR_MISSING_CRT_TYPE=>"paramètre absent : crt_type",
ERR_MISSING=>"paramètre incorrect ou absent :",
ERR_MAX_NB_CERT=>"Nombre maximum de certificats atteint pour ce DN",
ERR_INCORRECT_PROFILE=>"Identifiant de profile de certificat incorrect",
ERR_CRT_NOT_FOUND=>"Impossible de trouver le certificat N°"


);
sub _T{ my $s=shift; return $s unless (%L10N); my $r=$L10N{$s} || $s; return $r; }
1;
