package NikoSoft::I18N::en;

BEGIN { use Exporter 'import'; @EXPORT = qw(_T); @EXPORT_OK = qw(_T); }
use strict;
our %L10N=(

HOME=>"Home",
CA_MANAGEMENT=>"CA Management",
SETTINGS=>"Settings",
ABOUT=>"About",
LOGGED_USER=>"Logged User",

SEARCH_CERT=>"Search for Certificate",
DN=>'DN (user, server name or email)',
SHOW_REVOKED=>'Show revoked certificates',
SUBMIT=>'Submit',

REQUEST_CERTIFICATE=>"Request Certificate",
CN=>'CN (user or server name)',
EMAIL=>'Email (or MSUPN) of holder',
PKCS10=>'CSR (PKCS#10)',
PKCS12_PASSWORD=>'PKCS#12 Password',
CONFIRM_PASSWORD=>"Confirm Password",
CERTIFICATE_TYPE=>"Certificate Type",
DOWNLOAD_PKCS12=>"Download PKCS#12 file for",
CERTIFICATE_SIGN=>"Certificate Signature",

LIST_CERTIFICATES=>"List of Certificates",
SEARCH_STRING=>"Search String",
STATUS=>"Status",
SERIAL=>"Serial",
EXPIRATION_DATE=>"Expiration Date",
REVOCATION_DATE=>"Revocation Date",
SUBJECT_DN=>"Subject DN",

VIEW_CERT=>"Viewing Certificate #",
REVOKE_CERT=>"Revoke Certificate(s)",
REVOKE_CERT_CONFIRM=>"Are you sure you want to revoke this certificate(s)?",
GENERATION_CRL=>"Generating CRL",

SERVER_PROFILE=>'Server Authentication (SSL/TLS)',
CLIENT_SIGN_PROFILE=>'Client Authentication and Signature',
SIGN_PROFILE=>'Signature',
TIMESTAMP_PROFILE=>'Timestamping',
CODE_SIGN_PROFILE=>'Code Signing',
CA_PROFILE=>'Certification Authority',
ENCRYPTION_PROFILE=>'Encryption',

CA_CERTIFICATE=>'CA Certificate',
CA_CRL=>'CA CRL',
CA_CSR=>'CA CSR',
CRL_GENERATE=>'Generate a new CRL',
PLEASE_CLICK_TO_DOWNLOAD=>"Please click below to download",

TIP_CERT_SEARCH=>"Use this search form to search for certificates.<br/>You can use the wildcard \"*\" character to ease your search.<br/>To revoke a certificate, first search for it.",
TIP_CERT_REQ=>"Use this form to request a certificate. It will be issued in PKCS#12 format, unless you tick the 'PKCS#10' box. If you tick this box, then the Certificate Sign Request, in PEM/Base64 format, will have to be provided.<br/>Note: the 'email' field is intepreted as 'MSUPN' for the MS SmartCard Logon profile.<br/>Note: The password should be at least 6 characters long and include both letters and digits.",
TIP_CERT_SEARCH_RESULT=>"To see a certificate's detail, please click on its subject.",
TIP_CERT_VIEW=>"Browse till end of page to be able to revoke the displayed certificate.",
TIP_CRLDP=>"You can copy the CA CRL link to use it in application that can access the CRLDP through HTTP",

SUCCESS=>'Operation Successful',

ERROR=>'Error',
ERR_MISSING_CRT_TYPE=>"missing parameter crt_type",
ERR_MISSING=>"incorrect or missing parameter",
ERR_MAX_NB_CERT=>"Maximum number of certificate reached for this DN",
ERR_INCORRECT_PROFILE=>"Incorrect certificate profile identifier",
ERR_CRT_NOT_FOUND=>"Could not find certificate with serial:"

);
sub _T{ my $s=shift; return $s unless (%L10N); my $r=$L10N{$s} || $s; return $r; }
1;
