##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: InternalEntities.pm 395 2017-04-12 10:30:28Z loopkin $
##

=head1 NAME

NikoSoft::PKI::InternalEntities

=cut

package NikoSoft::PKI::InternalEntities;

BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
}

use strict;
use NSConfig;
use NikoSoft::PKI::CA;
use NikoSoft::PKI::Config;
use NikoSoft::PKI::Log;
use NikoSoft::PKI::TSA;
use NikoSoft::PKI::SCEP;
use NikoSoft::PKI::CryptoTools qw(exec_openssl);

=head1 METHODS

=head2 new

Constructor for NikoSoft::PKI::InternalEntities

=cut
sub new {
	my $class = shift;
	my $self= {};
	bless $self, $class;
	return $self;
}

sub _string2file {
	 my ($string,$filename)=@_;
	 open FIL, ">$filename";
	 print FIL $string;
	 close FIL;
}

sub _gen_key_and_cert {
	my $profile = shift;
	my $keyfile = shift;
	my $certfile = shift;

	my $config=NikoSoft::PKI::Config->new();
	my $log=NikoSoft::PKI::Log->new();
	my $ca=NikoSoft::PKI::CA->new($config,$log);
	$ca->set_output_mode(1);

	my %dn_params;
	$dn_params{cn}=$CAHOSTNAME;
	$dn_params{email}=$config->data('adminmail');
	my ($key,$csr)=$ca->gen_key_and_csr($profile,$CAHOSTNAME,$config->data('adminmail'));
	exit unless($csr);
	my ($cert,$certserial)=$ca->sign_csr($csr,$profile,\%dn_params,1);

	if (($key)&&($cert)) {
		_string2file($key,$keyfile);
		_string2file($cert,$certfile);
	}
}

=head2 copy_www_certificate

Copies the WWW certificate to its place for use by Apache.

=cut
sub copy_www_certificate {
	my $self = shift;
        my $user=`/usr/bin/id -u`;
        $user =~ s/\n//g;
	my $sslkeyfile='/etc/pki/tls/private/localhost.key';
	my $sslcertfile='/etc/pki/tls/certs/localhost.crt';
	my $sslcafile='/etc/pki/tls/certs/server-chain.crt';
        if (($user==0)&&(-f "$CAPATH/private/web.key")) {
		system("/bin/cp -f \"$CAPATH/private/web.key\" \"$sslkeyfile\" >/dev/null 2>&1");
		system("/bin/cp -f \"$CAPATH/web.crt\" \"$sslcertfile\" >/dev/null 2>&1");	
		system("/bin/cp -f \"$CAPATH/ca.crt\" \"$sslcafile\" >/dev/null 2>&1");
		system("/bin/cp $CAPATH/ca.crl /etc/pki/tls/certs/pki.crl  >/dev/null 2>&1");
		system("/bin/chown $APACHEUSER /etc/pki/tls/certs/pki.crl  >/dev/null 2>&1");
		if ((-d "/etc/httpd") && (-d "/etc/httpd/conf.d")) {
			system("/usr/bin/perl -pi -e 's/\#BOOTSTRAP//g' /etc/httpd/conf.d/ssl.conf");
			system("/usr/bin/perl -pi -e 's/\#BOOTSTRAP//g' /etc/httpd/conf.d/retrustmepki.conf");
		}
	} else {
		system("/bin/cp -f \"$sslcertfile\" \"$sslcafile\" >/dev/null 2>&1");
	}
	chmod(0440, $sslkeyfile); 
}

=head2 gen_internal_entities ($tsa_oid)

Generates the internal entities certificates

	$tsa_oid - OID of the TS Policy (optional: if not provided then TSA not generated)

=cut
sub gen_internal_entities {
	my $self = shift;
	my $tsa_oid = shift;
	my $renew = shift;

	# SSL certificates
	my $keyfile="$CAPATH/private/web.key";
	my $certfile="$CAPATH/web.crt";
	_gen_key_and_cert('serv_cert',$keyfile,$certfile);
	# This is ugly, but using GOST for SSL would require recompiling mod_ssl
	if ($CRYPTO eq 'gost') {
		exec_openssl(2,"req -new -newkey rsa:2048 -days 3650 -nodes -subj \"/CN=$CAHOSTNAME\" -x509 -extensions serv_cert -keyout \"$keyfile\" -out \"$certfile\" 2>&1","MSUPN=\"noname\@example.com\" DNS=\"$CAHOSTNAME\"");
	}
	print "\nSSL web server key/certificate generated, and installed as $keyfile and $certfile.\n";
	copy_www_certificate();


	# Signing Certificate
	# print "\nNow generating log signing key and certificate.\n";
	# _gen_key_and_cert('auth_cert',"$CAPATH/private/sign.key", "$CAPATH/sign.crt");

	my $config=NikoSoft::PKI::Config->new();
	my $log=NikoSoft::PKI::Log->new();
	print "\nNow generating SCEP messages signing key and certificate.\n";
	my $scep=NikoSoft::PKI::SCEP->new($config,$log);
	$scep->initiate_ops() or exit();
	$scep->gen_scep();
	
	if ($tsa_oid) {
		print "\nNow generating TSA key and certificate.\n";	
		my $tsa=NikoSoft::PKI::TSA->new($config);
		$tsa->initiate_ops() or exit();
		$tsa->gen_tsa($tsa_oid, $renew) ;
	}
	
	

	chmod(0440, "$CAPATH/private/scep.key"); 
	# chmod(0440, "$CAPATH/private/sign.key"); 
	chmod(0440, "$CAPATH/private/web.key"); 
}

1;
