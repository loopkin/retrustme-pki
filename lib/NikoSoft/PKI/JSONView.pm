##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: JSONView.pm 378 2015-12-14 21:18:02Z loopkin $
##

=head1 NAME

NikoSoft::PKI::JSONView

=cut

package NikoSoft::PKI::JSONView;

BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
}

use strict;

use NikoSoft::PKI::CA;
use NikoSoft::PKI::Utils qw(asn1_time utf8_print print_show_err);
use NikoSoft::PKI::Config;
use NSConfig;

=head1 METHODS

=head2 new ( $cgi, $config )

Constructor. $cgi is an instance of CGI.pm and $config an instance of NikoSoft::PKI::Config

=cut
sub new {
	my $class = shift;
	my ($cgi,$config) = @_;
	my $self  = bless { cgi => $cgi, config => $config }, $class;
	return $self;
}

=head2 json_start ($status, $isnextrow)

Starts writing a JSON reply

	$status - the status of the reply ('OK', 'error',...)
	$isnextrow - will there be another row of JSON information after this message?

=cut
sub json_start {
	my $self = shift;
	my $status=shift;
	my $isnextrow=shift;
	my $comma='';
	$comma=',' if ($isnextrow);
	print "{\n\t\"status\":\"$status\"$comma\n";
}

=head2 json_start_empty ()

Starts writing a JSON reply

=cut
sub json_start_empty {
	my $self = shift;
	print "{\n\t\n";
}

=head2 json_name_value ($name, $value, $isnextrow)

Writes a row of JSON information, in name/value pair format corresponding to the property to be written

	$name - name of the property (e.g. 'serial')
	$value - value of the property (e.g. '123456')
	$isnextrow - will there be another row of JSON information after this message?

=cut
sub json_name_value {
	my $self = shift;
	my $name = shift;
	my $value = shift;
	my $isnextrow=shift;
	my $comma='';
	$comma=',' if ($isnextrow);
	$value=~s/\n/\\n/g;
	print "\t\"$name\":\"$value\"$comma\n";
}

=head2 json_name_object ($name, $object, $isnextrow)

Writes a row of JSON information, in name/object pair format corresponding to the property to be written

	$name - name of the property (e.g. 'serial')
	$object - value of the property (e.g. '[]')
	$isnextrow - will there be another row of JSON information after this message?

=cut
sub json_name_object {
	my $self = shift;
	my $name = shift;
	my $value = shift;
	my $isnextrow=shift;
	my $comma='';
	$comma=',' if ($isnextrow);
	$value=~s/\n/\\n/g;
	$value=~s/\"\,\"/\"\,\n\t\t\"/g;
	print "\t\"$name\":$value$comma\n";
}

=head2 json_end

Finishes writing a JSON output

=cut
sub json_end {
	my $self = shift;
	print "}\n";
}


1;
