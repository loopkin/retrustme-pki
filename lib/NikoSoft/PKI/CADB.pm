##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: CADB.pm 377 2015-12-14 20:22:07Z loopkin $
##

=head1 NAME

NikoSoft::PKI::CADB

=cut

package NikoSoft::PKI::CADB;

=head1 DESCRIPTION

This module handles the access to the CA Database (certificate repository). Current version works using DBI and DBD::SQLite, or DBD::Pg for Enterprise version (autoselect depending on conf.)

=cut

BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
}

use strict;
use bigint;
use DBI;
use NSConfig;
use utf8;
use Encode qw (encode decode);
use NikoSoft::PKI::Log;

our  $DBFILE="pkidb.sqlite";

=head1 METHODS

=head2 new

Constructor for the NikoSoft::PKI::CADB module. Creates the database schema if the database does not exist.

NB: this class has a destructor, in order to manage DB disconnection.

=cut
sub new {
	my $class = shift;
	my $log = shift;
	my $self = {};
	$self->{log}=$log;
	if ($TYPE eq 'enterprise') {
		my $pgrun = "$INSTALLDIR/var/dbrun";
		$self->{dbh} = DBI->connect("dbi:Pg:dbname=pkica;host=$pgrun","pki","", 
						{AutoCommit => 1, RaiseError=>0, PrintError=>0, Warn=>0}) or die $DBI::errstr;
		bless $self, $class;
		return $self;
	}
	mkdir("$CAPATH") if (!(-f $CAPATH));
	if (!(-f "$CAPATH/$DBFILE")) {
		my $dbh = DBI->connect("dbi:SQLite:dbname=$CAPATH/$DBFILE","","") or die $DBI::errstr;
		$dbh->do("PRAGMA encoding = 'UTF-8'");
		my $sth=$dbh->prepare("CREATE TABLE cert_serial_seq (value INTEGER)");
		$sth->execute();
		$sth=$dbh->prepare("INSERT INTO cert_serial_seq VALUES(0)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE TABLE certificate (serial INTEGER PRIMARY KEY, dn TEXT NOT NULL, email TEXT NOT NULL, expiration TEXT NOT NULL, revocation TEXT, status TEXT NOT NULL, profile TEXT, pem TEXT)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_status ON certificate(status)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_profile ON certificate(profile)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_serial ON certificate(serial)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_dn ON certificate(dn)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_email ON certificate(email)");
		$sth->execute();
		$dbh->disconnect();
	}
	$self->{dbh} = DBI->connect("dbi:SQLite:dbname=$CAPATH/$DBFILE","","", { AutoCommit=>1, RaiseError=>0 } ) or die $DBI::errstr;
	$self->{dbh}->{sqlite_unicode} = 1;
	bless $self, $class;
	return $self;
}

sub DESTROY {
	my $self = shift;
	my $dbh = $self->{dbh};
	$dbh->disconnect() if ($dbh);
}


=head2 get_next_serial

Gets the next serial number from the DB sequence, and returns it as a decimal integer.

=cut
sub get_next_serial {
	my $self = shift;
	my $dbh = $self->{dbh};
	if ($TYPE eq 'enterprise') {
		my @rv=$dbh->selectrow_array("SELECT nextval('cert_serial_seq')");
		return $rv[0];
	}
	my $sth=$dbh->prepare("UPDATE cert_serial_seq set value=value+1");
	return $self->_error() unless ($sth);
	$sth->execute() or return $self->_error();
	my @rv=$dbh->selectrow_array("SELECT value FROM cert_serial_seq");
	return $rv[0];
}

=head2 insert_certificate ( $serial, $dn, $expiration, $profile, $pem )

Inserts a certificate into the CA Database.

	$serial - serial number of the certificate, in hexadecimal (it is stored in DB as decimal integer)
	$dn - Subject DN of the certificate to store, in UTF8 String
	$expiration - Expiration date of the certificate to store, in ASN1 UTCTime format.
	$profile - Name of the certificate profile used to issue the certificate
	$pem - X509 Certificate, in PKCS#7 PEM format

=cut
sub insert_certificate {
	my $self = shift;
	my ($serial,$dn,$email,$expiration,$profile,$pem)=@_;
	my $dbh = $self->{dbh};
	my $sth=$dbh->prepare("INSERT INTO certificate(serial,dn,email,expiration,profile,status,pem) VALUES(?,?,?,?,?,?,?)");
	return $self->_error() unless ($sth);
	$sth->execute(hex($serial),$dn,$email,$expiration,$profile,"V",$pem) or return $self->_error();
	return $self->_error() if ($sth->err);
	return 1;
}

=head2 revoke_certificate ( $serial, $rev_date )

Update certificate record in CA Database, by putting its status as revoked, and adding a revocation date. 

	$serial - Serial number of the certificate whose record is to be updated
	$rev_date - Revocation date

=cut
sub revoke_certificate {
	my $self = shift;
	my ($serial,$rev_date)=@_;
	my $dbh = $self->{dbh};
	my $sth=$dbh->prepare("UPDATE certificate SET status='R' , revocation='$rev_date' WHERE serial=".hex($serial));
	return $self->_error() unless ($sth);
	$sth->execute() or return $self->_error();
	return $self->_error() if ($sth->err);
	return 1;
}

=head2 expire_certificate ( $serial )

Sets specified certificate as expired. WARNING: this doesn't check that the certificate is indeed expired.

	$serial - Serial of the certificate to set as expired.

=cut
sub expire_certificate {
	my $self = shift;
	my ($serial)=@_;
	my $dbh = $self->{dbh};
	my $sth=$dbh->prepare("UPDATE certificate SET status='E' WHERE serial=".hex($serial));
	$sth->execute() or return $self->_error();
	return $self->_error() if ($sth->err);
	return 1;
}

=head2 remove_certificate ( $serial )

Removes specified certificate from CADB. WARNING: USE WITH CAUTION!

	$serial - Serial of the certificate to remove from DB.

=cut
sub remove_certificate {
	my $self = shift;
	my ($serial)=@_;
	my $dbh = $self->{dbh};
	my $sth=$dbh->prepare("DELETE FROM certificate WHERE serial=".hex($serial));
	return $self->_error() unless ($sth);
	$sth->execute() or return $self->_error();
	return $self->_error() if ($sth->err);
	return 1;
}


=head2 list_revoked_cert

Returns the list of revoked certificates, as an Hashref. This is a convenience method, calling list_certificates.

=cut
sub list_revoked_cert {
	my $self=shift;
	return $self->list_certificates("RR");
}

=head2 list_certificates ( $status, $dn, $profile )

Returns the list of certificates corresponding to the search parameters as an Hashref with the following structure: $href->{$serial_hex}->{expiration|revocation|dn|status}

	$status - Status of certificates to look for. Can be: 'V', 'R', 'E' or 'any'
	$dn - Subject DN or part of subject DN of the certificates to look for. The search is perfomed as %$dn%
	$profile - certificate profile used to issue the certificate (optional)

=cut
sub list_certificates {
	my $self = shift;
	my ($status,$dn,$profile)=@_;
	my $query="SELECT serial,expiration,revocation,dn,status FROM certificate WHERE profile<>'op_cert'";
	$query="SELECT serial,expiration,revocation,dn,status FROM certificate WHERE 1=1" if ($status eq 'RR');
	$status='R' if ($status eq 'RR');
	$query.=" AND status='$status'" if ($status ne 'any');
	$query.=" AND dn LIKE '%$dn%'" if ($dn);
	$query.=" AND profile='$profile'" if ($profile);
	$query.=" ORDER BY expiration DESC";
	my $dbh = $self->{dbh};
	my $rv=$dbh->selectall_hashref($query,"serial");
	my $rvhex={};
	foreach my $serial (keys(%{$rv})) { 
		my $oserial=$serial;
		$serial+=0; 
		$serial=uc($serial->as_hex); 
		$serial=~s/0X//; 
		$serial='0'.$serial if (length($serial) % 2);
		$rvhex->{$serial}->{expiration}=$rv->{$oserial}->{expiration}; 
		$rvhex->{$serial}->{revocation}=$rv->{$oserial}->{revocation} ; 
		$rvhex->{$serial}->{dn}=$rv->{$oserial}->{dn}; 
		$rvhex->{$serial}->{email}=$rv->{$oserial}->{email}; 
		$rvhex->{$serial}->{status}=$rv->{$oserial}->{status}; 
	}
	return $rvhex;		
}

=head2 retrieve_certificates ( $status, $email, $profile )

Returns the list of certificates corresponding to the search parameters as an array of PEM

	$status - Status of certificates to look for. Can be: 'V', 'R', 'E'or 'A'
	$email - Email of the certificates to look for. The search is performed in exact string match
	$profile - certificate profile used to issue the certificate (optional)

=cut
sub retrieve_certificates {
	my $self = shift;
	my ($status,$email,$profile)=@_;
	my $query="SELECT pem FROM certificate WHERE profile<>'op_cert'";
	$query.=" AND status='$status'" if ($status ne 'A');
	$query.=" AND email ='$email'" if ($email);
	$query.=" AND profile='$profile'" if ($profile);
	$query.=" ORDER BY expiration DESC LIMIT 10";
	my $dbh = $self->{dbh};
	my $rv=$dbh->selectall_arrayref($query);
	my @res=();
	foreach my $item(@{$rv}) { push @res, @{$item}};
	return @res;		
}

=head2 list_op_certificates ( $status )

Returns the list of operator certificates corresponding to the search parameters as an Hashref with the following structure: $href->{$serial_hex}->{expiration|revocation|dn|status}

	$status - Status of certificates to look for. Can be: 'V', 'R', 'E' or 'any'

=cut
sub list_op_certificates {
	my $self = shift;
	my ($status)=@_;
	my $query="SELECT serial,expiration,revocation,dn,status FROM certificate WHERE profile='op_cert'";
	$query.=" AND status='$status'" if ($status ne 'any');
	$query.=" ORDER BY expiration DESC";
	my $dbh = $self->{dbh};
	my $rv=$dbh->selectall_hashref($query,"serial");
	my $rvhex={};
	foreach my $serial (keys(%{$rv})) { 
		my $oserial=$serial;
		$serial+=0; 
		$serial=uc($serial->as_hex); 
		$serial=~s/0X//; 
		$serial='0'.$serial if (length($serial) % 2);
		$rvhex->{$serial}->{expiration}=$rv->{$oserial}->{expiration}; 
		$rvhex->{$serial}->{revocation}=$rv->{$oserial}->{revocation} ; 
		$rvhex->{$serial}->{dn}=$rv->{$oserial}->{dn}; 
		$rvhex->{$serial}->{email}=$rv->{$oserial}->{email}; 
		$rvhex->{$serial}->{status}=$rv->{$oserial}->{status}; 
	}
	return $rvhex;		
}


=head2 get_certificate_by_serial ( $serial )

Fetches the certificate using its serial number, and returns it as PKCS#7 PEM encoded.

	$serial - serial number of the certificate to fetch

=cut
sub get_certificate_by_serial {
	my $self = shift;
	my ($serial)=@_;
	my $dbh = $self->{dbh};
	my @rv=$dbh->selectrow_array("SELECT pem FROM certificate WHERE serial=".hex($serial));
	return $rv[0];		
}

=head2 get_certificate_by_dn ( $dn, $exact, $valid )

Fetches the certificate using its DN, and returns it as PKCS#7 PEM encoded, along with its expiration date, and the number of certificates with the same DN. If several certificates match the request, only the first one will be returned. Therefore, it is preferable to use the list_certificates method, if unsure.

	$dn - Subject DN of the certificate to look for
	$exact - If set to non-zero, tells to match the DN exactly, and not %$dn%
	$valid - If set to non-zero, tells to match only valid certificates

=cut
sub get_certificate_by_dn {
	my $self = shift;
	my ($dn,$exact,$valid)=@_;
	$dn="%".$dn."%" unless ($exact);
	my $query="SELECT expiration,pem FROM certificate WHERE dn='$dn'";
	$query.=" and status='V'" if ($valid);
	my $dbh = $self->{dbh};
	my @rv=$dbh->selectrow_array($query);
	if (scalar(@rv)>0) {
		$query="SELECT count(serial) FROM certificate WHERE dn='$dn'";
		$query.=" and status='V'" if ($valid);
		my @cnt=$dbh->selectrow_array($query);
		push @rv,$cnt[0];
	} 
	return @rv;		
}

# Internal method to log any error in case of error...
sub _error {
	my $self = shift;
	my $dbh = $self->{dbh};
	$self->{log}->log_error("CADB_ERROR","CADB",$dbh->errstr);
	return undef;
}


=head2 Optimize

Optimizes the databases (e.g. calls "VACUUM"). Should be called once a day. This is a static method, that should be called as NikoSoft::PKI::CADB::Optimize()

=cut
sub Optimize {
	if ($TYPE eq 'enterprise') {
		my $pgrun = "$INSTALLDIR/var/dbrun";
		system("/usr/bin/psql -h '$pgrun' -U pki -c \"VACUUM ANALYZE certificate\" pkica >/dev/null 2>&1");
		return;
	}
	system("/usr/bin/sqlite3 $CAPATH/$DBFILE \"VACUUM\" >/dev/null 2>&1");	 
}


1;
