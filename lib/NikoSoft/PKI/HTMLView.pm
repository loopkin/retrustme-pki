##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: HTMLView.pm 421 2017-12-31 15:41:25Z loopkin $
##

=head1 NAME

NikoSoft::PKI::HTMLView

=cut

package NikoSoft::PKI::HTMLView;

BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
}

use strict;

use NikoSoft::PKI::CA;
use NikoSoft::PKI::Utils qw(asn1_time utf8_print print_show_err dumpfile);
use NikoSoft::PKI::CryptoTools qw(exec_openssl);
use NikoSoft::PKI::Config;
use NikoSoft::PKI::JSONView;
use NikoSoft::PKI::AccessControl;
use NSConfig;

=head1 METHODS

=head2 new ( $cgi, $config )

Constructor. $cgi is an instance of CGI.pm and $config an instance of NikoSoft::PKI::Config

=cut
sub new {
	my $class = shift;
	my ($cgi,$config) = @_;
	my $self  = bless { cgi => $cgi, config => $config, accesscontrol => NikoSoft::PKI::AccessControl->new() }, $class;
	return $self;
}

=head2 end_html

HTML Out: code to end HTML page

=cut
sub end_html {
	my $self = shift;
	my $cgi = $self->{cgi};
	my $out=$cgi->br();
	$out.=$cgi->div({-class=>"footerbar"},"&nbsp; $PRODUCT v$VERSION");
	$out.=$cgi->end_html();
	return $out;
}


=head2 print_kc_form

HTML out: prints HTML form for Key Ceremony

=cut
sub print_kc_form {
	my $self = shift;
	my $cgi = $self->{cgi};
	print '<div style="clear:both; height:0px"></div>';
	print '<section class="ui-widget">'.$cgi->h2("Key Ceremony").'<div class="ui-widget-content" style="padding:6px;">';
	print $cgi->start_form(-method=>"post");
	print $cgi->start_table({-border=>'0', -cellspacing=>0, cellpadding=>'2px', -class=>'formtable' });
	print '<tr>'.$cgi->td(["Common Name:",$cgi->textfield(-name=>'cacn',-default=>"Snake Oil CA",-maxlength=>64)])."</tr>\n";
	print '<tr>'.$cgi->td(["Organization:",$cgi->textfield(-name=>'cao',-default=>"Snake Oil",-maxlength=>64)])."</tr>\n";
	print '<tr>'.$cgi->td(["Country:",$cgi->textfield(-name=>'cac',-default=>"EU",-maxlength=>64)])."</tr>\n";
	print '<tr>'.$cgi->td(["Administrator Email:",$cgi->textfield(-name=>'caemail',-default=>"",-maxlength=>64)])."</tr>\n";
	print '<tr><td colspan="2">'.$cgi->submit(-class=>'action',-value=>'  Submit  ').'</td></tr>';
	print $cgi->end_table();
	print $cgi->end_form();
	print '</div></section>';
}

=head2 print_cert_form

HTML out: prints HTML form for Certificate Request

=cut
sub print_cert_form {
	my $self = shift;
	my $ca = shift;
	my $cgi = $self->{cgi};
	my %labels=$ca->cert_profiles_list();
	my @labelslist=sort(keys(%labels));
	$self->print_smartcard_javascript("");
	my $tooltip='true';
	$tooltip='false' if ($main::LANG eq 'en');
	print <<__EOS__;
<script type="text/javascript">//<![CDATA[
\$(function() {
    \$( document ).tooltip({
    	disabled: $tooltip,
 	content: function (callback) {
		callback(\$(this).prop('title'));
	},
	open: function (event, ui) {
            setTimeout(function () {
                \$(ui.tooltip).hide('blind');
             }, 5000);
         }
    });
  });
function matchpwd() {
	if (document.getElementById('pwd').value != document.getElementById('cpwd').value) { alert('Password do not match!'); return false;}
	\$("#certformsubmit").attr("disabled", true);
	\$("#certform").submit();
	return true;
}
function changeReqType() {
	var rt=\$("input[name='RequestType']:checked").val();
	if (rt =='PKCS12') {
		\$("#formtype").val('enroll');
		\$("#pkcs12pwd").show('fade');
		\$("#pkcs12pwc").show('fade');
		\$("#pkcs10r").hide('fade');
	}
	if (rt =='PKCS10') {
		\$("#formtype").val('enroll_pkcs10');
		\$("#pkcs12pwd").hide('fade');
		\$("#pkcs12pwc").hide('fade');
		\$("#pkcs10r").show('fade');
	}
	if (rt=='CSP') {
		\$("#formtype").val('enroll_pkcs10');
		\$("#pkcs12pwd").hide('fade');
		\$("#pkcs12pwc").hide('fade');
		\$("#pkcs10r").hide('fade');
		if (!confirm('Do you want to create a certificate request using the CSP?')) { return; }
		CreateRequest();
	}
}
//]]></script>
__EOS__
	print '<div style="clear:both; height:0px"></div>';
	print '<section class="ui-widget">'.$cgi->h2(_T('REQUEST_CERTIFICATE')).'<div class="ui-widget-content" style="padding:6px;">';
	print $cgi->start_multipart_form(-method=>"post",-id=>"certform");
	$self->print_tip(_T('TIP_CERT_REQ'));
	my @rts=('PKCS12','PKCS10');
	push @rts, "CSP" if ($ENV{'HTTP_USER_AGENT'}=~m/Trident/);
	print $cgi->start_table({-border=>'0', -cellspacing=>0, cellpadding=>'2px', -class=>'formtable' });
	print  '<tr><td colspan="2">'.$cgi->radio_group(-name=>'RequestType',-values=>\@rts,-default=>'PKCS12',-onchange=>'changeReqType()',
		-title=>'<ul><li>PKCS12: PKCS#12 format - certificate and private key.</li><li>PKCS10: provide a Certificate Signing Request in PKCS#10 format</li><li>CSP: IE only. Generate private key and CSR using MSCAPI CSP</li></ul>')."</td></tr>\n";
	print '<tr>'.$cgi->td([_T('CN').': ',$cgi->textfield(-name=>'cn',-value=>'',-size=>'30',-maxlength=>'64',
		-title=>'<ul><li>For an user: First Name and Last Name.</li><li>For a server: DNS FQDN.</li></ul>')])."</tr>\n";
	print '<tr>'.$cgi->td([_T('EMAIL').": ",$cgi->textfield(-name=>'email',-value=>'',-size=>'30',-maxlength=>'64',
		-title=>'<ul><li>For an user: user\'s email.</li><li>For a server: administrator\'s email.</li><li>For SmartCardLogon profile: user\'s MSUPN.</li></ul>')])."</tr>\n";
	print '<tr>'.$cgi->td([_T('CERTIFICATE_TYPE').": ",$cgi->popup_menu(-name=>'crt_type',-values=>\@labelslist,-labels=>\%labels)])."</tr>\n";
	print '<tr id="pkcs12pwd">'.$cgi->td([_T('PKCS12_PASSWORD').": ",$cgi->password_field(-name=>'pwd',-size=>'30',-id=>'pwd',
		-title=>'Password must be between 6 and 16 characters, and contain at least one letter and one number')])."</tr>\n";
	print '<tr id="pkcs12pwc">'.$cgi->td([_T('CONFIRM_PASSWORD').": ",$cgi->password_field(-name=>'cpwd',-size=>'30',-id=>'cpwd',
		-title=>'Password must be between 6 and 16 characters, and contain at least one letter and one number')])."</tr>\n";
	print '<tr id="pkcs10r">'.$cgi->td([_T('PKCS10').": ",$cgi->filefield(-name=>'pkcs10',-size=>'30',-id=>'pkcs10',
		-title=>'Certificate Signing Request, in PKCS#10 format, Base64/PEM encoded')])."</tr>\n";
	print '<tr><td colspan="2">'.$cgi->submit(-class=>'action',-value=>'  '._T('SUBMIT').'  ',onClick=>'return matchpwd();',-id=>'certformsubmit').'</td></tr>';
	print $cgi->end_table();
	print $cgi->input({-type=>'hidden',-name=>'action',-value=>'enroll',-id=>'formtype'});
	print $cgi->end_form()."\n";
	print '</div>';
	print '</section>';
	print '<script type="text/javascript">$("#pkcs10r").hide();</script>';
}

=head2 print_op_cert_form

HTML out: prints HTML form for Operators' Certificate Request

=cut
sub print_op_cert_form {
	my $self = shift;
	my $ca = shift;
	my $cgi = $self->{cgi};
	my %labels=$ca->cert_profiles_list();
	my @labelslist=sort(keys(%labels));
	$self->print_smartcard_javascript("");
	my $tooltip='true';
	$tooltip='false' if ($main::LANG eq 'en');
	print <<__EOPS__;
<script type="text/javascript">//<![CDATA[
\$(function() {
    \$( document ).tooltip({
    	disabled: $tooltip,
	content: function (callback) {
		callback(\$(this).prop('title'));
	}
    });
  });
function matchpwd() {
	if (document.getElementById('pwd').value != document.getElementById('cpwd').value) { alert('Password do not match!'); return false;}
	return true;
}
//]]></script>
__EOPS__
	print '<div style="clear:both; height:0px"></div>';
	print '<section class="ui-widget">'.$cgi->h2(_T('REQUEST_CERTIFICATE')).'<div class="ui-widget-content" style="padding:6px;">';
	print $cgi->start_multipart_form(-method=>"post");
	print $cgi->start_table({-border=>'0', -cellspacing=>0, cellpadding=>'2px', -class=>'formtable' });
	print '<tr>'.$cgi->td([_T('CN').': ',$cgi->textfield(-name=>'cn',-value=>'',-size=>'30',-maxlength=>'64',
		-title=>'<ul><li>For an user: First Name and Last Name.</li><li>For a server: DNS FQDN.</li></ul>')])."</tr>\n";
	print '<tr>'.$cgi->td(["E-Mail: ",$cgi->textfield(-name=>'email',-value=>'',-size=>'30',-maxlength=>'64',
		-title=>'<ul><li>For an user: user\'s email.</li><li>For a server: administrator\'s email.</li></ul>')])."</tr>\n";
	print '<tr>'.$cgi->td([_T('UID').': ',$cgi->textfield(-name=>'uid',-value=>'',-size=>'30',-maxlength=>'64',
		-title=>'<ul><li>For an administrator: "admin".</li><li>For a lifecycle operator: anything else.</li></ul>')])."</tr>\n";
	print '<tr id="pkcs12pwd">'.$cgi->td([_T('PKCS12_PASSWORD').": ",$cgi->password_field(-name=>'pwd',-size=>'30',-id=>'pwd',
		-title=>'Password must be between 6 and 16 characters, and contain at least one letter and one number')])."</tr>\n";
	print '<tr id="pkcs12pwc">'.$cgi->td([_T('CONFIRM_PASSWORD').": ",$cgi->password_field(-name=>'cpwd',-size=>'30',-id=>'cpwd',
		-title=>'Password must be between 6 and 16 characters, and contain at least one letter and one number')])."</tr>\n";
	print '<tr><td colspan="2">'.$cgi->submit(-class=>'action',-value=>'  '._T('SUBMIT').'  ',onClick=>'return matchpwd();').'</td></tr>';
	print $cgi->end_table();
	print $cgi->input({-type=>'hidden',-name=>'action',-value=>'enroll',-id=>'formtype'});
	print $cgi->input({-type=>'hidden',-name=>'crt_type',-value=>'op_cert'});
	print $cgi->end_form()."\n";
	print '</div></section><br/>';
}


=head2 print_cert_search_form

HTML Out: Certificate search form.

=cut
sub print_cert_search_form {
	my $self = shift;
	my $ca = shift;
	my $cgi = $self->{cgi};
	print '<div style="clear:both; height:0px"></div>';
	print '<section class="ui-widget">'.$cgi->h2(_T('SEARCH_CERT')).'<div class="ui-widget-content" style="padding:6px;">';
	my %labels=$ca->cert_profiles_list();
	$labels{''}='...';
	my @labelslist=sort(keys(%labels));
	$self->print_tip(_T('TIP_CERT_SEARCH'));
	print $cgi->start_form(-method=>"post");
	print $cgi->start_table({-border=>'0', -cellspacing=>0, cellpadding=>'2px', -class=>'formtable' });
	print '<tr>'.$cgi->td([_T('DN').': ',$cgi->textfield('search_dn','','30')])."</tr>\n";
	print '<tr>'.$cgi->td([_T('CERTIFICATE_TYPE').": ",$cgi->popup_menu(-name=>'search_profile',-values=>\@labelslist,-labels=>\%labels)])."</tr>\n";
	print '<tr>'.$cgi->td([_T('SHOW_REVOKED').': ',$cgi->checkbox('search_revoked',0,'1','')])."</tr>\n";
	print '<tr><td colspan="2">'.$cgi->submit(-class=>'action',-value=>'  '._T('SUBMIT').'  ').'</td></tr>';
	print $cgi->end_table();
	print $cgi->input({-type=>'hidden',-name=>'action',-value=>'search'});
	print $cgi->end_form()."\n";
	print '</div></section><br/>';
}

=head2 print_cert_list ($search_revoked,$search_dn, $ca)

HTML out / JSON out: prints emitted certificate list, accordingly to the search. Max 20 certs. This is the only method in this class that outputs JSON optionally (accordingly to $output).

=cut
sub print_cert_list {
	my $self=shift;
	my ($output, $jsonview, $search_revoked, $search_dn, $ca,$profile)=@_;
	my $cgi = $self->{cgi};
	my $config = $self->{config};
	unless ($output==1) {
		print '<div style="clear:both; height:0px"></div>';
		print '<section class="ui-widget">'.$cgi->h2(_T('LIST_CERTIFICATES')).'<div class="ui-widget-content" style="padding:6px;">';
		$self->print_tip(_T('TIP_CERT_SEARCH_RESULT'));
		print $cgi->p(_T('SEARCH_STRING').": $search_dn");
		print $cgi->start_form(-method=>"post");
		print $cgi->start_table({-border=>'1px', -cellspacing=>0, -cellpadding=>'3px',-class=>"result",-id=>"results_table"});
		print '<tr>'.$cgi->th(['',_T('STATUS'),_T('SERIAL'),_T('EXPIRATION_DATE'),_T('REVOCATION_DATE'),_T('SUBJECT_DN')])."</tr>\n" unless ($output==1);
	}
	$jsonview->json_start('OK',1) if ($output==1);
	my $serials=$ca->list_certificates($search_revoked,$search_dn,$profile);
	my $count=0;
	foreach my $serial (sort(keys(%{$serials}))) {
		my $revdate=$serials->{$serial}->{revocation} || '';
		my $expdate=$serials->{$serial}->{expiration};
		my $status=$serials->{$serial}->{status};
		my $show_rev=0;
		$show_rev=1 if ($status eq 'V');
		my $subj=$serials->{$serial}->{dn};
		$subj=utf8_print($subj);
		$subj=~s/emailAddress/email/;
		print "<tr>".$cgi->td([$cgi->checkbox('rev_serial',0,$serial,''),$status,$serial,asn1_time($expdate),asn1_time($revdate),"<a href=\"?action=viewcert&view_serial=$serial&show_rev=$show_rev\" class=\"subjecta\">$subj</a>"])."</tr>\n" unless ($output==1);
		$jsonview->json_name_value("res$count","$status,$serial,$expdate,$revdate,$subj",1) if ($output==1);
		last if ($count==20);
		$count++;
	}
	$jsonview->json_name_value("count",$count,0) if ($output==1);
	$jsonview->json_end() if ($output==1);
	unless ($output==1) {
		print $cgi->end_table();
		print $cgi->input({-type=>'hidden',-name=>'action',-value=>'revoke'});
		print $cgi->br();
		print $cgi->submit(-class=>'action',-value=>'  '._T('REVOKE_CERT').'  ',onClick=>'return confirm("'._T('REVOKE_CERT_CONFIRM').'");');
		print $cgi->end_form();
		print '</div></section><br/>';
		print '<script>$("#results_table tr:even").addClass("alt");</script>';
	}
}

=head2 print_op_cert_list ( $search_revoked )

HTML out: prints emitted certificate list, accordingly to the search. Max 20 certs.

=cut
sub print_op_cert_list {
	my $self=shift;
	my ($search_revoked,$ca)=@_;
	my $cgi = $self->{cgi};
	my $config = $self->{config};
	print '<div style="clear:both; height:0px"></div>';
	print '<section class="ui-widget">'.$cgi->h2(_T('LIST_CERTIFICATES')).'<div class="ui-widget-content" style="padding:6px;">';
	print $cgi->start_form(-method=>"post");
	print $cgi->start_table({-border=>'1px', -cellspacing=>0, -cellpadding=>'3px',-class=>"result",-id=>"results_table"});
	print '<tr>'.$cgi->th(['',_T('STATUS'),_T('SERIAL'),_T('EXPIRATION_DATE'),_T('REVOCATION_DATE'),_T('SUBJECT_DN')])."</tr>\n";
	my $serials=$ca->list_op_certificates($search_revoked);
	my $count=0;
	foreach my $serial (sort(keys(%{$serials}))) {
		my $revdate=$serials->{$serial}->{revocation} || '';
		my $expdate=$serials->{$serial}->{expiration};
		my $status=$serials->{$serial}->{status};
		my $show_rev=0;
		$show_rev=1 if ($status eq 'V');
		my $subj=$serials->{$serial}->{dn};
		$subj=utf8_print($subj);
		$subj=~s/emailAddress/email/;
		print "<tr>".$cgi->td([$cgi->checkbox('rev_serial',0,$serial,''),$status,$serial,asn1_time($expdate),asn1_time($revdate),"<a href=\"?action=viewcert&view_serial=$serial&show_rev=$show_rev\" class=\"subjecta\">$subj</a>"])."</tr>\n";
		last if ($count==20);
		$count++;
	}
	print $cgi->end_table();
	print $cgi->input({-type=>'hidden',-name=>'action',-value=>'revoke'});
	print $cgi->br();
	print $cgi->submit(-class=>'action',-value=>'  '._T('REVOKE_CERT').'  ',onClick=>'return confirm("'._T('REVOKE_CERT_CONFIRM').'");');
	print $cgi->end_form();
	print '</div></section><br/>';
	print '<script>$("#results_table tr:even").addClass("alt");</script>';
}

=head2 print_cert ($serial,$ca,$show_rev)

HTML Out: certificate

=cut
sub print_cert {
	my $self = shift;
	my ($serial,$crt,$show_rev)=@_;
	my $cgi = $self->{cgi};
	print '<div style="clear:both; height:0px"></div>';
	print '<section class="ui-widget">'.$cgi->h2(_T('VIEW_CERT').$serial).'<div class="ui-widget-content" style="padding:6px;">';
	$crt=~ s/\</\&lt\;/g;
	$self->print_tip(_T('TIP_CERT_VIEW')) if ($show_rev);
	print "<pre>";
	print utf8_print($crt);
	print "</pre>\n";
	print "<a href=\"?action=revoke&rev_serial=$serial\" class=\"button\" onClick=\"return confirm('Are you sure you want to revoke this certificate?');\">&nbsp; "._T('REVOKE_CERT')." &nbsp;</a>".$cgi->br() if ($show_rev);
	print '</div></section>';
}

=head2 print_settings_form

HTML out: print settings formtable

=cut
sub print_settings_form {
	my $self = shift;
	my $ca = shift;
	my $cgi = $self->{cgi};
	print <<__END_SJS__;
<script type="text/javascript">//<![CDATA[
\$(function() {
    \$("#idTab").tabs();
    \$("#idTab").show();
    \$( document ).tooltip();
  });

function submitSettings() {
	\$.ajax({
		type: 'POST',
		data: \$("#settingsForm").serialize(),
		dataType: 'json',
		success: function (data) {
			if (data.status==='OK') {
				\$("#resultSettings").empty();
				\$("#resultSettings").append(' <span class="success">Settings Saved.</span>');
				\$("#resultSettings").show();
				\$("#resultSettings").hide('fade',5000);
			} else {
				\$("#resultSettings").empty();
				\$("#resultSettings").append(' <span class="error">'+data.msg+'</span>');
				\$("#resultSettings").show();
				\$("#resultSettings").hide('fade',5000);
			}
		}
	});
}
//]]></script>
__END_SJS__
	print $cgi->start_form(-method=>"post",-name=>"settings_form",-id=>'settingsForm');
	print '<div style="clear:both; height:0px"></div><div id="idTab" style="display: none">';
	print '<ul>';
	print '<li><a href="#idTab-1">'.$cgi->h4('General')."</a></li>\n";
	print '<li><a href="#idTab-2">'.$cgi->h4('User Keys Management')."</a></li>\n";
	print '<li><a href="#idTab-3">'.$cgi->h4('Certificate Templates')."</a></li>\n";
	print '</ul>';
	print '<div id="idTab-1">';
	print $cgi->start_table({-border=>'0', -cellspacing=>0, cellpadding=>'2px', -class=>'formtable' });
	print '<tr>'.$cgi->td(['Administrator\'s email: ',$cgi->textfield( -name=>'adminmail', -default=>$self->{config}->data()->{adminmail},-maxlength=>64,-size=>35)])."</tr>\n";
	print '<tr class="alt">'.$cgi->td(['Send renewal emails to holder: ',$cgi->checkbox('opt_renewholder',$self->{config}->data('renewholder'),'1','')])."</tr>\n";
	print '<tr>'.$cgi->td(['Send renewal emails to admin: ',$cgi->checkbox('opt_renewadmin',$self->{config}->data('renewadmin'),'1','')])."</tr>\n";
	print '<tr class="alt">'.$cgi->td(['Renewal email\'s subject: ',$cgi->textfield( -name=>'renewmailsubject', -default=>$self->{config}->data('renewmailsubject'),-maxlength=>64,-size=>40)])."</tr>\n";
	my $renewmailtext=$self->{config}->data('renewmailtext');
	print '<tr>'.$cgi->td(['Renewal email\'s text: ',$cgi->textarea( -name=>'renewmailtext', -default=>$renewmailtext,-rows=>8,-columns=>40)])."</tr>\n";
	print '<tr class="alt">'.$cgi->td(['Purge expired certificates: ',$cgi->checkbox('opt_autopurge',$self->{config}->data()->{autopurge},'1','').' (check with caution)'])."</tr>\n";
	print '<tr>'.$cgi->td(['Activate daily tasks: ',$cgi->checkbox('opt_dailycron',$self->{config}->data('dailycron'),'1','').' (uncheck with caution)'])."</tr>\n";
	print '<tr class="alt">'.$cgi->td(['New CRL on every revocation: ',$cgi->checkbox('opt_crlonrevoke',$self->{config}->data('crlonrevoke'),'1','').' (uncheck with caution; if unchecked CRL is issued every hour.)'])."</tr>\n";
	print $cgi->end_table();
	print '</div>';
	print '<div id="idTab-2">';
	print $cgi->start_table({-border=>'0', -cellspacing=>0, cellpadding=>'2px', -class=>'formtable' });
	print '<tr>'.$cgi->td(['Send PKCS#12 to user by e-mail: ',$cgi->checkbox('opt_p12mail',$self->{config}->data()->{p12mail} ,'1','')])."</tr>\n";
	print '<tr class="alt">'.$cgi->td(['Show PKCS#12 download link: ',$cgi->checkbox('opt_p12link',$self->{config}->data()->{p12link} ,'1','')])."</tr>\n";
	print '<tr>'.$cgi->td(['PKCS#12 email\'s subject: ',$cgi->textfield( -name=>'p12mailsubject', -default=>$self->{config}->data()->{p12mailsubject},-maxlength=>64,-size=>40)])."</tr>\n";
	my $p12mailtext=$self->{config}->data('p12mailtext');
	print '<tr class="alt">'.$cgi->td(['PKCS#12 email\'s text: ',$cgi->textarea( -name=>'p12mailtext', -default=>$p12mailtext,-rows=>8,-columns=>40)])."</tr>\n";
	print '<tr>'.$cgi->td(['CSP for smart card key generation: ',$cgi->textfield( -name=>'mscapicsp',
			-default=>$self->{config}->data('mscapicsp'),-maxlength=>64,-size=>40,
			-title=>'Name of the MSCAPI CSP, as seen under HKLM\SOFTWARE\Microsoft\ Cryptography\Defaults\Provider').'(IE only)'])."</tr>\n";
	my %profiles=$ca->cert_profiles_list(1);
	$profiles{none}='None';
	my @profileslist=sort(keys(%profiles));
	print '<tr>'.$cgi->td(["ACME Certificate Profile: ",$cgi->popup_menu(-name=>'opt_acme_crt_type',-values=>\@profileslist,-labels=>\%profiles,-default=>$self->{config}->data('acme_crt_type'))])."</tr>\n";
	if ($TYPE eq 'enterprise') {
		print '<tr class="alt">'.$cgi->td(['Use static SCEP challenge: ',$cgi->checkbox('opt_static_scep',$self->{config}->data('static_scep') ,'1','').' (Use with caution: if checked, SCEP challenge is not renewed every day)'])."</tr>\n";
		print '<tr>'.$cgi->td(["SCEP Certificate Profile: ",$cgi->popup_menu(-name=>'opt_scep_crt_type',-values=>\@profileslist,-labels=>\%profiles,-default=>$self->{config}->data('scep_crt_type'))])."</tr>\n";
	}
	print $cgi->end_table();
	print '</div>';
	print '<div id="idTab-3">';
	print $cgi->start_table({-border=>'0', -cellspacing=>0, cellpadding=>'2px', -class=>'formtable' });
	print '<tr>'.$cgi->td([' Profile Name ',' Enabled ',' Max Crt/Usr ',' Lifetime ',' OU in DN '])."</tr>\n";
	my $i=0;
	my $class='';
	foreach my $profid (@profileslist) {
		if ($i % 2) { $class = ''; } else { $class =' class="alt"'; }
		print "<tr$class>".$cgi->td([$profiles{$profid}.':',
					$cgi->checkbox('opt_prof_'.$profid,$self->{config}->data()->{'prof_'.$profid} ,'1',''),
					$cgi->textfield( -name=>'opt_prof_'.$profid.'_maxnbcert', -default=>$self->{config}->data('prof_'.$profid.'_maxnbcert'),-maxlength=>4,-size=>8,-title=>'Maximum number of certificates per user (identified as DN)'),
					$cgi->textfield( -name=>'opt_prof_'.$profid.'_crtlifetime', -default=>$self->{config}->data('prof_'.$profid.'_crtlifetime'),-maxlength=>4,-size=>8,-title=>'Life time of the certificate, in days. 0 means 1 hour.'),
					$cgi->textfield( -name=>'opt_prof_'.$profid.'_ou', -default=>$self->{config}->data('prof_'.$profid.'_ou'),-maxlength=>20,-size=>15,-title=>'Content of the OU component of the DN. Optional.')
					])."</tr>\n";
		$i++;
	}
	print '<tr>'.$cgi->td(['Put e-mail in DN: ']).$cgi->td({-colspan=>'4'},
		[$cgi->checkbox(-name=>'opt_mail_in_dn',-checked=>$self->{config}->data('mail_in_dn'),-value=>'1',
		-label=>'',-title=>'E-mail will always be present in SAN')])."</tr>\n";
	print '<tr class="alt">'.$cgi->td(['Guess root DN from e-mail: ']).$cgi->td({-colspan=>'4'},
		[$cgi->checkbox(-name=>'opt_guess_dn_from_mail',-checked=>$self->{config}->data('guess_dn_from_mail'),
			-value=>'1',-title=>'If checked, John Doe <john.doe@gmail.com> will have at least the following DN: /DC=com/DC=gmail/CN=John Doe',-label=>'')])."</tr>\n";
	print '<tr>'.$cgi->td(['URL for CRL download (CRLDP): ']).$cgi->td({-colspan=>'4'},[$cgi->textfield( -name=>'crldp', -default=>$self->{config}->data()->{crldp},-maxlength=>64,-size=>30,-title=>'Best practice: put an URL that is reachable from the Internet')])."</tr>\n";
	print '<tr>'.$cgi->td(['URL for CA certificate download (AIA): ']).$cgi->td({-colspan=>'4'},[$cgi->textfield( -name=>'aiaca', -default=>$self->{config}->data()->{aiaca},-maxlength=>64,-size=>30,-title=>'Best practice: put an URL that is reachable from the Internet')])."</tr>\n";
	print '<tr class="alt">'.$cgi->td(['Certificate Policy OID: ']).$cgi->td({-colspan=>'4'},[$cgi->textfield( -name=>'cpoid', -default=>$self->{config}->data()->{cpoid},-maxlength=>64,-size=>30,-title=>'See RFC 5280 for more information')])."</tr>\n";
	print $cgi->end_table();
	print '</div>';
	print $cgi->input({-type=>'hidden',-name=>'action',-value=>'settings'});
	print $cgi->input({-type=>'hidden',-name=>'do',-value=>'settings'});
	print $cgi->input({-type=>'hidden',-name=>'direct',-value=>'1'});
	print $cgi->button(-class=>'action',-value=>'  Submit  ',-name=>'do',-id=>'doSettings',-onclick=>'submitSettings()');
	print '<span id="resultSettings"></span>';
	print '</div>';
	print $cgi->end_form()."\n";
}


=head2 print_about

HTML Out: prints the version number of NikoSoft PKI, as well as version information for OpenSSL

=cut
sub print_about {
	my $self = shift;
	my $cgi = $self->{cgi};
	print '<div style="clear:both; height:0px"></div>';
	print '<section class="ui-widget">'.$cgi->h2("$PRODUCT v.$VERSION").'<div class="ui-widget-content" style="padding:6px;">';
	print $cgi->p("&copy; 2010-2018 Alexandre Aufrere. Published under the terms of GPL version 3.");
	print $cgi->p("See http://www.gnu.org/licenses/gpl.html for information about the license terms.");
	my $osslversion=exec_openssl(2,'version 2>/dev/null');
	print $cgi->p("Using OpenSSL version: $osslversion");
	print $cgi->p("OpenSSL: Copyright (c) 1998-2017 The OpenSSL Project.  All rights reserved.");
	print $cgi->p("This version of OpenSSL is certified FIPS 140-2 Level 1 (NIST Certificate #1747)") if ($osslversion=~m/fips/);
	my $osslengines=exec_openssl(2,'engine 2>/dev/null');
	print $cgi->p("This version of OpenSSL has uncertified GOST support") if ($osslengines=~m/gost/i);
	print $cgi->p("This version of OpenSSL has certified GOST support") if ($osslengines=~m/cryptocom/i);
	print '</div></section><br/><section class="ui-widget"><h2>Product URLs</h2><div class="ui-widget-content" style="padding:6px;">';
	print $cgi->p("PKI operations and configuration: https://$CAHOSTNAME/pki");
	print $cgi->p("CRMP operations as per CRMP specifications: https://$CAHOSTNAME/crmp/");
	print $cgi->p("ACME operations as per ACMEv1 specifications: https://$CAHOSTNAME/acme/directory/");
	print $cgi->p("TSA timestamps as per RFC 3161: http://$CAHOSTNAME/tsa");
	print $cgi->p("SCEP connector for certificate issuance: http://$CAHOSTNAME/scep/ or http://$CAHOSTNAME/scep/pkiclient.exe") if ($TYPE eq 'enterprise');
	print '</div></section><br/>';
}

=head2 print_tip ( $tipstr )

HTML Out: Prints the tip $tipstr

=cut
sub print_tip {
	my $self = shift;
	my $tipstr = shift;
	my $cgi = $self->{cgi};
	print "<div class=\"tip\">";
	print $tipstr;
	print "</div>";
}

=head2 print_menu_bar ($relurl, $logout)

HTML Out: Prints the menu bar.

=cut
sub print_menu_bar {
	my $self = shift;
	my $action = shift;
	my $relurl = shift;
	my $logout = shift;
	my $cgi = $self->{cgi};
	my $org=$self->{config}->data()->{organization} || '';
	my $class='menubar';
	print  "<h1>$org PKI <span style=\"font-size: 11pt; float:right; padding-top:6px; padding-right:10px; \" title=\"".$self->{accesscontrol}->get_connected_user_fn()."\">&#9906; ".$self->{accesscontrol}->get_connected_user(). "</span></h1>";
	print "<div class=\"menubar\">";
	print "<a class=\"menubar\" href=\"$relurl\">&nbsp; "._T("HOME")." &nbsp;</a> ";
	if (-d "$CAPATH/private") {
		$class="menuselect" if ($action eq 'camgt');
		print "<a class=\"$class\" href=\"$relurl?action=camgt\">&nbsp; "._T('CA_MANAGEMENT')." &nbsp;</a> ";
		$class='menubar';
		$class="menuselect" if ($action eq 'scepinfo');
		print "<a class=\"$class\" href=\"$relurl?action=scepinfo\">&nbsp; SCEP &nbsp;</a> " if ($TYPE eq 'enterprise');
		$class='menubar';
		if (($AUTH_MODE eq 'CERT')&&($self->{accesscontrol}->is_connected_user_admin())) {
			$class="menuselect" if ($action eq 'opcert');
			print "<a class=\"$class\" href=\"$relurl?action=opcert\">&nbsp; Operators Certificates &nbsp;</a> ";
			$class='menubar';
		}
		$class="menuselect" if ($action eq 'settings');
		print "<a class=\"$class\" href=\"$relurl?action=settings\">&nbsp; "._T('SETTINGS')." &nbsp;</a>" if ($self->{accesscontrol}->is_connected_user_admin());
	}
	$class='menubar';
	$class="menuselect" if ($action eq 'about');
	print "<a class=\"$class\" href=\"$relurl?action=about\">&nbsp; "._T('ABOUT')." &nbsp;</a>";
	print "</div>\n";
}

=head2 print_sc_management

HTML OUT: prints SmartCard management page

=cut
sub print_sc_management {
	my $self = shift;
	my $cgi = $self->{cgi};
	my $crldp=$self->{config}->data()->{crldp} || '';
	print '<div style="clear:both; height:0px"></div>';
	#return unless ($self->{accesscontrol}->is_connected_user_admin());
	print <<__END_SCL__;
<script type="text/javascript">
\$(function() {
    \$( document ).tooltip({
 	content: function (callback) {
		callback(\$(this).prop('title'));
	},
	open: function (event, ui) {
            setTimeout(function () {
                \$(ui.tooltip).hide('blind');
             }, 5000);
         }
    });
  });
function toggleUnlockDiag() {
	\$('#challenge_response').show();
	\$('#challenge_response').dialog({
	minWidth: window.innerWidth-30,
	maxHeight: window.innerHeight-100,
	title: 'Smart Card Unblock',
	closeText: "close",
	position: { at: "left top+150" }
	});
}
function getResponse() {
	\$('#cr_results_table').empty();
	\$.ajax({
		type: 'POST',
		data: \$("#challenge_response_form").serialize(),
		dataType: 'json',
		success: function (data) {
			if (data.status==='OK') {
				var trHTML = '';
				trHTML+='<tr><td><span>Response: '+data.response+'</span></td></tr>';
				\$('#cr_results_table').append(trHTML);
			} else {
				\$('#results_table').append('<tr><td><span class="error">Error</span></td></tr>');
			}
		}
		});
	}
</script>
<div id="challenge_response" style="display: none">
<form method="POST" id="challenge_response_form" border="0" cellpadding="2px" cellspacing="0">
<table class="formtable" border="0" cellpadding="2px" cellspacing="0" style="font-size: 1.2em">
<tr><td>Challenge Unblock Code: </td><td><input type="text" name="challenge"/></td></tr>
</table>
<input type="hidden" name="action" value="challenge_response"/>
<input type="hidden" name="direct" value="1"/>
<input type="button" class="action" onClick="getResponse()" value=" Get Response Unblock Code "/>
<br/><br/>
</form>
<table id="cr_results_table" class="result">
</table>
</div>
__END_SCL__
	print '<section class="ui-widget"><h2>Smart Card</h2><div class="ui-widget-content" style="padding:6px;">';
	print "<a class=\"cabutton\" href=\"#\" onClick=\"toggleUnlockDiag()\">Unblock Smart Card</a><br/>";
	print '</div></section>';
}

=head2 print_ca_management

HTML OUT: prints CA management page

=cut
sub print_ca_management {
	my $self = shift;
	my $log = shift;
	my $cgi = $self->{cgi};
	my $crldp=$self->{config}->data()->{crldp} || '';
	print '<div style="clear:both; height:0px"></div>';
	print '<section class="ui-widget"><h2>Downloads</h2><div class="ui-widget-content" style="padding:6px;">';
	print "<a class=\"cabutton\" href=\"$CAPUBLICURI/ca.crt\">"._T('CA_CERTIFICATE')."</a><br/><br/>";
	print "<a class=\"cabutton\" href=\"$crldp\">"._T('CA_CRL')."</a><br/><br/>\n";
	print '<a class=\"cabutton\" href="'.$CAPUBLICURI.'/EnrollmentClientInstaller.exe">Enrollment Client Installer</a><br/>'."\n";
	print '</div></section><br/>';
	return unless ($self->{accesscontrol}->is_connected_user_admin());
	my @actions=$log->get_action_list();
	my $logs_action=$cgi->popup_menu(-name=>'action_log',-values=>\@actions);
	print <<__END_SL__;
<script type="text/javascript">
\$(function() {
    \$( document ).tooltip({
 	content: function (callback) {
		callback(\$(this).prop('title'));
	},
	open: function (event, ui) {
            setTimeout(function () {
                \$(ui.tooltip).hide('blind');
             }, 5000);
         }
    });
  });
function toggleSearchLogs() {
	\$('#search_logs').show();
	\$('#search_logs').dialog({
	minWidth: window.innerWidth-30,
	maxHeight: window.innerHeight-100,
	title: 'Search Logs',
	closeText: "close",
	position: { at: "left top+150" }
	});
}
function searchLogs() {
	\$('#results_table').empty();
	\$.ajax({
		type: 'POST',
		data: \$("#search_logs_form").serialize(),
		dataType: 'json',
		success: function (data) {
		if (data.status==='OK') {
			var response=data;
			var trHTML = '';
			if (response.count>0) {
				trHTML+='<tr><th>Log ID</th><th>Date and Time</th><th>Action</th>';
				trHTML+='<th>User</th><th>Target Entity</th><th>Outcome</th></tr>';
			}
			for (i=0; i<response.count; i++) {
				var line=response[ 'res'+i ];
				trHTML += '<tr>';
				var items=line.split("|");
				for (j=0; j<5; j++) {
					trHTML+= '<td>' + items[j] + '</td>';
				}
				trHTML+='<td title=\\''+ items[6]+'\\'>'+ items[5] + '</td>';
				trHTML+='</tr>';
			}
			\$('#results_table').append(trHTML);
			\$("#results_table tr:even").addClass('alt');
			} else {
				\$('#results_table').append('<tr><td><span class="error">Error</span></td></tr>');
			}
		}
		});
	}
</script>
<div id="search_logs" style="display: none">
<form method="POST" id="search_logs_form" border="0" cellpadding="2px" cellspacing="0">
<table class="formtable" border="0" cellpadding="2px" cellspacing="0" style="font-size: 1.2em">
<tr><td>Action Type: </td><td>$logs_action</td></tr>
<tr><td>Log Outcome: </td><td><select name="outcome"><option value="">...</option><option value="E">Error</option><option value="S">Success</option></select></td></tr>
</table>
<input type="hidden" name="action" value="search_logs"/>
<input type="hidden" name="direct" value="1"/>
<input type="button" class="action" onClick="searchLogs()" value=" Search Logs "/>
<br/><br/>
</form>
<table id="results_table" class="result">
</table>
</div>
__END_SL__
	print '<section class="ui-widget"><h2>CA Lifecycle</h2><div class="ui-widget-content" style="padding:6px;">';
	$self->print_tip("Caution: the CA CSR is needed only for signing by another CA");
	print "<a class=\"cabutton\" href=\"?action=crlgen\">"._T('CRL_GENERATE')."</a><br/><br/>";
	print "<a class=\"cabutton\" href=\"$CAPUBLICURI/ca.csr\">"._T('CA_CSR')."</a><br/>\n";
	print '</div></section><br/>';
	print '<section class="ui-widget"><h2>Logs</h2><div class="ui-widget-content" style="padding:6px;">';
	print "<a class=\"cabutton\" href=\"?action=download_logs\">Download Logs</a><br/><br/>";
	print "<a class=\"cabutton\" href=\"#\" onClick=\"toggleSearchLogs()\">Search Logs</a><br/>";
	print '</div></section>';
}


=head2 print_scep_info

HTML OUT: prints SCEP connector information page

=cut
sub print_scep_info {
	my $self = shift;
	my $cgi = $self->{cgi};
	print '<div style="clear:both; height:0px"></div>';
	return unless ($self->{accesscontrol}->is_connected_user_admin());
	print '<section class="ui-widget"><h2>SCEP Challenge</h2><div class="ui-widget-content" style="padding:6px;">';
	print $self->{config}->data('scep_challenge');
	print '</div></section><br/>';
	print '<section class="ui-widget"><h2>SCEP CA</h2><div class="ui-widget-content" style="padding:6px;">';
	print "<pre>\n";
	print exec_openssl(2,"x509 -in $CAPATH/scep.crt -fingerprint -sha1");
	print "</pre>\n";
	print '</div></section><br/>';
}

=head2 print_mscep_page

HTML OUT: prints MSCEP emulator information page

=cut
sub print_mscep_page {
	my $self = shift;
	my $cgi = $self->{cgi};
	print '<HTML><Head><Meta HTTP-Equiv="Content-Type" Content="text/html; charset=UTF-8"><Title>Network Device Enrollment';
	return unless ($self->{accesscontrol}->is_connected_user_admin());
	my $challenge= $self->{config}->data('scep_challenge');
	my $cafinger= exec_openssl(2,"x509 -in $CAPATH/scep.crt -fingerprint -md5 -noout 2>/dev/null");
	$cafinger=~s/\://g;
	$cafinger=~s/\n//g;
	$cafinger=~s/MD5 Fingerprint=//g;
	$cafinger=~s/([0-9A-F]{8})([0-9A-F]{8})([0-9A-F]{8})([0-9A-F]{8})/$1 $2 $3 $4/;
	print ' Service</Title></Head><Body BgColor=#FFFFFF><Font ID=locPageFont Face="Arial"><Table Border=0 CellSpacing=0 CellPadding=4';
	print ' Width=100% BgColor=#008080><TR><TD><Font ID=locPageTitleFont Face="Arial" Size=-1 Color=#FFFFFF><LocID';
	print ' ID=locMSCertSrv>Network Device Enrollment Service</LocID></Font></TD></TR></Table><P ID=locPageTitle> Network Device Enrollment';
	print ' Service allows you to obtain certificates for routers or other network devices using the Simple Certificate Enrollment Protocol (SCEP). </P><P>';
	print ' To complete certificate enrollment for your network device you will need the following information: <P> The thumbprint (hash value) for the CA';
	print " certificate is: <B> $cafinger </B> <P> The enrollment challenge password is: <B> $challenge";
	print ' </B> <P> This password can be used only once and will expire within 60 minutes. <P> Each enrollment requires a new challenge password.';
	print ' You can refresh this web page to obtain a new challenge password. </P> <P ID=locPageDesc> For more information see  <A';
	print ' HREF=http://go.microsoft.com/fwlink/?LinkId=67852>Using Network Device Enrollment Service </A>. </P> <P></Font></Body></HTML>';
}


=head2 print_start_spinner

HTML out: print the spinner and starts spinning

=cut
sub print_start_spinner {
	my $self = shift;
	print '<div id="progressbar"></div><script> $("#progressbar").progressbar({ value: false }); </script>';
}

=head2 print_end_spinner

HTML out: ends spinning the spinner

=cut
sub print_end_spinner {
	my $self = shift;
	my $status = shift;
	$status='' unless ($status);
	my $class = 'success';
	$class = 'error' if ($class=~m/error/i);
	print "<script type=\"text/javascript\">\$(\"#progressbar\").progressbar( \"disable\" ); \$(\"#progressbar\").empty(); \$(\"#progressbar\").removeClass(); \$(\"#progressbar\").append('$status'); \$(\"#progressbar\").addClass('success');</script>";
}

=head2 print_smartcard_javascript

Prints javascript for smartcard management

=cut
sub print_smartcard_javascript {
	my $self = shift;
	my $cert = shift;
	$cert=~ s/\n//g;
	$cert=~ s/\r//g;
	my $mscapicsp=$self->{config}->data('mscapicsp');
	my $mscapicsptype=$cryptosystems{$CRYPTO}{mscapi_csptype} || '1';
	return unless ($ENV{'HTTP_USER_AGENT'}=~m/Trident/);
	print <<__END_JS__;
<object id="objCertEnrollClassFactory" classid="clsid:884e2049-217d-11da-b2a4-000e7bbb2b09"></object>
<script type="text/javascript">//<![CDATA[

    function CreateRequest()
    {
      try {
      	document.getElementById('pkcs10r').className='hiddenTr';
      	document.getElementById('pkcs10').type="hidden";

        // Variables
        var classFactory = document.getElementById("objCertEnrollClassFactory");
        var objCSP = classFactory.CreateObject("X509Enrollment.CCspInformation");
        var objCSPs = classFactory.CreateObject("X509Enrollment.CCspInformations");
        var objPrivateKey = classFactory.CreateObject("X509Enrollment.CX509PrivateKey");
        var objRequest = classFactory.CreateObject("X509Enrollment.CX509CertificateRequestPkcs10");
        var objDn = classFactory.CreateObject("X509Enrollment.CX500DistinguishedName");
        var objEnroll = classFactory.CreateObject("X509Enrollment.CX509Enrollment");

        //  Initialize the csp object using the desired Cryptograhic Service Provider (CSP)
        objCSP.InitializeFromName("$mscapicsp");

        //  Add this CSP object to the CSP collection object
        objCSPs.Add(objCSP);

        //  Provide key container name, key length and key spec to the private key object
	objPrivateKey.ProviderType = '$mscapicsptype'; // RSA Full (or other cepending on cryptosystem)
        objPrivateKey.Length = 2048;
        objPrivateKey.KeySpec = 1; // AT_KEYEXCHANGE = 1
        objPrivateKey.MachineContext = false;
        objPrivateKey.KeyProtection = 1; // PIN/PWD requested
        objPrivateKey.ProviderName="$mscapicsp";

        //  Provide the CSP collection object (in this case containing only 1 CSP object)
        //  to the private key object
        objPrivateKey.CspInformations = objCSPs;

        // Initialize P10 based on private key
        objRequest.InitializeFromPrivateKey(1, objPrivateKey, ""); // context user = 1

        // DN related stuff
        objDn.Encode("CN=dummy", 0); // XCN_CERT_NAME_STR_NONE = 0
        objRequest.Subject = objDn;

        // Enroll
        objEnroll.InitializeFromRequest(objRequest);
        var pkcs10 = objEnroll.CreateRequest(3); // XCN_CRYPT_STRING_BASE64REQUESTHEADER = 3

        document.getElementById('pkcs10').value=pkcs10;
      }
      catch (ex) {
        document.write("<br>ERROR: " + ex.number + " " + ex.description);
        return false;
      }
      return true;
    }

   function InstallCert() {
    document.write("<br/>Installing certificate...<br/>");

    try {
    // Variables
    var classFactory = document.getElementById("objCertEnrollClassFactory");
    var objEnroll = classFactory.CreateObject("X509Enrollment.CX509Enrollment")
    var sPKCS7 = "$cert";

    objEnroll.Initialize(1); // ContextUser
    objEnroll.InstallResponse(4, sPKCS7, 6, ""); // AllowUntrustedRoot = 4, XCN_CRYPT_STRING_BASE64_ANY = 6
    }
    catch (ex) {
      document.write("<br>ERROR: " + ex.description);
      return false;
    }

    return true;
  }
//]]></script>
__END_JS__
}


=head2 start_html

HTML Out: code to start HTML page, including css.

=cut
sub start_html {
	my $self = shift;
	my $cgi = $self->{cgi};
	my $org=$self->{config}->data()->{organization} || '';
	return $cgi->start_html(-title=>"$org PKI",
		-encoding => "utf-8",
		-script=> [ { -type=>'text/javascript', -src=>'/pkinc/jquery.min.js'},
		 {-type=>'text/javascript', -src=>'/pkinc/jquery-ui.min.js' } ],
		-style=> [ {-type=>'text/css', -src=>'/pkinc/jquery-ui.css'}, {-type=>'text/css',
			-code=>'
body { margin: 0; padding: 0 }
select { border: 1px #ccc solid; background: #f2f2f2; font-family: Arial, Sans; font-size: 11pt; margin: 1px; }
input, textarea { border: 1px #ccc solid; background: #f2f2f2; font-family: Arial, Sans; font-size: 11pt; margin: 1px; }
input.action, a.button, a.button:hover { font-weight: normal; text-decoration: none; color: #000; border: 1px #ccc solid; background: #f2f2f2; font-family: Arial, Sans; font-size: 11pt; border-radius: 5px; -moz-border-radius: 5px; padding: 2px; margin-top: 2px; }
input.action:hover, a.button:hover { color: #c00; background-color: #fff; }
body, div, h1, h2, h3, h4 { font-family: Ubuntu, \'Segoe UI\', Verdana, Helvetica, sans-serif; font-size: 11pt; }
pre { font-size: 10pt; word-wrap: break-word; }
a.subjecta { text-decoration: none; color: #000000; border: 0px #000000 solid; width: 100%; display:block; }
a.subjecta:hover { text-decoration: none; color: #c00; border: 0px #000000 solid; background: #f5f5f5; width: 100%; }
table { width: 100%; }
table.formtable { width: auto; }
table.result { border: 1px solid #ccc; border-collapse: collapse; font-size: 9pt;}
table.result th, table.result td { border: 1px solid #ccc; }
table.result th { background-color: #f2f2f2; color: #000; }
table.result td { padding: 5px; }
th { background: #f2f2f2; }
h1 { font-size: 1.5em; padding: 0.6em 8px; color: #fff; background-color: #000; margin: 0; }
h2 { font-size: 1.05em; color:#000; background-color: #f2f2f2; padding: 6px; margin:0; border: 1px solid #ccc; border-top-left-radius: 4px; border-top-right-radius: 4px; }
h4 { font-size: 1.05em; color:#000; margin-top: 2px; margin-bottom: 2px; }
hr { width: 100%; height: 0px; margin-top: 15px; margin-bottom: 15px; border-top: 1px solid #ccc; border-bottom: none; }
.error { color: #f00 }
.success { color: #007f00 }
div.menubar { width: 100%; float: left; margin: 0 0 2em 0; padding: 0; list-style: none; background-color: #f2f2f2; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc; color: #000; }
div.menubar a.menubar, div.menubar a.menuselect { float:left; display: block; padding: 6px 12px; text-decoration: none; font-weight: regular; color: #000; border-right: 1px solid #ccc; font-size: 1.1em; }
div.menubar a.menubar:hover, div.menubar a.menuselect { color: #c00; background-color: #fff; }
div.footerbar { width: 100%; margin: 0; padding: 4px 0; list-style: none; background-color: #f2f2f2; border-bottom: 1px solid #ccc; border-top: 1px solid #ccc; color: #000; font-size: 0.7em;}
div.tip { display: block; float: right ; background-color: #feb; padding: 6px; border: 1px solid #ccc; margin-right: 10px; margin-top: 10px; width: 35% }
.hiddenTab, .hiddenTr, object { display:none; }
.showTab { display:block; }
.showTr { display: table-row; }
.ui-widget { margin-left: 8px; margin-right: 8px; }
.ui-widget-content { font-size: 0.9em; }
.ui-tabs-panel { font-size: 1.1em; }
tr.alt { background:#f5f5f5; }
'} ] );
}


1;
