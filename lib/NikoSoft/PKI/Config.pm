##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: Config.pm 274 2015-01-27 22:42:49Z loopkin $
##

=head1 NAME

NikoSoft::PKI::Config

=cut

package NikoSoft::PKI::Config;

=head1 DESCRIPTION

Package to manage the configuration of NikoSoft PKI. Uses Config::Tiny

NB: this class has a destructor, in order to manage DB disconnection.

=cut

BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
}

use strict;
use NikoSoft::PKI::Utils qw(asn1_time utf8_print print_show_err);
use Config::Tiny;
use DBI;
use NikoSoft::PKI::Log;
use NikoSoft::PKI::AccessControl;
use NSConfig;

our  $DBFILE="pkiconfdb.sqlite";

=head1 METHODS

=head2 new

Constructor. Initializes the conf db if needed.

=cut
sub new {
	my $class = shift;
	mkdir("$CAPATH") if (!(-f $CAPATH));
	if (!(-f "$CAPATH/$DBFILE")) {
		my $dbh = DBI->connect("dbi:SQLite:dbname=$CAPATH/$DBFILE","","") or die $DBI::errstr;
		my $sth=$dbh->prepare("CREATE TABLE config (data TEXT)");
		$sth->execute();
		$sth=$dbh->prepare("INSERT INTO config VALUES('[general]')");
		$sth->execute();
	}
	my $self = { }; 
	$self->{dbh} = DBI->connect("dbi:SQLite:dbname=$CAPATH/$DBFILE","","");
	$self->{configdata} = Config::Tiny->read_string(_get_config_data($self));				
	$self->{accesscontrol} = NikoSoft::PKI::AccessControl->new();				
	bless $self, $class;
	return $self;
}

sub DESTROY {
	my $self = shift;
	my $dbh = $self->{dbh};
	$dbh->disconnect();
}

=head2 save ($output, $nolog, $bypass_acl)

Saves the configuration. Configuration saving can only be done by user 'admin'.

=cut
sub save {
	my $self = shift;
	my $output=shift;
	my $nolog=shift;
	my $bypass_acl=shift;
	if ((!($self->{accesscontrol}->is_connected_user_admin()))&&(!$bypass_acl)) {
		print_show_err($output, "Settings can be modified only by 'admin' user.");
		return;
	}
	$self->_set_config_data($self->{configdata}->write_string()) or return undef;
	my $log=NikoSoft::PKI::Log->new();
	$log->log_success("CONF_SAVE","$CAHOSTNAME Configuration",$self->_get_config_data()) unless ($nolog);
	return 1;
}

=head2 data

Fetches the configuration as an HASHREF

=cut
sub data {
	my $self = shift;
	my $param = shift;
	if ($param) {
		my $value=$self->{configdata}->{general}->{$param};
		$value=~ s/\\n/\n/g if ($value);
		return $value;
	}
	return $self->{configdata}->{general};
}

=head2 set_data ($name,$value)

Sets configuration item $name, with value $value

=cut
sub set_data {
	my $self = shift;
	my ($name,$value,$default)=@_;
	$value=$default unless ($value);
	$value=~ s/\n/\\n/g;
	$self->{configdata}->{general}->{$name}=$value;
}


sub _get_config_data {
	my $self = shift;
	my $dbh = $self->{dbh};
	my @rv=$dbh->selectrow_array("SELECT data FROM config");
	return $rv[0];		
}

sub _set_config_data {
	my $self = shift;
	my ($data)=@_;
	my $dbh = $self->{dbh};
	my $sth=$dbh->prepare("UPDATE config SET data='$data'");
	if (!$sth->execute()) {
		my $log=NikoSoft::PKI::Log->new();
		$log->log_error("CONFIG_DB_ERROR","CONFIG",$dbh->errstr);
		return undef;
	}
	return 1;		
}

1;
