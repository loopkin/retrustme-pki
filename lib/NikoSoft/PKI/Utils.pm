##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: Utils.pm 371 2015-12-01 21:33:51Z loopkin $
##

=head1 NAME

NikoSoft::PKI::Utils

=cut

package NikoSoft::PKI::Utils;

=head1 DESCRIPTION

Miscellaneous utilities used within NikoSoft PKI. This module exports all its functions.

=cut

BEGIN {
	use Exporter 'import';
	our @EXPORT_OK = qw(asn1_time time_to_asn1_time utf8_print sendmail mylock myunlock dumpfile print_show_err is_asn1_date_soon validate_data str2file guess_c_o_from_email);
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
}

use strict;

use Fcntl qw(:flock);
use Time::Piece;
use NikoSoft::PKI::Log;
use NikoSoft::PKI::Config;
use NikoSoft::PKI::CryptoTools qw(parse_pkcs10);
use NSConfig;
use Encode;
use MIME::Base64;

=head1 METHODS

=head2 new

Constructor. For now, does nothing.

=cut
sub new { 
	my $class=shift;
	my $self= {};
	bless $self, $class;
	return $self;
}

=head2 asn1_time ( $asntime )

Input: date/time in OpenSSL ASN1 UTCTime Format

Output: date/time in human readable format

=cut
sub asn1_time {
	my $asntime=shift;
	return "" unless $asntime;
	my $time=Time::Piece->strptime(substr($asntime,0,10),"%y%m%d%H%M");
	return $time->strftime("%F %H:%M");
}

=head2 time_to_asn1_time ( $sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst )

Converts inputted time (from localtime() or gmtime()) to ASN1 UTCTime format (YYMMDDHHMMSSZ).

=cut
sub time_to_asn1_time {
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=@_;
        $year = sprintf("%02d", $year % 100);
	$mon=sprintf("%02d",$mon+1);
	$mday=sprintf("%02d",$mday);
	$hour=sprintf("%02d",$hour);
	$min=sprintf("%02d",$min);
	$sec=sprintf("%02d",$sec);
	return "$year$mon$mday$hour$min$sec"."Z";
}


=head2 utf8_print

Input: string with '\'-encoded UTF-8 (from OpenSSL, typically)
Output: string with decoded UTF-8

=cut
sub utf8_print {
	my $string=shift;
	$string=~s/\\x([0-9a-f]{2})\\x([0-9a-f]{2})/Encode::encode('UCS-2BE',pack("U",hex($1.$2)))/egi;
	$string=~s/\\x00//g;
	return Encode::decode_utf8($string);
}

=head2  sendmail ( $dest, $subject, $message, $p12 )

MAIL out: technically sends mail, using command-line "sendmail"

=cut
sub sendmail {
        my ($dest, $subject, $message, $config, $p12)=@_;
        my $adminmail=$config->data('adminmail');
        my $mail=<<__EOF__;
From: $adminmail
To: $dest
Subject: $subject
MIME-Version: 1.0
Content-Transfer-Encoding: binary
Content-Type: multipart/mixed; boundary="_----------mimeBoundary4P12"

--_----------mimeBoundary4P12
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

$message
__EOF__

        if ($p12) {
                $mail.="\n--_----------mimeBoundary4P12\nContent-Type: application/x-pkcs12; name=\"secure_store.p12\"\nContent-Transfer-Encoding: base64\n\n";
                $mail.=encode_base64(dumpfile($p12));
        }
        $mail.="\n--_----------mimeBoundary4P12--\n.\n";
        open (SML,"| /usr/sbin/sendmail -t");
        print SML $mail;
        close SML;
}

=head2 mylock

Locks context, typically for OpenSSL CA related calls. Tries every second for two minutes to lock, and then dies in error.

=cut
sub mylock {
        open LOCK, ">$TMPDIR/lock";
        my $count=0;
        while (!flock(LOCK, LOCK_EX)) {
        	die "Fatal Error: cannot get lock." if ($count >=120);
                sleep(1);
                $count++;
        }
        close LOCK;
}

=head2 myunlock

releases lock set by mylock, typically for OpenSSL CA related calls

=cut
sub myunlock {
        open LOCK, ">$TMPDIR/lock";
        while (!flock(LOCK, LOCK_UN)) {
                sleep(1);
        }
        close LOCK;
}

=head2 dumpfile ( $filename )

Dumps the content of the file $filename, as a string

=cut
sub dumpfile {
	my $filename = shift;
	my $res="";
	open FILE, "$filename" or return undef;
	binmode(FILE);
	while (<FILE>) {
		$res.=$_;
	}
	close FILE;
	return $res;
}

=head2 str2file ( $filename, $str, $binmode )

Dumps the string $str to the file $filename, in binmode if $binmode;

=cut
sub str2file {
	my $filename = shift;
	my $str = shift;
	my $binmode=shift;
	open OUT, ">$filename" or return undef;
	binmode(OUT) if ($binmode);
	print OUT $str;
	close OUT;
	return 1;
}


=head2 print_show_err ( output, strings... )

	Used to print output from command lines, removing confidential stuff, and emphazising errors

 	output - Sets the method of output:
		0 - verbose HTML output
		1- error-only JSON output
		2- error-only HTML output

=cut
sub print_show_err {
	my $output=shift;
	my @strings=@_;
	my $out=1;
	foreach my $string (@strings) {
		if ($string=~m/error/i) {
			my $log=new NikoSoft::PKI::Log();
			if ($string=~m/openssl/i) {
				$log->log_error("CRYPTO_OPERATION","CA","Error: $string");
			} else {
				$log->log_error("UNKNOWN_OPERATION","Unknown","Error: $string");
			}		
			if ($output==1) {
				$string=~s/\n//g;
				$string=~s/TXT_DB error number 2/TXT_DB error number 2: certificate subject already exists/;
				print "{\n\t\"status\":\"error\",\n\t\"code\":\"500\",\n\t\"msg\":\"$string\"\n}";
				exit;
			} elsif (($output==0)||($output==2)) {
				my $error_str=_T('ERROR');
				$string=~s/error/<span class=\"error\">$error_str<\/span>/gi;
                                $string=~s/$CAPATH\///g;
                                $string=~s/$TMPDIR\///g;
                                print $string;
			}			
			$out=0;
		} else {
                        $string=~s/$CAPATH\///g;
                        $string=~s/$TMPDIR\///g;
                        print $string if (($output==0)||($DEBUG));
                }
	}
	return $out;
}

=head2 is_asn1_date_soon ( $date, $mode )

Tells whether $date is in the "near" future. "Near" being set by $mode.

	$date - ASN1 UTCTime formatted date
	$mode - mode 1: in less than a month, mode 2: in exactly a month, mode 3: today or less (yesterday, etc.)

=cut
sub is_asn1_date_soon {
	my $date = shift;
	my $mode = shift; # mode 1: in less than a month, mode 2: in exactly a month, mode 3: today or less (yesterday, etc.)
	my $t=gmtime;
	my $day=Time::Piece->strptime($t->strftime("%y%m%d"),"%y%m%d");
	my $daynextmonth=$day->add_months(1);
	my $exptime=Time::Piece->strptime(substr($date,0,6),"%y%m%d");
        if ($mode==1) {
 		return 1 if (($exptime>=$day)&&($exptime<=$daynextmonth));
        }
        if ($mode==2) {
		return 1 if ($exptime==$daynextmonth);
	}
	if ($mode==3) {
		return 1 if ($exptime<=$day) ;
	}
	return 0;
}

=head2 validate_data ($data, $type)

Validates the data format

	$data - the data to validate
	$type - type of the data to validate. can be 'pkcs10', 'email', 'country', 'number', 'oid', 'serial', 'password' or 'string'. by default, it's 'string'

	Note: in case "pkcs10" data is to be validated, consistance is tested (key size should be min 2048, else it should be GOST)
	
=cut
sub validate_data {
	my $data = shift;
	my $type=shift || '';
	if ($type eq 'pkcs10') {
		return 0 unless ($data=~m/CERTIFICATE REQUEST\-\-/);
		my ($alg_oid,$keysize,$dn)=parse_pkcs10($data);
		return 0 unless ($alg_oid);
		if ($alg_oid eq '1.2.643.2.2.3') {
			return 1;
		} elsif ($alg_oid=~m/1.2.840.10045.4/) {
			return 1;
		} else {
			return 1 if ($keysize >= 2048);
		}
		return 0;
	}
	return 0 if (length($data)>64);
	if ($type eq 'email') {
		return 0 if ($data=~m/[^0-9a-z\@\.\_\-\+]/i);
	}
	if ($type eq 'country') {
		return 0 if ($data=~m/[^A-Z]+/);
	}
	if ($type eq 'number') {
		return 0 if ($data=~m/[^0-9]+/);
		return 1;
	}
	if ($type eq 'serial') {
		return 0 if ($data=~m/[^0-9a-f\-\.\:]+/i);
	}
	if ($type eq 'oid') {
		return 0 if ($data=~m/[^0-9\.]+/);
	}
	if ($type eq 'password') {
		return 1 if (($data=~m/[a-zA-Z0-9\@\.\_\-\+]{6,16}/) && ($data=~m/[a-z]/i) && ($data=~m/[0-9]/i));
		return 0;
	}
	if ($type eq 'profile') {
		return 0 if ($data=~m/[^a-z\_]+/i);
	}
	if ($type eq 'uid') {
		return 0 if ($data=~m/[^0-9a-z\_]+/i);
	}
	if ($type eq 'utfstring') {
		return 0 if ($data=~m/[\"\'\/]+/);
	}
	return 0 unless ($data);
	return 0 if ($data=~m/[\"\']+/);
	return 1;
}

=head2 guess_c_o_from_email ( $email )

Tries to guess Country and Organization from email, and returns ($country, $organization, $root_dn). If it cannot guess, returns (undef,undef,undef). In case Country is more than 2 characters, returns the $root_dn as /DC=.../DC=...

	$email - email to guess information from

=cut
sub guess_c_o_from_email {
	my $email = shift;
	$email=~s/[0-9a-z\.\_\-\+]+\@([0-9a-z\.\_\-\+]+)/$1/i;
	my $country=lc($email);
	$country=~s/[0-9a-z\_\-]+\.([0-9a-z\_\-]+)$/$1/;
	$country=uc($country);
	my $org=lc($email);
	$org=~s/([0-9a-z\_\-]+)\.[0-9a-z\_\-]+$/$1/;
	return (undef,undef,undef) if ((!$country)||(!$org));
	my $root_dn=undef;
	$root_dn="/C=$country/O=$org" if (length($country)==2);
	$root_dn="/DC=$country/DC=$org" if (length($country)>=3);
	return (undef,undef,undef) if (!$root_dn);
	return ($country,$org,$root_dn);
}



1;
