##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: SCEP.pm 364 2015-09-29 17:24:56Z loopkin $
##

=head1 NAME

NikoSoft::PKI::SCEP

=cut

package NikoSoft::PKI::SCEP;

=head1 DESCRIPTION

perl module for the management of SCEP protocol
Contains all business methods and OpenCA-SCEP (heavily modified) calls.

=cut
 
BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
}
  
use strict;
use MIME::Base64;
use NikoSoft::PKI::Utils qw(utf8_print time_to_asn1_time mylock myunlock dumpfile print_show_err sendmail str2file validate_data);
use NikoSoft::PKI::CryptoTools qw(parse_pkcs10 get_random);
use NikoSoft::PKI::Log;
use NikoSoft::PKI::Config;
use NikoSoft::PKI::CADB;
use NSConfig;

=head1 METHODS

=head2 new ( $config ) 

Constructor of the NikoSoft::PKI::SCEP module.
	
	$config - A NikoSoft::PKI::Config instance
	
=cut
sub new {
	my $class=shift;
	my ($config,$log)=@_;
	my $self= {};
	$self->{log}=$log;
	$self->{config}=$config;
	bless $self, $class;
	return $self;
}

=head2 initiate_ops ()

Iniates SCEP cryptographic operations. MUST be called before any combination of TSA operations.

=cut
sub initiate_ops {
	my $self = shift;
	my $config=$self->{config};
	my $optconf=$cryptosystems{$CRYPTO}{openssl_cnf_sect};
        open (CNF, ">$TMPDIR/lpkiscep.cnf");
        print CNF <<__EOF__;
HOME		= $TMPDIR
RANDFILE	= $CAPATH/.rnd

$optconf

[ req ]
distinguished_name      = req_distinguished_name

[ req_distinguished_name ]
commonName                      = Common Name
commonName_max                  = 64

[ ca_cert ]
basicConstraints=critical,CA:true,pathlen:0
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always
keyUsage=cRLSign, keyCertSign

__EOF__
        close CNF;
        return 1;
}

=head2 gen_scep ()

Generates the SCEP internal, technical CA.

=cut
sub gen_scep {
	my $self = shift;
	my $o=$self->{config}->data('organization');
	my $c=$self->{config}->data('country');
	$self->_exec_openssl("req -new -newkey rsa:2048 -utf8 -set_serial 0x4863 -x509 -nodes -days 7300 -subj \"/C=$c/O=$o/CN=SCEPCA\" -keyout \"$CAPATH/private/scep.key\" -out \"$CAPATH/scep.crt\" -extensions ca_cert -batch 2>&1") or return undef;
	$self->_exec_openssl("x509 -in \"$CAPATH/scep.crt\" -out \"$CAPATH/scep.der\" -outform DER");
	$self->change_challenge();
}


=head2 get_scepp7_operation ( $scepreqfn )

Get the "messageType" field from the SCEP PKCS#7 request contained in file $scepreqfn

=cut
sub get_scepp7_operation {
	my $self = shift;
	my $scepreqfn = shift;
	my $pkioperation=`$OPENCASCEP -noout -print_msgtype -in "$scepreqfn"`;
	$pkioperation=~s/[0-9\(\)\ ]*\n//g;
	return 'PKCSReq' if ($pkioperation=~m/PKCSReq/);
	return $pkioperation;
}

=head2 get_scepp7_p10 ( $scepreqfn )

Retrieves the PKCS#10 contained in the SCEP PKCS#7 request in file $scepreqfn.

In the process of doing so, this method also checks the validity of the challenge, and, if it's invalid, the validity of the certificate signing the request (SCEP certificate renewal)

=cut
sub get_scepp7_p10 {
	my $self = shift;
	my $scepreqfn = shift;
	my $pkcs10=`$OPENCASCEP -key \"$CAPATH/private/scep.key\" -noout -print_req -in "$scepreqfn"`;
	if (!validate_data($pkcs10,'pkcs10')) {
		$self->{log}->log_error('SCEP_REQ_ERROR','SCEP_CONNECTOR',"Cannot parse SCEP request $pkcs10");
		return undef;
	}
	my ($alg_oid,$keysize,$dn,$challenge)=parse_pkcs10($pkcs10);
	my $cert=`$OPENCASCEP -key \"$CAPATH/private/scep.key\" -noout -print_scert -in "$scepreqfn"`;
	str2file("$scepreqfn.scr",$cert);
	if ($challenge ne $self->{config}->data('scep_challenge')) {
		$self->{log}->log_error('SCEP_REQ_ERROR','SCEP_CONNECTOR',"Incorrect or empty challenge");
		my $dn=`echo \"$cert\" | $OPENSSL x509 -noout -subject`;
		$dn=~s/subject=\ //g;
		my $cadb=NikoSoft::PKI::CADB->new($self->{log});
		my @res=$cadb->get_certificate_by_dn($dn,1,1);
		if (scalar(@res)==0) {
			$self->{log}->log_error('SCEP_REQ_ERROR','SCEP_CONNECTOR',"Incorrect request certificate");
			return undef;			
		}		
	}
	my @dnparts=split(/\//,$dn);
	my $email = $self->{config}->data('adminmail');
	foreach my $dnpart (@dnparts) {
		# Retrieve email from emailAddress, if exists
		if ($dnpart=~m/1.2.840.113549.1.9.1/) {
			$email=$dnpart;
			$email=~s/1.2.840.113549.1.9.1=//g;		
		}
		# Retrieve CN either from unstructuredName or from commonName
		if ($dnpart=~m/1.2.840.113549.1.9.2/) {
			$dn=$dnpart;
			$dn=~s/1.2.840.113549.1.9.2=//g;
		} elsif ($dnpart=~m/2.5.4.3/) {
			$dn=$dnpart;
			$dn=~s/2.5.4.3=//g;
		}
	}
	return undef if ($dn eq '');
	$self->{log}->log_success('SCEP_REQUEST','SCEP_CONNECTOR',"Got valid and authenticated SCEP request for $dn, from: ".$ENV{REMOTE_ADDR});
	return ($pkcs10,$dn,$email);
}

=head2 change_challenge

Changes the SCEP challenge inside the PKI config

=cut
sub change_challenge {
	my $self = shift;
	my $challenge=uc(get_random(8));
	$self->{config}->set_data('scep_challenge',$challenge);
	if ($self->{config}->save(1,1,1)) {
		$self->{log}->log_success('SCEP_GEN_CHALLENGE','SCEP_CONNECTOR',"Generated new SCEP challenge");
	} else {
		$self->{log}->log_error('SCEP_GEN_CHALLENGE','SCEP_CONNECTOR',"Could not save new SCEP challenge");
	}
}

=head2 get_scepp7_reply ($scepreqfn, $crtfn, $p10fn )

Build a SCEP PKCS#7 reply of messageType "CertRep" using the SCEP PKCS#7 request in file $scepreqfn, produced certificate in $crtfn, and PKCS#10 request in $p10fn. Returns it Base64-encoded.

=cut
sub get_scepp7_reply {
	my $self = shift;
	my $scepreqfn = shift;
	my $crtfn = shift;
	my $p10fn = shift;
	my $cmd='';
	if ($scepreqfn) {
		$cmd.="-reccert \"$scepreqfn.scr\" " if (-f "$scepreqfn.scr");		
	}
	my $scepreply=`$OPENCASCEP -key \"$CAPATH/private/scep.key\" -new -msgtype "CertRep" -issuedcert "$crtfn" -status SUCCESS -in "$scepreqfn" -crlfile "$CAPATH/ca.crl" -reqfile "$p10fn" $cmd -CAfile "$CAPATH/ca.crt"  -signcert "$CAPATH/scep.crt"`;
	return undef unless ($scepreply);
	return $scepreply;
}

=head2 get_scepp7_error ()

Builds a SCEP PKCS#7 generic error reply of type "CertRep". Returns it Base64-encoded.

=cut
sub get_scepp7_error {
	my $self = shift;
	my $filename = shift;
	my $cmd='';
	if ($filename) {
		$cmd.="-in \"$TMPDIR/$filename\" " if (-f "$TMPDIR/$filename");
		$cmd.="-reccert \"$TMPDIR/$filename.scr\" " if (-f "$TMPDIR/$filename.scr");		
	}
	my $scepreply=`$OPENCASCEP -new -msgtype "CertRep" -status FAILURE -failinfo badRequest $cmd -key "$CAPATH/private/scep.key" -signcert "$CAPATH/scep.crt"`;
	if (!$scepreply) {
		$self->{log}->log_error('SCEP_GEN_ERROR','SCEP_CONNECTOR',"Could not generate SCEP generic error message: fatal error.");
		return '';
	}
	return $scepreply;
}


# Private method to run openssl cleanly
sub _exec_openssl {
	my $self=shift;
	my $command = shift;
	my $ret=`OPENSSL_CONF="$TMPDIR/lpkiscep.cnf" $OPENSSL $command`;
	my $rcode=$?;
	if ($rcode>0) {
		$command="" unless ($DEBUG);
		# We should NOT print anything, because of SCEP specifications...
		$self->{log}->log_error("ERROR: failure while executing openssl $command\nReturn code is $rcode, output is:\n$ret");
		return undef;
	}
	# below is to return something in case execution went fine, but we have nothing to return
	$ret="OK" if ((!$ret)||($ret eq ''));
	return $ret;
}


1;
