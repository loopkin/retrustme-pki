##
## (c) 2010-2015 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: TSA.pm 379 2015-12-27 17:36:01Z loopkin $
##

=head1 NAME

NikoSoft::PKI::TSA

=cut

package NikoSoft::PKI::TSA;

=head1 DESCRIPTION

perl module for the management of the TimeStamping Authority, in the sense of RFC 3161
Contains all business methods and OpenSSL calls.

=cut
 
BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
}
  
use strict;
use MIME::Base64;
use NikoSoft::PKI::Utils qw(utf8_print time_to_asn1_time mylock myunlock dumpfile print_show_err sendmail);
use NikoSoft::PKI::Log;
use NikoSoft::PKI::Config;
use NikoSoft::PKI::CA;
use NSConfig;

=head1 METHODS

=head2 new ( $config ) 

Constructor of the NikoSoft::PKI::TSA module.
	
	$config - A NikoSoft::PKI::Config instance
	
=cut
sub new {
	my $class=shift;
	my ($config)=@_;
	my $self= {};
	$self->{log}=NikoSoft::PKI::Log->new();
	$self->{config}=$config;
	$self->{keyfile}="$CAPATH/private/tsa.key";
	$self->{certfile}="$CAPATH/tsa.crt";
	bless $self, $class;
	return $self;
}

=head2 initiate_ops ()

Iniates TSA cryptographic operations. MUST be called before any combination of TSA operations.

=cut
sub initiate_ops {
	my $self = shift;
	my $config=$self->{config};
	my $oid = $config->data('tsa_oid') || '1.3.6.1.4.1.4146.2.2';
	my $optconf=$cryptosystems{$CRYPTO}{openssl_cnf_sect};
        open (CNF, ">$TMPDIR/lpkitsa.cnf");
        print CNF <<__EOF__;
HOME		= $TMPDIR
RANDFILE	= $CAPATH/.rnd

$optconf

[ req ]
distinguished_name      = req_distinguished_name

[ req_distinguished_name ]
commonName                      = Common Name
commonName_max                  = 64

[ tsa ]
default_tsa = tsa_config

[ tsa_config ]
dir			= $CAPATH
serial		= \$dir/tsaserial          # The current serial number
default_policy	= $oid
digests		= sha1, sha256
tsa_name	= yes

__EOF__
        close CNF;
        return 1;
}

=head2 gen_tsa_key_csr ( $oid )

Generates the timestamp authority's key and CSR

=cut
sub gen_tsa_key_csr {
	my $self = shift;
	my $oid = shift || '1.3.6.1.4.1.4146.2.2';
	my $renew = shift;
	my $config=$self->{config};
	my $log=$self->{log};
	system("echo '01' > $CAPATH/tsaserial");
	$self->{config}->set_data('tsa_oid',$oid);

	print_show_err(1,`$cryptosystems{$CRYPTO}{delkey_tsa} 2>/dev/null`) if (($renew)&&($cryptosystems{$CRYPTO}{delkey_tsa}));
	print_show_err(1,`$cryptosystems{$CRYPTO}{genkey_tsa} 2>&1`) if ($cryptosystems{$CRYPTO}{genkey_tsa});
	my $req_params=$cryptosystems{$CRYPTO}{req_genkey_tsa};
	my $tmp="tsa_$oid";
	$self->_exec_openssl("req -new $req_params -utf8 -out \"$TMPDIR/$tmp.csr\" -subj \"/CN=TimeStampingAuthority\" 2>&1") or return undef;
	my $csr=dumpfile("$TMPDIR/$tmp.csr");
	unlink("$TMPDIR/$tmp.csr");
	return undef unless($csr);
	$self->{config}->save();
	chmod(0440, $self->{keyfile});
	return $csr;
}

=head2 gen_tsa ( $oid )

Generates the timestamp authority (key, CSR and certificate)

=cut
sub gen_tsa {
	my $self = shift;
	my $oid = shift || '1.3.6.1.4.1.4146.2.2';
	my $renew = shift;
	my $csr=$self->gen_tsa_key_csr($oid, $renew);
	my $config=$self->{config};
	my $log=$self->{log};
	my $ca=new NikoSoft::PKI::CA($config,$log);
	$ca->set_output_mode(1);
	my %dn_params;
	$dn_params{cn}=$CAHOSTNAME;
	$dn_params{email}=$config->data('adminmail');
	my ($cert,$certserial)=$ca->sign_csr($csr,'tsp_cert',\%dn_params,1);

	if ($cert) {
		_string2file($cert,$self->{certfile});
	}
	return 1;
}

=head2 sign_tsq ( $tsq_file )

Signs the timestamp request stored in $tsq_file

=cut
sub sign_tsq {
	my $self = shift;
	my $tsq_file = shift;
	my $config=$self->{config};
	
	my $certfile=$self->{certfile};
	my $keyfile=$self->{keyfile};
	system("/usr/bin/ntpstat >/dev/null 2>&1");
	return $self->error_tsr() unless ($? == 0);
	$self->_exec_openssl("ts -reply $cryptosystems{$CRYPTO}{tsa_sign} -queryfile \"$TMPDIR/$tsq_file\" -signer \"$certfile\"  -chain \"$CAPATH/ca.crt\" -out \"$TMPDIR/$tsq_file.tsr\" 2>&1") or return $self->error_tsr();
	my $tsr=dumpfile("$TMPDIR/$tsq_file.tsr");
	unlink("$TMPDIR/$tsq_file.tsr");
	
	return $tsr;
}

=head2 error_tsr ()

Returns a generic error timestamp token. MUST be used while managing errors, to comply with RFC 3161.

=cut
sub error_tsr {
	my $self = shift;
	# Below is a generic error Timestamp token, to be returned in case of error...
	my $error=decode_base64('MBkwFwIBAjANDAt1bnNwZWNpZmllZAMDAQAC');
	return $error;
}


sub _string2file {
	 my ($string,$filename)=@_;
	 open FIL, ">$filename";
	 print FIL $string;
	 close FIL;
}

# Private method to run openssl cleanly
sub _exec_openssl {
	my $self=shift;
	my $command=shift;
	my $ret=`OPENSSL_CONF="$TMPDIR/lpkitsa.cnf" $OPENSSL $command`;
	my $rcode=$?;
	if ($rcode>0) {
		$command="" unless ($DEBUG);
		# We should NOT print anything, because of RFC 3161 specifications...
		$self->{log}->log_error('TSA_OPENSSL_ERROR','TSA_CONNECTOR',"ERROR: failure while executing openssl $command\nReturn code is $rcode, output is:\n$ret");
		return undef;
	}
	# below is to return something in case execution went fine, but we have nothing to return
	$ret="OK" if ((!$ret)||($ret eq ''));
	return $ret;
}


1;
