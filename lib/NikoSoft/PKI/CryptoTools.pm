##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: CryptoTools.pm 418 2017-11-27 19:04:41Z loopkin $
##

=head1 NAME

NikoSoft::PKI::CryptoTools

=cut

package NikoSoft::PKI::CryptoTools;

=head1 DESCRIPTION

Utility class for cryptographic operations. This module exports all its functions.

=cut

BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
	use Exporter 'import';
        our @EXPORT_OK = qw(openssl_cnf gen_friendly_name get_random parse_pkcs10 parse_x509 exec_openssl exec_pipe_openssl compute_response_from_challenge);
}

use strict;
use NSConfig;
use utf8;
use Math::BigInt;
use MIME::Base64;
use Convert::ASN1;
use Text::Unidecode;
use NikoSoft::PKI::Utils qw(print_show_err);

# Unused...
sub new {
	my $class=shift;
	my $self={};
	bless $self,$class;
	return $self;
}

=head1 METHODS

=head2 openssl_cnf ( $config, $dns, $msupn )

FILE out: outputs openssl configuration onto $TMPDIR/lpkica.cnf

=cut
sub openssl_cnf {
        my $config=shift;
        my $short=shift;
        my $crldp=$config->data()->{crldp} || "http://$CAHOSTNAME$CAPUBLICURI/ca.crl";
        my $aiaca=$config->data()->{aiaca} || "http://$CAHOSTNAME$CAPUBLICURI/ca.crt";
        my $cpoid=$config->data()->{cpoid} || '2.5.29.32.0';
        my $optconf=$cryptosystems{$CRYPTO}{openssl_cnf_sect};
        open (CNF, ">$TMPDIR/lpkica.cnf");
        print CNF <<__EOF__;
HOME            = $TMPDIR
RANDFILE        = $CAPATH/.rnd

$optconf

[ ca ]
default_ca      = CA_default            # The default ca section

[ CA_default ]
dir             = $CAPATH
certs           = \$dir/certs           # Where the issued certs are kept
crl_dir         = \$dir/crl             # Where the issued crl are kept
database        = \$dir/index.txt       # database index file.
certificate     = \$dir/ca.crt  # The CA certificate
serial          = \$dir/serial          # The current serial number
crlnumber       = \$dir/crlnumber       # the current crl number
default_days    = 730                   # how long to certify for
default_crl_days= 7                     # how long before next CRL
default_md      = sha256                # which md to use.
crl_extensions  = crl_ext
new_certs_dir   = \$dir/newcerts
policy          = policy_match
email_in_dn     = yes
copy_extensions = none

[ CA_dc ]
dir             = $CAPATH
certs           = \$dir/certs           # Where the issued certs are kept
crl_dir         = \$dir/crl             # Where the issued crl are kept
database        = \$dir/index.txt       # database index file.
certificate     = \$dir/ca.crt  # The CA certificate
serial          = \$dir/serial          # The current serial number
crlnumber       = \$dir/crlnumber       # the current crl number
default_days    = 730                   # how long to certify for
default_crl_days= 7                     # how long before next CRL
default_md      = sha256                # which md to use.
crl_extensions  = crl_ext
new_certs_dir   = \$dir/newcerts
policy          = policy_match
email_in_dn     = yes
copy_extensions = copy

[ policy_match ]
domainComponent            = optional
domainComponent            = optional
countryName            = optional
organizationName        = optional
organizationalUnitName = optional
serialNumber            = optional
commonName              = supplied
emailAddress            = optional
x500UniqueIdentifier    = optional
UID                             = optional

[ req ]
distinguished_name      = req_distinguished_name

[ req_distinguished_name ]
commonName                      = Common Name
commonName_max                  = 64

[ ca_cert ]
basicConstraints=critical,CA:true,pathlen:0
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always
keyUsage=cRLSign, keyCertSign
certificatePolicies=$cpoid

[ crl_ext ]
authorityKeyIdentifier=keyid:always

__EOF__

        unless($short) {
              print CNF <<__EOF__;
[ auth_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,digitalSignature
extendedKeyUsage=clientAuth,emailProtection
subjectAltName=email:copy
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca

[ sign_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,digitalSignature,nonRepudiation
extendedKeyUsage=emailProtection
subjectAltName=email:copy
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca

[ serv_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,digitalSignature,keyEncipherment
extendedKeyUsage=clientAuth,serverAuth
subjectAltName=email:copy,DNS:\$ENV::DNS
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca

[ tsp_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,digitalSignature
extendedKeyUsage=critical,timeStamping
subjectAltName=email:copy
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca

[ scl_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,digitalSignature
extendedKeyUsage=clientAuth,msSmartcardLogin
subjectAltName=otherName:msUPN;UTF8:\$ENV::MSUPN
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca

[ dc_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,digitalSignature,keyEncipherment
extendedKeyUsage=clientAuth,serverAuth,msSmartcardLogin
1.3.6.1.4.1.311.20.2=ASN1:BMP:DomainControllerAuthentication
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca

[ enc_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,dataEncipherment,keyEncipherment
extendedKeyUsage=emailProtection,msEFS
subjectAltName=email:copy
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca

[ ocsp_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,digitalSignature
extendedKeyUsage=OCSPSigning
subjectAltName=email:copy
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca
1.3.6.1.5.5.7.48.1.5=ASN1:NULL

[ op_cert ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
keyUsage=critical,digitalSignature
extendedKeyUsage=clientAuth
subjectAltName=email:copy
crlDistributionPoints=URI:$crldp
certificatePolicies=$cpoid
authorityInfoAccess=caIssuers;URI:$aiaca

__EOF__
        }
        close CNF;
}

=head2 gen_friendly_name ( $cn )

Output: Friendly Name (ASCII only, no spaces, reasonably unique)

=cut
sub gen_friendly_name {
        my ($fcn)=@_;
        $fcn=unidecode($fcn);
        $fcn=~s/\s/_/g;
        $fcn.='_'.get_random(2);
        return $fcn;
}

=head2 get_random ( $nbytes )

Generates $nbytes random bytes, using OpenSSL if possible, else reverting to PERL rand

=cut
sub get_random {
        my $nbytes = shift;
        $nbytes+=0;
        # Trying to get it from openssl
        my $res = exec_openssl(3,"rand -hex $nbytes 2>/dev/null");
        # ... but in case of failure ...
        if (!$res) {
                # ...reverting to cryptographically unsecure random...
                $res='';
                for (my $i=0;$i<$nbytes;$i++) {
                        $res.=''.int(rand(9));
                }
        } else {
		$res=~s/\n//g;
        }
        return $res;
}

=head2 parse_pkcs10 ( $pkcs10 )

Parses PKCS10 to return information relevant for management.

        $pkcs10 - the PKCS10 to parse
        
        Returns: Array ( $algorithm_oid, $key_size, $requested_dn )

=cut
sub parse_pkcs10 {
        my $pkcs10 = shift;
        # We need to convert to DER. And pay attention to new and old style PEM banners...
        # And we do it in a safe way...
        if($pkcs10=~m/CERTIFICATE\sREQUEST-----/ ) { 
                $pkcs10=~s/-----BEGIN NEW CERTIFICATE REQUEST-----//;
                $pkcs10=~s/-----END NEW CERTIFICATE REQUEST-----//;                
                $pkcs10=~s/-----BEGIN CERTIFICATE REQUEST-----//;
                $pkcs10=~s/-----END CERTIFICATE REQUEST-----//;
                $pkcs10=~s/^\n//g;
                $pkcs10 = decode_base64($pkcs10);
        }
        # Now we instantiate the parser, and parse...
        my $parser = _create_p10_parser();
        my $parsed = $parser->decode($pkcs10);
        return (undef,undef,undef,$parser->error()) unless($parsed);
        return ($parsed->{'signatureAlgorithm'}->{'algorithm'},$parsed->{'signature'}[1],
                        _dn_to_string($parsed->{'certificationRequestInfo'}->{'subject'}),
                        _challenge_from_attributes($parsed->{'certificationRequestInfo'}->{'attributes'}));
}

=head2 parse_x509 ( $certificate )

Parses X.509 certificate to return information relevant for management.

        $certificate - the X.509 certificate to parse
        
        Returns: Array ( $serialNumber, $issuer, $subject )

=cut
sub parse_x509 {
        my $cert = shift;
        # We need to convert to DER. And pay attention to new and old style PEM banners...
        # And we do it in a safe way...
        if($cert=~m/CERTIFICATE-----/ ) { 
                $cert=~s/-----BEGIN CERTIFICATE-----//;
                $cert=~s/-----END CERTIFICATE-----//;
                $cert=~s/^\n//g;
                $cert = decode_base64($cert);
        }
        # Now we instantiate the parser, and parse...
        my $parser = _create_x509_parser();
        my $parsed = $parser->decode($cert);
        return (undef,undef,undef,$parser->error()) unless($parsed);
	my $serial=Math::BigInt->new($parsed->{tbsCertificate}->{serialNumber});
	$serial=$serial->as_hex;
	$serial=~s/0x//;
        $serial='0'.$serial if (length($serial) %2);
        return ($serial,
                        _dn_to_string($parsed->{tbsCertificate}->{issuer}),
                        _dn_to_string($parsed->{tbsCertificate}->{subject}));
}

# Private method to convert DN Hashref to string
sub _dn_to_string {
    my $dn_hash = shift;
    my $dn='';
    foreach my $entry ( @{$dn_hash} ) {
        $dn.= '/'.$entry->[0]->{'type'} .'='. (values(%{$entry->[0]->{'value'}}))[0];
    }
    return $dn;
}

# Private method to parse challenge from CSR
sub _challenge_from_attributes {
    my $attributes_hash = shift;
    return undef unless ($attributes_hash);
    my $asn1 = Convert::ASN1->new();
    my $parsed=$asn1->prepare( <<__ASN1_DESC__ );
    DirectoryString ::= CHOICE {
      teletexString   TeletexString,
      printableString PrintableString,
      bmpString       BMPString,
      universalString UniversalString,
      utf8String      UTF8String,
      ia5String       IA5String,
      integer         INTEGER}

    challengePassword ::= DirectoryString
__ASN1_DESC__
    my $parser=$parsed->find('challengePassword');
    foreach my $entry ( @{$attributes_hash} ) {
        return undef if  (($entry->{'type'} eq '1.2.840.113549.1.9.7')&&(!($entry->{'values'}[0])));
        return ''.(values(%{$parser->decode($entry->{'values'}[0])}))[0] if ($entry->{'type'} eq '1.2.840.113549.1.9.7');
    }
    return undef;
}

# Private method to instantiate ASN1 Parser for PKCS#10
sub _create_p10_parser {
    my $asn1 = Convert::ASN1->new;
    my $asn1_description=<<__ASN1_DESCRIPTION__;
    CertificationRequest ::= SEQUENCE {
      certificationRequestInfo  CertificationRequestInfo,
      signatureAlgorithm        AlgorithmIdentifier,
      signature                 BIT STRING}

    CertificationRequestInfo ::= SEQUENCE {
      version       INTEGER ,
      subject       Name OPTIONAL,
      subjectPKInfo SubjectPublicKeyInfo,
      attributes    [0] Attributes OPTIONAL}

    AlgorithmIdentifier ::= SEQUENCE {
      algorithm  OBJECT IDENTIFIER,
      parameters Algorithms OPTIONAL}

    Algorithms ::= CHOICE {
        undef         ANY}

    SubjectPublicKeyInfo ::= SEQUENCE {
      algorithm        AlgorithmIdentifier,
      subjectPublicKey BIT STRING}

    Name ::= SEQUENCE OF RelativeDistinguishedName
    RelativeDistinguishedName ::= SET OF AttributeTypeAndValue
    AttributeTypeAndValue ::= SEQUENCE {
      type  OBJECT IDENTIFIER,
      value DirectoryString}

    Attributes ::= SET OF Attribute
    Attribute ::= SEQUENCE {
      type   OBJECT IDENTIFIER,
      values SET OF ANY}

    DirectoryString ::= CHOICE {
      teletexString   TeletexString,
      printableString PrintableString,
      bmpString       BMPString,
      universalString UniversalString,
      utf8String      UTF8String,
      ia5String       IA5String,
      integer         INTEGER}
__ASN1_DESCRIPTION__

    $asn1->prepare($asn1_description);
    my $parsed = $asn1->find('CertificationRequest');
    return $parsed;
}

# Private method to instantiate ASN1 Parser for X509
sub _create_x509_parser {
    my $asn1 = Convert::ASN1->new;
    my $asn1_description=<<__ASN1_DESCRIPTION__;

Certificate ::= SEQUENCE  {
	tbsCertificate		TBSCertificate,
	signatureAlgorithm	AlgorithmIdentifier,
	signature		BIT STRING
	}

TBSCertificate  ::=  SEQUENCE  {
	version		    [0] EXPLICIT Version OPTIONAL,  --DEFAULT v1
	serialNumber		CertificateSerialNumber,
	signature		AlgorithmIdentifier,
	issuer			Name,
	validity		Validity,
	subject			Name,
	subjectPublicKeyInfo	SubjectPublicKeyInfo,
	issuerUniqueID	    [1] IMPLICIT UniqueIdentifier OPTIONAL,
		-- If present, version shall be v2 or v3
	subjectUniqueID	    [2] IMPLICIT UniqueIdentifier OPTIONAL,
		-- If present, version shall be v2 or v3
	extensions	    [3] EXPLICIT Extensions OPTIONAL
		-- If present, version shall be v3
	}

Version ::= INTEGER  --{  v1(0), v2(1), v3(2)  }

CertificateSerialNumber ::= INTEGER

Validity ::= SEQUENCE {
	notBefore		Time,
	notAfter		Time}

Time ::= CHOICE {
	utcTime			UTCTime,
	generalTime		GeneralizedTime}

UniqueIdentifier ::= BIT STRING

AlgorithmIdentifier ::= SEQUENCE {
      algorithm  OBJECT IDENTIFIER,
      parameters Algorithms OPTIONAL}

Algorithms ::= CHOICE {
        undef         ANY}

SubjectPublicKeyInfo ::= SEQUENCE {
      algorithm        AlgorithmIdentifier,
      subjectPublicKey BIT STRING}

Name ::= SEQUENCE OF RelativeDistinguishedName
RelativeDistinguishedName ::= SET OF AttributeTypeAndValue
AttributeTypeAndValue ::= SEQUENCE {
      type  OBJECT IDENTIFIER,
      value DirectoryString}

Attributes ::= SET OF Attribute
Attribute ::= SEQUENCE {
      type   OBJECT IDENTIFIER,
      values SET OF ANY}

DirectoryString ::= CHOICE {
      teletexString   TeletexString,
      printableString PrintableString,
      bmpString       BMPString,
      universalString UniversalString,
      utf8String      UTF8String,
      ia5String       IA5String,
      integer         INTEGER}

Extensions ::= SEQUENCE OF ANY

__ASN1_DESCRIPTION__

    $asn1->prepare($asn1_description);
    print $asn1->error();
    my $parsed = $asn1->find('Certificate');
    return $parsed;
}


=head2 exec_openssl ($output, $command, $env)

=cut
sub exec_openssl {
	my $output=shift;
	my $command = shift;
	my $env = shift || '';
	$env.=" MSUPN=noname\@example.com " unless ($env=~m/MSUPN\=/);
	$env.=" DNS=example.com " unless ($env=~m/DNS\=/);
	# Below is just a work-around to defeat an incorrect conf
	$TMPDIR='/tmp' if (! -d $TMPDIR);
	my $OSSLCNF="OPENSSL_CONF=\"$TMPDIR/lpkica.cnf\"";
	# Below is just a work-around to defeat an incorrect conf
        $OSSLCNF='' unless (-f "$TMPDIR/lpkica.cnf");
        # printing debug info if needed, and then launching the command, redirecting STDERR to a temp file
	print "$OSSLCNF $env $OPENSSL $command\n" if ($DEBUG);
	my $ret=`$OSSLCNF $env $OPENSSL $command 2>"$TMPDIR/ossl.err"`;
	my $rcode=$?;
	if ($rcode>0) {
		$command="" unless ($DEBUG);
		# in case there's no output from OpenSSL on STDOUT, then retrieve STDERR
		$ret=`cat "$TMPDIR/ossl.err"` if ($ret eq '');
		# finally call this magic command, that will also take care of the logging
		print_show_err($output,"ERROR: failure while executing openssl $command \nReturn code is $rcode, output is: \n$ret");
		return undef;
	}
	print $ret if ($DEBUG);
	# below is to return something in case execution went fine, but we have nothing to return
	$ret=1 if ((!$ret)||($ret eq ''));
	return $ret;
}

=head2 exec_pipe_openssl ($input, $output, $command, $env)

=cut
sub exec_pipe_openssl {
	my $input=shift;
	my $output=shift;
	my $command = shift;
	my $env = shift;
	$env='' unless ($env);
	$env.=" MSUPN=noname\@example.com " unless ($env=~m/MSUPN\=/);
	$env.=" DNS=example.com " unless ($env=~m/DNS\=/);
	my $OSSLCNF="OPENSSL_CONF=\"$TMPDIR/lpkica.cnf\"";
        $OSSLCNF='' unless (-f "$TMPDIR/lpkica.cnf");
	print "$OSSLCNF $env $OPENSSL $command\n" if ($DEBUG);
	my $ret=`echo \"$input\" | $OSSLCNF $env $OPENSSL $command `;	
	my $rcode=$?;
	if ($rcode>0) {
		$command="" unless ($DEBUG);
		print_show_err($output,"ERROR: failure while executing openssl $command\nReturn code is $rcode, output is:\n$ret");
		return undef;
	}
	print $ret if ($DEBUG);
	# below is to return something in case execution went fine, but we have nothing to return
	$ret=1 if ((!$ret)||($ret eq ''));
	return $ret;
}

=head2 compute_response_from_challenge ($challenge)

=cut

sub compute_response_from_challenge {
	my $challenge=shift;

	$challenge=pack "H*",$challenge;
	my $ret=exec_pipe_openssl($challenge,1,"des-ede3 -e -K $SCSECRET");
	$ret = substr(uc(unpack "H*", $ret),0,16);

	return $ret;
}

1;
