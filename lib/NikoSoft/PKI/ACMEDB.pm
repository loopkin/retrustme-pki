##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: ACMEDB.pm 428 2018-03-21 08:00:33Z loopkin $
##

=head1 NAME

NikoSoft::PKI::ACMEDB

=cut

package NikoSoft::PKI::ACMEDB;

BEGIN {
	use Cwd qw(abs_path);
	our $path=abs_path($0);
}
use lib ("$path/etc","$path/lib");

use strict;
use DBI;
use NSConfig qw($CAPATH);
use utf8;

our $DBFILE="acmedb.sqlite";

=head2 new

Constructor for the NikoSoft::PKI::ACME class.

=cut

sub new {
  my $class=shift;
  my ($config,$log)=@_;
  my $self= {};
  $self->{log}=$log;
  $self->{config}=$config;
  mkdir("$CAPATH") if (!(-f $CAPATH));
  if (!(-f "$CAPATH/$DBFILE")) {
		my $dbh = DBI->connect("dbi:SQLite:dbname=$CAPATH/$DBFILE","","") or die $DBI::errstr;
		$dbh->do("PRAGMA encoding = 'UTF-8'");
		my $sth=$dbh->prepare("CREATE TABLE acme (pubkey TEXT PRIMARY KEY, email TEXT NOT NULL, domain TEXT, token TEXT, cert_serial INTEGER, status TEXT NOT NULL)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_status ON acme(status)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_pubkey ON acme(pubkey)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_domain ON acme(domain)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX cert_email ON acme(email)");
		$sth->execute();
		$dbh->disconnect();
  }
  $self->{dbh} = DBI->connect("dbi:SQLite:dbname=$CAPATH/$DBFILE","","", { AutoCommit=>1, RaiseError=>0 } ) or die $DBI::errstr;
  $self->{dbh}->{sqlite_unicode} = 1;
  bless $self, $class;
  return $self;
}

sub DESTROY {
	my $self = shift;
	my $dbh = $self->{dbh};
	$dbh->disconnect();
}

=head2 insert_account ($pubkey, $email)

Inserts a new account in the ACME database

=cut

sub insert_account {
  my $self = shift;
  my ($pubkey,$email)=@_;
  my $dbh=$self->{dbh};
  my $sth=$dbh->prepare("INSERT INTO acme(pubkey,email,status) VALUES(?,?,?)");
  $sth->execute($pubkey,$email,'account_created');
}

=head2 update_account_with_authz ($domain,$token,$status,$certserial,$pubkey)

Updates an entry in the ACME DB, identified by its pubkey.

=cut

sub update_account_with_authz {
  my $self=shift;
  my ($domain,$token,$status,$certserial,$pubkey)=@_;
  my $dbh=$self->{dbh};
  my $sth=$dbh->prepare("UPDATE acme SET domain='$domain', token='$token', status='$status', cert_serial='$certserial' WHERE pubkey='$pubkey'");
  $sth->execute();
}

=head2 get_account_authz ($pubkey)

Retrieves an entry from the ACME DB, identified by its pubkey.
Returns ($email,$domain,$token,$status)

=cut

sub get_account_authz {
  my $self=shift;
  my $pubkey=shift;
  my $query="SELECT email,domain,token,status FROM acme WHERE pubkey='$pubkey'";
  my $dbh = $self->{dbh};
  my @rv=$dbh->selectrow_array($query);
  my $email=$rv[0];
  my $domain=$rv[1];
  my $token=$rv[2];
  my $status=$rv[3];
  return ($email,$domain,$token,$status);
}
