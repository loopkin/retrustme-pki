##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: Log.pm 365 2015-09-29 18:59:44Z loopkin $
##

=head1 NAME

NikoSoft::PKI::Log

=cut

package NikoSoft::PKI::Log;

=head1 DESCRIPTION

Module used to log actions within NikoSoft PKI. For now, logs to syslog.

=cut

BEGIN {
	use Cwd qw(abs_path);
	our $path=abs_path($0);
}
use lib ("$path/etc","$path/lib");

use strict;
use Sys::Syslog;
use Time::Piece;
use NSConfig;
use NikoSoft::PKI::AccessControl;
use Crypt::OpenSSL::RSA;
use MIME::Base64;

our  $DBFILE="logdb.sqlite";

=head1 METHODS

=head2 new

Constructor

=cut
sub new {
	my $class = shift;
	my $self= {};
	$self->{logfacility}=$SYSLOGFACILITY;
	$self->{accesscontrol} = NikoSoft::PKI::AccessControl->new();
	my $spkey=`cat $CAPATH/private/sign.key`;
	$self->{sign_priv_key} = Crypt::OpenSSL::RSA->new_private_key($spkey);
	$self->{sign_priv_key}->use_sha256_hash();
	if ($TYPE eq 'enterprise') {
		my $pgrun = "$INSTALLDIR/var/dbrun";
		$self->{dbh} = DBI->connect("dbi:Pg:dbname=pkica;host=$pgrun","pki","", 
						{AutoCommit => 1, RaiseError=>0, PrintError=>0, Warn=>0}) or die $DBI::errstr;
		bless $self, $class;
		return $self;
	}
	mkdir("$CAPATH") if (!(-f $CAPATH));
	if (!(-f "$CAPATH/$DBFILE")) {
		my $dbh = DBI->connect("dbi:SQLite:dbname=$CAPATH/$DBFILE","","") or die $DBI::errstr;
		$dbh->do("PRAGMA encoding = 'UTF-8'");
		my $sth=$dbh->prepare("CREATE TABLE logs_serial_seq (value INTEGER)");
		$sth->execute();
		$sth=$dbh->prepare("INSERT INTO logs_serial_seq VALUES(0)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE TABLE logs (id INTEGER PRIMARY KEY, ts DATETIME, action TEXT NOT NULL, actor TEXT NOT NULL, entity TEXT NOT NULL, outcome TEXT NOT NULL, comment TEXT, signature TEXT)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX logs_action ON logs(action)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX logs_outcome ON logs(outcome)");
		$sth->execute();
		$sth=$dbh->prepare("CREATE INDEX logs_ts ON logs(ts)");
		$sth->execute();
		$dbh->disconnect();
	}
	$self->{dbh} = DBI->connect("dbi:SQLite:dbname=$CAPATH/$DBFILE","","", { AutoCommit=>1, RaiseError=>0 } ) or die $DBI::errstr;
	$self->{dbh}->{sqlite_unicode} = 1;
	bless $self, $class;
	return $self;
}

=head2 log_success ($action, $entity_id, $comment)

Logs an action as success.

=cut
sub log_success {
	my $self = shift;
	my ($action,$entity_id,$comment)=@_;
	my $actor=$self->{accesscontrol}->get_connected_user_fn() || 'nobody';
	$comment=~ s/\n/\#/g;
	my $message=_timestamp()."|$action|$actor|$entity_id|SUCCESS|$comment";
	syslog("info|$self->{logfacility}",$message);
	$self->_insert_log($self->_get_next_logid(),_timestamp(),$action,$actor,$entity_id,'SUCCESS',$comment);
}

=head2 log_error ($action, $entity_id, $comment)

Logs an action as error.

=cut
sub log_error {
	my $self = shift;
	my ($action,$entity_id,$comment)=@_;
	$self->_log_error_full($action,$entity_id,$comment,1);
}

sub _log_error_full {
	my $self=shift;
	my ($action,$entity_id,$comment,$db)=@_;
	my $actor=$self->{accesscontrol}->get_connected_user_fn() || 'nobody';
	$comment=~ s/\n/\#/g;
	my $message=_timestamp()."|$action|$actor|$entity_id|ERROR|$comment";
	syslog("err|$self->{logfacility}",$message);
	return unless ($db); 
	$self->_insert_log($self->_get_next_logid(),_timestamp(),$action,$actor,$entity_id,'ERROR',$comment);
}

=head2 read_logs ()

Returns the PKI logs as a string.

=cut
sub read_logs {
	my $self = shift;
	my $signed = shift;
	if (!($self->{accesscontrol}->is_connected_user_admin())) {
		print "Logs can be downloaded only by 'admin' user.";
		return;
	}
	my $dbh = $self->{dbh};
	my @result=@{$dbh->selectall_arrayref("SELECT id,ts,action,actor,entity,outcome,comment,signature FROM logs ORDER BY id DESC")};
	my $res='';
	foreach (@result) {
		$res.=join('|',@$_)."\n";
	}
	return $res;
}

=head2 get_action_list

Returns the list of actions in log entries currently existing in logs DB

=cut
sub get_action_list {
	my $self = shift;
	my $query="SELECT DISTINCT action FROM logs ORDER BY action";
	my $dbh = $self->{dbh};
	my @res = map( { $_->[0] } @{ $dbh->selectall_arrayref($query) } );
	return @res;		
}

=head2 get_logs_by_action_date

Retrieves the logs from parameters

=cut
sub get_logs_by_action_date {
	my $self = shift;
	my $params = shift;
	my $query = "SELECT id, ts, action, actor, entity, outcome, comment FROM logs WHERE 1=1";
	$query.=" AND action='".$params->{action}."'" if ($params->{action});
	$query.=" AND outcome='".$params->{outcome}."'" if ($params->{outcome});
	$query.=" ORDER BY id DESC LIMIT 20";
	my $dbh = $self->{dbh};
	my $res=$dbh->selectall_hashref($query,"id");
	return $res;			
}


sub _get_next_logid {
	my $self = shift;
	my $dbh = $self->{dbh};
	if ($TYPE eq 'enterprise') {
		my @rv=$dbh->selectrow_array("SELECT nextval('logs_serial_seq')");
		return $rv[0];
	}
	my $sth=$dbh->prepare("UPDATE logs_serial_seq set value=value+1");
	return $self->_error() unless ($sth);
	$sth->execute() or return $self->_error();
	my @rv=$dbh->selectrow_array("SELECT value FROM logs_serial_seq");
	return $rv[0];
}

sub _insert_log {
	my $self = shift;
	my ($id,$ts,$action,$actor,$entity,$outcome,$comment)=@_;
	my $signature=encode_base64($self->{sign_priv_key}->sign("$id|$ts|$action|$actor|$entity|$outcome|$comment"),'');
	my $dbh = $self->{dbh};
	my $sth=$dbh->prepare("INSERT INTO logs(id,ts,action,actor,entity,outcome,comment,signature) VALUES(?,?,?,?,?,?,?,?)");
	return $self->_error() unless ($sth);
	$sth->execute($id,$ts,$action,$actor,$entity,$outcome,$comment,$signature) or return $self->_error();
	return $self->_error() if ($sth->err);
	return 1;
}

# Internal method to log any error in case of error...
sub _error {
	my $self = shift;
	my $dbh = $self->{dbh};
	$self->_log_error_full("CADB_ERROR","CADB",$dbh->errstr,0);
	return undef;
}

sub _timestamp {
	my $t=gmtime;
	return $t->strftime("%F %T");
}

sub DESTROY {
	my $self = shift;
	my $dbh = $self->{dbh};
	$dbh->disconnect() if ($dbh);
}

1;
