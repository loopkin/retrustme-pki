##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: AccessControl.pm 371 2015-12-01 21:33:51Z loopkin $
##

=head1 NAME

NikoSoft::PKI::AccessControl

=cut

package NikoSoft::PKI::AccessControl;

BEGIN {
	use Cwd qw(abs_path);
	our $path=abs_path($0);
}
use lib ("$path/etc","$path/lib");

use strict;

=head2 new

Constructor for the NikoSoft::PKI::AccessControl class.

=cut
sub new {
	my $class = shift;
	my $self= {};
	bless $self, $class;
	return $self;
}

=head 2 is_connected_user_admin

Tells whether the connected user is an admin or not.

=cut
sub is_connected_user_admin {
	my $self = shift;
	my $user=_get_user();
	if (($user eq 'admin') || ($user eq 'root')) {
		return 1;
	}
	return 0;
}

=head 2 is_connected_user_operator

Tells whether the connected user is an operator or not.
NB: an admin is an operator.

=cut
sub is_connected_user_operator {
	my $self = shift;
	if (_get_user() ne '') {
		return 1;
	}
	return 0;
}

=head 2 get_connected_user

Returns the connected user. To be used to identify the user (access control, ...).

=cut
sub get_connected_user {
	my $self = shift;
	return _get_user();
}

=head 2 get_connected_user_fn

Returns the connected user's full name. To be used to get more information about the user (log/display).

=cut
sub get_connected_user_fn {
	my $self = shift;
	my $SSLCert=$ENV{SSL_CLIENT_S_DN} || '';
	$SSLCert=~s/emailAddress/E/g;
	return  $SSLCert || _get_user();
}

# convenience private method
sub _get_user {
	return $ENV{REMOTE_USER} || $ENV{USER};	
}

1;
