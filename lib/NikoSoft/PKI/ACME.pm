##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: ACME.pm 432 2018-03-22 22:13:02Z loopkin $
##

=head1 NAME

NikoSoft::PKI::ACME

=cut

package NikoSoft::PKI::ACME;

BEGIN {
	use Cwd qw(abs_path);
	our $path=abs_path($0);
}
use lib ("$path/etc","$path/lib");

use strict;
use DBI;
use NSConfig qw($CAPATH $PRODUCT $VERSION);
use utf8;
use JSON;
use JSON::WebToken;
use MIME::Base64 qw (decode_base64 decode_base64url encode_base64 encode_base64url);
use Digest::SHA qw(sha256_hex);
use NikoSoft::PKI::CryptoTools qw(get_random parse_pkcs10);
use NikoSoft::PKI::Utils qw(validate_data);
use LWP::UserAgent;
use NikoSoft::PKI::CA;
use NikoSoft::PKI::ACMEDB;

sub _parseJWT {
  my $message=shift;
  my $json=decode_json($message);
  my $protected=decode_json(decode_base64url($json->{protected}));
  my $pubkey=JSON::WebToken::get_pubkey_from_jwk(encode_json($protected->{jwk}));
  my $jwt=decode_jwt($message,$pubkey);
  return ($jwt,$pubkey,$protected);
}

=head2 new

Constructor for the NikoSoft::PKI::ACME class.

=cut

sub new {
  my $class=shift;
  my ($config,$log)=@_;
  my $self= {};
  $self->{log}=$log;
  $self->{config}=$config;
  my $acmedb=NikoSoft::PKI::ACMEDB->new($config,$log);
  $self->{db}=$acmedb;
  bless $self, $class;
  return $self;
}

=head2 create_account($message)

Creates an ACME account, using the JSON contents of the ACME message
Returns (sha256_hex($pubkey.encode_base64($email)),encode_json($json->{protected}->{jwk}),$email)

=cut

sub create_account {
  my $self=shift;
  my $message=shift;
  my ($jwt,$pubkey,$protected)=_parseJWT($message);
  return ('error:malformed','','') if ((!$jwt)||(!$pubkey)||(!$protected));
  return ('errpr:badNonce','','') if ($protected->{nonce} ne $self->get_nonce());
  my @emails=@{$jwt->{contact}};
  my $email=$emails[0];
  $email=~s/mailto://;
  $email=~s/\s//g;
  return ('error:invalidContact','','') unless ($email);
  return ('error:invalidContact','','') unless (validate_data($email,'email'));
  my $db=$self->{db};
  my $sth=$db->insert_account(sha256_hex($pubkey),$email);
  $self->{log}->log_success('ACME_CREATE_ACCOUNT','ACME_CONNECTOR',"Created account for $email with key hash ".sha256_hex($pubkey));
  return (sha256_hex($pubkey.encode_base64($email)),encode_json($protected->{jwk}),$email);
}

=head2 create_authz($message)

Creates an Authentication for a Domain, using the ACME protocol.
Returns (sha256_hex($pubkey),$token,$domain)

=cut

sub create_authz {
  my $self=shift;
  my $message=shift;
  my ($jwt,$pubkey,$protected)=_parseJWT($message);
  return ('error:malformed','','') if ((!$jwt)||(!$pubkey)||(!$protected));
  return ('error:malformed','','') unless ($jwt->{identifier});
  return ('errpr:badNonce','','') if ($protected->{nonce} ne $self->get_nonce());
  my $domain=$jwt->{identifier}->{value};
  return ('error:malformed','','') unless ($domain);
  my $db=$self->{db};
  my $token=get_random(16);
  my $sth=$db->update_account_with_authz($domain,$token,'pending','0',sha256_hex($pubkey));
  return (sha256_hex($pubkey),$token,$domain);
}

=head2 authz($uri,$message)

Authenticates a domain identified by its URI (aka. hex pubkey hash)
Returns (sha256_hex($pubkey),$token,$domain)

=cut

sub authz {
  my $self=shift;
  my $uri=shift;
  my $message=shift;
  unless ($message) {
    my $db = $self->{db};
    my @rv=$db->get_account_authz($uri);
    my $domain=$rv[1];
    my $token=$rv[2];
    my $status=$rv[3];
    return ('error:unauthorized','','') if ($status ne 'valid');
    return ($uri,$token,$domain);
  }
  my ($jwt,$pubkey,$protected)=_parseJWT($message);
  return ('error:malformed','','') if ((!$jwt)||(!$pubkey)||(!$protected));
  return ('errpr:badNonce','','') if ($protected->{nonce} ne $self->get_nonce());
  return ('error:unauthorized','','') if ($uri ne sha256_hex($pubkey));
  my $db = $self->{db};
  my @rv=$db->get_account_authz($uri);
  my $domain=$rv[1];
  my $token=$rv[2];
  my $ua=LWP::UserAgent->new();
  $ua->agent("$PRODUCT $VERSION");
  my $response=$ua->get("http://$domain/.well-known/acme-challenge/$token");
  return ('error:connection',$response->status_line,'') if (!$response->is_success);
  my $sth=$db->update_account_with_authz($domain,$token,'valid','0',$uri);
  $self->{log}->log_success('ACME_VALIDATE_DOMAIN','ACME_CONNECTOR',"Validated domain $domain with http");
  return (sha256_hex($pubkey),$token,$domain);
}

=head2 new_cert($message)

Operates a new cert request, using the CSR contained in the ACME message.
Returns ($crtserial,$crt)

=cut

sub new_cert {
  my $self=shift;
  my $message=shift;
  my ($jwt,$pubkey,$protected)=_parseJWT($message);
  return ('error:malformed','','') if ((!$jwt)||(!$pubkey)||(!$protected));
  return ('errpr:badNonce','') if ($protected->{nonce} ne $self->get_nonce());
  my $csr=$jwt->{csr};
  $csr=encode_base64(decode_base64url($csr));
  $csr="-----BEGIN CERTIFICATE REQUEST-----\n".$csr."-----END CERTIFICATE REQUEST-----";
  my ($alg, $size, $dn)=parse_pkcs10($csr);
  return ('error:badCSR') unless (validate_data($csr,'pkcs10'));
  $dn=~s/\/2.5.4.3\=(.+)$/$1/;
  return ('error:badCSR') unless ($dn);
  my $db = $self->{db};
  my @rv=$db->get_account_authz(sha256_hex($pubkey));
  my $email=$rv[0];
  my $domain=$rv[1];
  my $token=$rv[2];
  my $status=$rv[3];
  return ('error:unauthorized','') if ($domain ne $dn);
  return ('error:unauthorized','') if ($status ne 'valid');
  my ($crt,$crtserial)=();
  eval {
    my $ca=NikoSoft::PKI::CA->new($self->{config},$self->{log});
    $ca->set_output_mode(4);
    my %dn_params;
    $dn_params{cn}=$domain;
    $dn_params{email}=$email;
    ($crt,$crtserial)=$ca->sign_csr($csr,$self->{config}->data('acme_crt_type'),\%dn_params,0);
    my $sth=$db->update_account_with_authz($domain,$token,'valid',$crtserial,sha256_hex($pubkey));
  };
  return ('error:serverInternal','CA Error: See PKI Logs for details') if ($@);
  return ('error:serverInternal','CA Error: See PKI Logs for details') unless ($crtserial);
  $self->{log}->log_success('ACME_ISSUE_CERTIFICATE','ACME_CONNECTOR',"Issued certificate $crtserial for $domain");
  return ($crtserial,$crt);
}

=head2 get_cert($serial)

Retrieves a certificate from the CA DB based on its serial as dec.
Returns $crt

=cut

sub get_cert {
  my $self=shift;
  my $serial=shift;
  my $ca=NikoSoft::CA->new($self->{config},$self->{log});
  $ca->set_output_mode(4);
  my $cert=$ca->get_certificate_by_serial($serial);
  return 'error:certificateNotFound' unless ($cert);
  return $cert;
}

=head2 change_nonce

Changes the global nonce inside the PKI config

=cut
sub change_nonce {
	my $self = shift;
	my $nonce=sha256_hex(lc(get_random(8)));
	$self->{config}->set_data('acme_nonce',$nonce);
}

=head2 get_nonce

Retrieves the global nonce inside the PKI config

=cut
sub get_nonce {
  my $self=shift;
  return $self->{config}->data()->{acme_nonce} || 'ThisIsRetrustMePKIACMEConnector';
}

1;
