##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
##
## $Id: CA.pm 423 2018-01-17 23:06:06Z loopkin $
##

=head1 NAME

NikoSoft::PKI::CA

=cut

package NikoSoft::PKI::CA;

=head1 DESCRIPTION

CA perl module for the management of the Certification Authority.
Contains all business methods and OpenSSL calls.

PKCS#7, PKCS#8 and PKCS#10 formats are manipulated as PEM.
PKCS#12 format is generated as DER.

=cut

BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
}

use strict;
use NikoSoft::PKI::CADB;
use NikoSoft::PKI::Utils qw(utf8_print time_to_asn1_time mylock myunlock dumpfile print_show_err sendmail is_asn1_date_soon guess_c_o_from_email);
use NikoSoft::PKI::CryptoTools qw(openssl_cnf gen_friendly_name get_random exec_pipe_openssl exec_openssl);
use NikoSoft::PKI::Log;
use NikoSoft::PKI::Config;
use NSConfig;
use utf8;
use MIME::Base64;

=head1 METHODS

=head2 new ( $config )

Constructor of the NikoSoft::PKI::CA module.

	$config - A NikoSoft::PKI::Config instance

=cut
sub new {
	my $class=shift;
	my ($config,$log)=@_;
	my $self= {};
	$self->{log}=$log;
	$self->{db}=NikoSoft::PKI::CADB->new($self->{log});
	$self->{config}=$config;
	$self->{output}=2;
	$self->{returnnoerror}=0;
	bless $self, $class;
	return $self;
}

=head2 set_output_mode ( $output )

Sets output verbosity for the next actions.

	$output - Sets the method of output, see NikoSoft::PKI::Utils::print_show_err for more information

=cut
sub set_output_mode {
	my $self = shift;
	my $output = shift;
	$self->{output}=$output;
	if ($output==4) {
		$self->{output}=1;
		$self->{returnnoerror}=1;
	}
}


=head2 gen_serial

Generates a serial number using a sequence from NikoSoft::PKI::CADB and random bits. Serial number is returned as hexadecimal integer.

=cut
sub gen_serial {
	my $self = shift;
	my $cadb=$self->{db};
	my $serial=$cadb->get_next_serial();
	$serial='0'.$serial if (length($serial) % 2);
	$serial.=get_random(4);
	return $serial;
}

=head2 initiate_ops ($custom)

Iniates CA cryptographic operations. MUST be called before any combination of CSR/CRT/KEY operations. Parameters are optional.

WARNING: if parameters are given, this function MUST be called only under lock protection along with certificate signing operation

	$custom - use the custom profile, not standard one

=cut
sub initiate_ops {
	my $self = shift;
	my $custom = shift;
	if ($custom) {
		openssl_cnf($self->{config},1);
		system("cat $INSTALLDIR/etc/custom_profile.cnf >> $TMPDIR/lpkica.cnf");
	} else {
		openssl_cnf($self->{config});
	}
}

=head2 gen_ca ( $c,$o,$cn,$caemail )

Generates a CA key and certificate, and initializes PKI settings. To be used during Key Ceremony / Bootstrap. Also generates the CRL.

	$cn - a string representing the Common Name of the CA to generate.
	$o - a string representing the Organization of the CA to generate.
	$c - a string representing the Country of the CA to generate.

=cut
sub gen_ca {
	my $self = shift;
	my ($c,$o,$cn,$caemail)=@_;
	return unless ($cn);
	my $output=$self->{output};
	mkdir("$CAPATH");
	mkdir("$CAPUBLICPATH");
	mkdir("$CAPATH/private");
	mkdir("$CAPATH/newcerts");
	system("touch $CAPATH/index.txt");
	system("touch $CAPATH/index.txt.attr");
	system("echo '02' > $CAPATH/serial");
	system("echo '01' > $CAPATH/crlnumber");
	my $caserial=$self->gen_serial();
	print_show_err($output,`$cryptosystems{$CRYPTO}{genkey_ca} 2>&1`) if ($cryptosystems{$CRYPTO}{genkey_ca});
	exec_openssl($output,"req -new $cryptosystems{$CRYPTO}{req_genkey_ca} -utf8 -set_serial 0x$caserial -x509 -subj \"/C=$c/O=$o/CN=$cn\" -out \"$CAPATH/ca.crt\" -extensions ca_cert 2>&1") or return undef;
	exec_openssl($output,"req $cryptosystems{$CRYPTO}{gencsr_ca} -new -utf8 -subj  \"/C=$c/O=$o/CN=$cn\" -out \"$CAPATH/ca.csr\" 2>&1") or return undef;
	exec_openssl($output,"x509 -in \"$CAPATH/ca.crt\" -out \"$CAPATH/ca.der\" -outform DER");
	print_show_err($output,`/bin/cp -f "$CAPATH/ca.crt" "$CAPUBLICPATH/ca.crt" 2>&1`);
	print_show_err($output,`/bin/cp -f "$CAPATH/ca.csr" "$CAPUBLICPATH/ca.csr" 2>&1`);
	$self->{log}->log_success("CA_CREATION","/C=$c/O=$o/CN=$cn","CA Serial Number: $caserial");
	$self->{config}->set_data('organization',$o);
	$self->{config}->set_data('country',$c);
	$self->{config}->set_data('p12mailsubject',"Your PKCS#12 file");
	$self->{config}->set_data('p12mailtext',"Here is PKCS#12 file for [CN]");
	$self->{config}->set_data('renewmailsubject','Expiration notice');
	$self->{config}->set_data('renewmailtext','The certificate [DN] is about to expire');
	$self->{config}->set_data('adminmail',$caemail);
	$self->{config}->set_data('mscapicsp','Microsoft Base Smart Card Crypto Provider');
	$self->{config}->set_data('prof_auth_cert',1);
	$self->{config}->set_data('prof_serv_cert',1);
	$self->{config}->set_data('scep_crt_type','serv_cert');
	$self->{config}->set_data('acme_crt_type','serv_cert');
	my %profiles=$self->cert_profiles_list(2);
	my @profileslist=sort(keys(%profiles));
	foreach my $profid (@profileslist) {
		$self->{config}->set_data('prof_'.$profid.'_crtlifetime','730');
		$self->{config}->set_data('prof_'.$profid.'_maxnbcert','1');
		$self->{config}->set_data('prof_'.$profid.'_ou',$profiles{$profid});
	}
	$self->{config}->set_data('crlonrevoke',1);
	$self->{config}->set_data('dailycron',1);
	$self->{config}->set_data('p12mail',1);
	$self->{config}->set_data('p12link',1);
	$self->{config}->set_data('mail_in_dn',1);
	$self->{config}->set_data('crldp',"http://$CAHOSTNAME$CAPUBLICURI/ca.crl");
	$self->{config}->set_data('aiaca',"http://$CAHOSTNAME$CAPUBLICURI/ca.crt");
	$self->{config}->set_data('cpoid','2.5.29.32.0');
	$self->{config}->save($output,0,1);
	$self->generate_crl();
	chmod(0440, "$CAPATH/private/ca.key");
	return 1;
}

=head2 gen_key_and_csr ( $crt_type, $cn, $email )

Generates a private key and associated CSR. Returns an Array($key, $csr)

	$crt_type - certificate profile to be used (needed because key generation parameters can change)
	$cn - Common Name associated to the request (ie. CN of the holder of the key)
	$email - Email associated to the request (ie. email of the holder of the key)

	$key - the generated key in PKCS#8 format
	$csr - the generated CSR in PKCS#10 format

=cut
sub gen_key_and_csr {
	my $self = shift;
	my $output=$self->{output};
	my ($crt_type,$cn,$email)=@_;
	my $req_params=$cryptosystems{$CRYPTO}{req_genkey_client};
	my $tmp=gen_friendly_name($cn);
	$req_params=$cryptosystems{$CRYPTO}{req_genkey_server} if ($crt_type eq 'serv_cert');
	exec_openssl($output,"req -new $req_params -utf8 -keyout \"$TMPDIR/$tmp.key\" -out \"$TMPDIR/$tmp.csr\" -subj \"/CN=$cn/emailAddress=$email\" -nodes 2>&1") or return (undef,undef);
	$self->{log}->log_success("KEY_GENERATION","/CN=$cn/emailAddress=$email","Key generation parameters: $req_params");
	my $key=dumpfile("$TMPDIR/$tmp.key");
	my $csr=dumpfile("$TMPDIR/$tmp.csr");
	unlink("$TMPDIR/$tmp.key");
	unlink("$TMPDIR/$tmp.csr");
	return ($key,$csr);
}

=head2 sign_csr ( $csr, $crt_type, $dn_params, $bypass_profile_check,$force_lifetime )

Signs a CSR using the Certification Authority private key. Returns an array with the certificate as PKCS#7 and its serial number.

	$csr - the CSR to be signed, in PKCS#10 format
	$crt_type - certificate profile to be used
	$dn_params - pointer to a hash containing DN parameters: cn (mandatory), email (mandatory), device_serial_number (optional) and uid (optional: for operators only)
	$bypass_profile_check - Bypass or not the profile check (but not duplicate DN) - optional (default to not bypass)
	$force_lifetime - Certificate lifetime, in days - optional (default to certificate profile settings)

=cut
sub sign_csr {
	my $self = shift;
	my $output=$self->{output};
	my $cadb=$self->{db};
	my ($csr,$crt_type,$dn_params,$bypass_profile_check,$force_lifetime)=@_;

	my $cn=$dn_params->{cn};
	my $email=$dn_params->{email};
	my $uid=$dn_params->{uid};
	my $device_serial_number=$dn_params->{device_serial_number};
	return (undef,undef) unless (($csr)&&($crt_type)&&($cn)&&($email));

	my $org=$self->{config}->data()->{organization};
	my $country=$self->{config}->data()->{country};
	my $ou=$self->{config}->data('prof_'.$crt_type.'_ou');
	$ou="/OU=$ou" if ($ou);
	$ou='' unless ($ou);
	$device_serial_number="/serialNumber=$device_serial_number" if ($device_serial_number);
	$device_serial_number='' unless ($device_serial_number);
	$uid="/UID=$uid" if ($uid);
	$uid='' unless ($uid);
	my $maxnbcert=$self->{config}->data('prof_'.$crt_type.'_maxnbcert');
	# force_lifetimeis optional... else we get info from profile.
	my $lifetime=$force_lifetime || $self->{config}->data('prof_'.$crt_type.'_crtlifetime');
	# specific stuff for operators certificates
	if ($crt_type eq 'op_cert') {
		$bypass_profile_check=1;
		$maxnbcert=1;
		$lifetime=730;
	}
	my $certserial=$self->gen_serial();
	my $rootdn="/C=$country/O=$org";
	$rootdn="/C=$country" if ($org eq 'null');
	if ($self->{config}->data('guess_dn_from_mail')) {
		my ($gc,$go,$grdn)=guess_c_o_from_email($email);
		$rootdn=$grdn if ($grdn);
	}
	my $dn="$rootdn$ou$device_serial_number$uid/CN=$cn/emailAddress=$email";
	# For SmartCardLogon, use x500UniqueIdentifier instead of emailAddress in the DN.
	$dn="/C=$country/O=$org$ou$device_serial_number/CN=$cn/x500UniqueIdentifier=$email" if ($crt_type eq 'scl_cert');
	my $actualdn=$dn;
	my $mail_in_dn=' -noemailDN';
	if ($self->{config}->data('mail_in_dn')) {
		$mail_in_dn='' ;
	} else {
		$actualdn="$rootdn$ou$device_serial_number$uid/CN=$cn" if ($crt_type ne 'scl_cert');
	}

	my %labels=$self->cert_profiles_list();
	my @labelslist=sort(keys(%labels));
	if ((!grep(/$crt_type/,@labelslist)) && (!$bypass_profile_check)) {
		$self->{log}->log_error("CERT_CREATION",$dn,"Incorrect certificate profile identifier $crt_type");
		return (undef,undef) if ($self->{returnnoerror}==1);
		print_show_err($output,"error: "._T('ERR_INCORRECT_PROFILE')." $crt_type");
		return (undef,undef);
	}
	# Fetching from database informations about certificates with same DN.
	my @existingcert=$cadb->get_certificate_by_dn($actualdn,1,1);
	# Based on retrieved information, guessing what to do, depending on the situation (taking into account the renewal period)
	$maxnbcert++ if ( (scalar(@existingcert)>0) && (is_asn1_date_soon($existingcert[0],1)) );
	if ( (scalar(@existingcert)>0) && ($existingcert[2]>=$maxnbcert) ) {
		$self->{log}->log_error("CERT_CREATION",$dn,"Maximum number of certificate reached for this DN");
		return (undef,undef) if ($self->{returnnoerror}==1);
		print_show_err($output,"error: "._T('ERR_MAX_NB_CERT'));
		return (undef,undef);
	}

	# For the DC cert, we need a special CA section, in order to be able to copy the GUID from the CSR
	my $section='CA_default';
	$section='CA_dc' if ($crt_type eq 'dc_cert');
	mylock();
	$self->initiate_ops(1) if ($crt_type eq 'custom_cert');
	# now we dump the data and sign
	print_show_err($output,`echo "$csr" > "$TMPDIR/$certserial.csr"`);
	print_show_err($output,`echo "$certserial" > "$CAPATH/serial"`);
	my $enddate=time_to_asn1_time(gmtime(time+($lifetime*24*60*60)));
	$enddate=time_to_asn1_time(gmtime(time+(60*60))) if ($lifetime == 0);
	exec_openssl($output,"ca $cryptosystems{$CRYPTO}{ca_sign} -utf8 -in \"$TMPDIR/$certserial.csr\" -cert \"$CAPATH/ca.crt\" -enddate $enddate -extensions $crt_type -subj \"$dn\" -name $section -notext -batch$mail_in_dn -out \"$TMPDIR/$certserial.crt\" 2>&1","MSUPN=\"$email\" DNS=\"$cn\"") or return (undef,undef);
	$self->{log}->log_success("CERT_CREATION",$actualdn,"Certificate Serial Number: $certserial - Certificate Lifetime: $lifetime");
	my $cert=dumpfile("$TMPDIR/$certserial.crt");
	unlink("$TMPDIR/$certserial.crt");
	unlink("$TMPDIR/$certserial.csr");
	unlink("$CAPATH/index.txt");
	print_show_err($output,`touch $CAPATH/index.txt 2>&1`);
	myunlock();

	if (!$cadb->insert_certificate($certserial,$actualdn,$email,$enddate,$crt_type,$cert)) {
		# Rollbacking in case of error
		$self->revoke_cert($certserial);
		return (undef,undef);
	}
	return ($cert,$certserial);
}

=head2 gen_p12_as_file ( $key, $crt, $cn, $pwd )

Generates a PKCS#12 file, containing the CA certificate on top of the following elements:

	$key - the key to be included into the PKCS#12
	$crt - the certificate to be included into the PKCS#12
	$cn - the Common Name of the $crt's holder, used to generate the PKCS#12 "friendly name"
	$pwd - the password used to encrypt the PKCS#12

=cut
sub gen_p12_as_file {
	my $self = shift;
	my $output=$self->{output};
	my ($key,$crt,$cn,$pwd)=@_;
	my $tmp=gen_friendly_name($cn);
	print_show_err($output,`echo "$key" > "$TMPDIR/$tmp.key"`);
	print_show_err($output,`echo "$crt" > "$TMPDIR/$tmp.crt"`);
	exec_openssl($output,"pkcs12 -export $cryptosystems{$CRYPTO}{create_pkcs12} -name \"$tmp\" -inkey \"$TMPDIR/$tmp.key\" -in \"$TMPDIR/$tmp.crt\" -certfile \"$CAPATH/ca.crt\" -out \"$TMPDIR/$tmp.p12\" -passout pass:$pwd 2>&1")  or return (undef, undef);
	print_show_err($output,`mv "$TMPDIR/$tmp.p12" "$CAPUBLICPATH/$tmp.p12" 2>&1`);
	unlink("$TMPDIR/$tmp.key");
	unlink("$TMPDIR/$tmp.crt");
	return ("$tmp.p12",$tmp);
}

=head2 get_p12_as_base64 ($filename)

Returns the PKCS12 as a Base64 blob.

	$filename- name of PKCS12 file on the filesystem

=cut
sub get_p12_as_base64 {
	my $self = shift;
	my $filename = shift;
	my $output=$self->{output};
	# TODO: move to perl-based Base64 implementation
	my $p12base64=encode_base64(dumpfile($filename),'');
	return $p12base64;
}


=head2 revoke_cert ( $serial )

Revokes the given certificate. Juste flags it as revoked, and doesn't issue the CRL.

	$serial - the serial number of the certificate to be revoked.

=cut
sub revoke_cert {
	my $self = shift;
	my $output=$self->{output};
	my $cadb=$self->{db};
	my ($serial)=@_;
	my $revdate=time_to_asn1_time(gmtime(time));
	if ($cadb->revoke_certificate($serial,$revdate)) {
		$self->{log}->log_success("CERT_REVOCATION","$serial","Certificate Serial Number: $serial");
		return 1;
	}
	return undef;
}

=head2 generate_crl

Generates the CRL, using the CADB information and the CA key.

=cut
sub generate_crl {
	my $self = shift;
	my $output=$self->{output};
	my $cadb=$self->{db};
	my $rev_serials=$cadb->list_revoked_cert();
	mylock();
	open INDEX, ">$CAPATH/index.txt" or do { print_show_err($output, "error: cannot write to index.txt"); return undef; };
	my $count=0;
	foreach my $rev_serial (keys(%{$rev_serials})) {
		my $revdate=$rev_serials->{$rev_serial}->{revocation};
		my $expdate=$rev_serials->{$rev_serial}->{expiration};
		print INDEX "R\t$expdate\t$revdate,unspecified\t$rev_serial\tunkown\t/SN=$rev_serial\n";
		$count++;
	}
	close INDEX;
	exec_openssl($output,"ca $cryptosystems{$CRYPTO}{ca_revoke} -gencrl -config $TMPDIR/lpkica.cnf -out \"$CAPATH/ca.crl\" 2>&1") or return undef;
	$self->{log}->log_success("CRL_CREATION","ca.crl","Number of certificates revoked in CRL: $count");
        print_show_err($output,`cp "$CAPATH/ca.crl" "$CAPUBLICPATH/" 2>&1`);
        myunlock();
        return 1;
}

=head2 list_certificates ( $status, $dn, $profile )

Returns the list of certificates matching the request, as an HASHREF whose key is the certificate serial number. See NikoSoft::PKI::CADB::list_certificates for more information.

	$status - status of certificates to list (V, R, or any)
	$dn - dn, or part of the dn of the searched certificates
	$profile - certificate profile used to issue the certificate (optional)

=cut
sub list_certificates {
	my $self = shift;
	my ($status,$dn,$profile)=@_;
	my $cadb=$self->{db};
	return $cadb->list_certificates($status,$dn,$profile);
}

=head2 retrieve_certificates ( $status,$email, $profile )

Returns the list of certificates matching the request, as a array of PEM. See NikoSoft::PKI::CADB::retrieve_certificates for more information.

	$status - status of certificates to list (V, R, or any)
	$email - email of the searched certificates
	$profile - certificate profile used to issue the certificate (optional)

=cut
sub retrieve_certificates {
	my $self = shift;
	my ($status,$email,$profile)=@_;
	my $cadb=$self->{db};
	return $cadb->retrieve_certificates($status,$email,$profile);
}

=head2 list_op_certificates ( $status )

Returns the list of operators certificates matching the request, as an HASHREF whose key is the certificate serial number. See NikoSoft::PKI::CADB::list_op_certificates for more information.

	$status - status of certificates to list (V, R, or any)

=cut
sub list_op_certificates {
	my $self = shift;
	my ($status,$dn,$profile)=@_;
	my $cadb=$self->{db};
	return $cadb->list_op_certificates($status);
}

=head2 get_certificate_by_serial ( $serial )

Fetches the certificate using its serial number, and returns it as PKCS#7 plus text showing its content.

	$serial - the serial number of the certificate to fetch
	$details - display certificate details. This uses OpenSSL.

=cut
sub get_certificate_by_serial {
	my $self = shift;
	my ($serial,$details)=@_;
	my $cadb=$self->{db};
	my $output=$self->{output};
	my $cert=$cadb->get_certificate_by_serial($serial);
	return undef unless ($cert);
	return $cert if (!$details);
	# This is the only exception not using exec_openssl, because here we need to pipe in the data.
	my $txt=exec_pipe_openssl($cert,$output,'x509 -text -certopt no_pubkey,no_sigdump 2>/dev/null');
	$txt=$cert if (!$txt);
	return $txt;
}

=head2 expiration_notice_and_flag

Sends e-mail expiration notice to PKI admin (cronjob), and flags certificates as expired in the CA DB.

=cut
sub expiration_notice_and_flag {
	my $self=shift;
	my $cadb=$self->{db};
        my $serials=$self->list_certificates('any');
        my $autopurge=$self->{config}->data('autopurge');
	foreach my $serial (keys(%{$serials})) {
		my $rev=$serials->{$serial}->{revocation};
		my $exp=$serials->{$serial}->{expiration};
		my $status=$serials->{$serial}->{status};
		my $subj=$serials->{$serial}->{dn};
		my $email=$serials->{$serial}->{email};
                if (is_asn1_date_soon($exp,2)) {
			my $mailsubj=$self->{config}->data('renewmailsubject');
			$mailsubj=~s/\[DN\]/$subj/g;
			my $mailbody=$self->{config}->data('renewmailtext');
			$mailbody=~s/\[DN\]/$subj/g;
			sendmail($self->{config}->data('adminmail'),$mailsubj,$mailbody,$self->{config}) if ($self->{config}->data('renewadmin'));
			# Warning: for the SCL profile, email can be empty.
			sendmail($email,$mailsubj,$mailbody,$self->{config}) if (($self->{config}->data('renewholder'))&&($email));
                }
                if (is_asn1_date_soon($exp,3)) {
                	if ($autopurge) {
				if ($cadb->remove_certificate($serial)) {
					$self->{log}->log_success("REMOVE_CERT_FROM_CADB",$subj,
											"Certificate #$serial was deleted from CA database");
				}
                	} elsif ($status ne 'E') {
				if ($cadb->expire_certificate($serial)) {
					$self->{log}->log_success("EXPIRE_CERT_FROM_CADB",$subj,
											"Certificate #$serial was flagged as expired in CA database");
				}
                 	}
                }
        }
}

=head2 cert_profiles_list ( $list_type )

Returns the list of active certificate profiles, as a hash.

	$list_type - the type of list: 0 - active profiles, 1 - all profiles, 2 - all profiles with default OU names

=cut
sub cert_profiles_list {
	my $self = shift;
	my $returnall = shift;
	my %labels=();
	if (($returnall) && ($returnall==2)) {
		$labels{'serv_cert'}='Server';
		$labels{'auth_cert'}='Authentication';
		$labels{'sign_cert'}='Signature';
		$labels{'tsp_cert'}='TimeStamping';
		$labels{'scl_cert'}='MsSmartCardLogon';
		$labels{'dc_cert'}='MsDomainController';
		$labels{'enc_cert'}='Encryption';
		$labels{'ocsp_cert'}='OCSP';
		$labels{'custom_cert'}='CustomProfile';
		return %labels;
	}
	$labels{'serv_cert'}=_T('SERVER_PROFILE') if (($self->{config}->data()->{'prof_serv_cert'}) || ($returnall));
	$labels{'auth_cert'}=_T('CLIENT_SIGN_PROFILE') if (($self->{config}->data()->{'prof_auth_cert'}) || ($returnall));
	$labels{'sign_cert'}=_T('SIGN_PROFILE') if (($self->{config}->data()->{'prof_sign_cert'}) || ($returnall));
	$labels{'tsp_cert'}=_T('TIMESTAMP_PROFILE') if (($self->{config}->data()->{'prof_tsp_cert'}) || ($returnall));
	$labels{'scl_cert'}='Microsoft SmartCard Logon' if (($self->{config}->data()->{'prof_scl_cert'}) || ($returnall));
	$labels{'dc_cert'}='Microsoft Domain Controller' if (($self->{config}->data()->{'prof_dc_cert'}) || ($returnall));
	$labels{'enc_cert'}=_T('ENCRYPTION_PROFILE') if (($self->{config}->data()->{'prof_enc_cert'}) || ($returnall));
	$labels{'ocsp_cert'}='OCSP' if (($self->{config}->data()->{'prof_ocsp_cert'}) || ($returnall));
	if (($self->{config}->data()->{'prof_custom_cert'}) || ($returnall)) {
		$labels{'custom_cert'}=$self->{config}->data()->{'prof_custom_cert_ou'} || 'Custom Profile' ;
	}
	#%labels=('ca_cert'=>_T('CA_PROFILE')) if ($self->{config}->data()->{rootca} == 1);
	return %labels;
}


1;
