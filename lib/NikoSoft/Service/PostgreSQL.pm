##
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under proprietary terms:
## - Software is provided "AS IS", without any warranty whatsoever
## - It is explicitely forbidden to use, distribute, copy, reproduce, redistribute or give without 
## explicit written consent of the author
## - Such consent could lead to agree to the terms of a separate license agreement.
##
## $Id: PostgreSQL.pm 394 2017-04-12 08:27:45Z loopkin $
##

=head1 NAME

NikoSoft::Service::PostgreSQL

=cut

package NikoSoft::Service::PostgreSQL;

=head1 DESCRIPTION

Module used to manage PostgreSQL service (sysinit).

=cut

BEGIN {
	push @INC, $main::NSPATH."/lib";
	push @INC, $main::NSPATH."/etc";
	eval  "use NikoSoft::I18N::".$main::LANG.";";
}

use strict;
use NSConfig;
use NikoSoft::PKI::Utils qw(dumpfile);
use DBI;

=head1 METHODS

=head2 new

Constructor

=cut
sub new {
	my $class = shift;
	my $verbose = shift;
	my $self  = {};
	$self->{verbose} = $verbose;
	$self->{pgdata} = "$INSTALLDIR/var/db";
	$self->{pgrun} = "$INSTALLDIR/var/dbrun";
	$self->{pgport} = "5432";
	bless $self, $class;
	return $self;
}

# Internal method for initializing database environment
sub _initdb {
	my $self = shift;
	my $pgdata=$self->{pgdata};
	my $pgrun=$self->{pgrun};
	system("mkdir -p \"$pgdata\"");
	system("chown postgres:postgres \"$pgdata\"");
	system("chmod go-rwx \"$pgdata\"");
	system("mkdir -p \"$pgrun\"");
	system("chown postgres:$APACHEUSER \"$pgrun\"");
	system("chmod 750 \"$pgrun\"");
	system("/bin/su -l postgres -c \"/usr/bin/initdb --pgdata='$pgdata' --auth='ident'\" >>/var/log/nspki_pgsql.log 2>&1 < /dev/null");
	system("mkdir -p \"$pgdata/pg_log\"");
	system("chown postgres:postgres \"$pgdata/pg_log\"");
	system("chmod go-rwx \"$pgdata/pg_log\"");
	open PGHBA, ">$pgdata/pg_hba.conf";
	print PGHBA <<__EOF__;
# Locally, use Unix filesystem access control ('trust') and domain socket only
local   all         all                                             trust
__EOF__
	close PGHBA;
	return 1 if (-f "$pgdata/PG_VERSION");
	print "\nUnkown error initializing DB\n";
	return 0;
}

=head2 start

Starts the DB engine

=cut
sub start {
	my $self = shift;
	my $pgdata=$self->{pgdata};
	my $pgrun=$self->{pgrun};
	my $pgport=$self->{pgport};
	my $firsttime=0;
	$firsttime=1 unless (-f "$pgdata/PG_VERSION");
	$self->_initdb() if ($firsttime);
	my $version=dumpfile("$pgdata/PG_VERSION");
	unless ($version=~m/^[8-9]/) {
		print "\nIncorrect DB version.\n";
		return 0;
	}
	system("/bin/su -l postgres -c \"/usr/bin/postgres -h '' -p '$pgport' -D '$pgdata' -k '$pgrun' &\" >>/var/log/nspki_pgsql.log 2>&1 < /dev/null");
	$self->_createdb if ($firsttime);
	return 1;
}

=head2 stop

Stops the DB engine

=cut
sub stop {
	my $self = shift;
	my $pgdata=$self->{pgdata};
	my $pgport=$self->{pgport};
	system("/bin/su -l postgres -c \"/usr/bin/pg_ctl stop -D '$pgdata' -s -m fast\" > /dev/null 2>&1 < /dev/null");
	return 1;
}

=head2 status

Gives status of the DB engine

=cut
sub status {
	my $self = shift;
	my $pgdata=$self->{pgdata};
	return 1 if (-f "$pgdata/postmaster.pid");
	return 0;
}

# Internal method for creating database
sub _createdb {
	my $self = shift;
	my $pgdata=$self->{pgdata};
	my $pgrun=$self->{pgrun};
	my $pgport=$self->{pgport};
	sleep(2);
	system("/bin/su -l postgres -c \"/usr/bin/psql -h '$pgrun' -c \\\"CREATE USER pki WITH PASSWORD 'pkicarw'\\\"\" >>/var/log/nspki_pgsql.log 2>&1 < /dev/null");
	system("/bin/su -l postgres -c \"/usr/bin/createdb -h '$pgrun' -O pki -E UTF8 pkica\" >>/var/log/nspki_pgsql.log 2>&1 < /dev/null");
	my $dbh = DBI->connect("dbi:Pg:dbname=pkica;host=$pgrun","pki","", {AutoCommit => 1, RaiseError=>1, PrintError=>0, Warn=>0}) or die $DBI::errstr;
	my 	$sth=$dbh->prepare("CREATE TABLE certificate (serial BIGINT PRIMARY KEY, dn TEXT NOT NULL, email TEXT NOT NULL, expiration TEXT NOT NULL, revocation TEXT, status TEXT NOT NULL, profile TEXT, pem TEXT)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE INDEX certificate_status ON certificate(status)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE INDEX certificate_dn ON certificate(dn)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE INDEX certificate_email ON certificate(email)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE INDEX cert_profile ON certificate(profile)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE INDEX cert_serial ON certificate(serial)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE SEQUENCE cert_serial_seq START 1");
	$sth->execute();
	$sth=$dbh->prepare("CREATE TABLE logs (id BIGINT PRIMARY KEY, ts TIMESTAMP, action TEXT NOT NULL, actor TEXT NOT NULL, entity TEXT NOT NULL, outcome TEXT NOT NULL, comment TEXT, signature TEXT)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE INDEX logs_action ON logs(action)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE INDEX logs_outcome ON logs(outcome)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE INDEX logs_ts ON logs(ts)");
	$sth->execute();
	$sth=$dbh->prepare("CREATE SEQUENCE logs_serial_seq START 1");
	$sth->execute();	
	$dbh->disconnect();
}


1;
