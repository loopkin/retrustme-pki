#!/usr/bin/perl

## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
#
# $Id: installcacert.pl 368 2015-10-14 20:47:29Z loopkin $
#

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/installcacert.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NSConfig;
use NikoSoft::PKI::CryptoTools qw(exec_openssl);

sub print_usage {
		print <<__USG__;
Usage: installcacert.pl <ca.crt> 

	WARNING: use with caution!

	Install the CA certificate contained in <ca.crt> as the new CA certificate. To be used when the CA was signed from an external CA. 
	
__USG__
}

if (($ARGV[0] eq '--help') || ($ARGV[0] eq '')) {
	print_usage();
	exit;
}

my $cafile=$ARGV[0];

unless ( -f "$cafile") {
	print_usage();
	exit;
}

my $parsed=exec_openssl(1,"x509 -text -in \"$cafile\" -noout -certopt ca_default,no_sigdump,no_serial,no_subject,no_validity,no_signame");

if ($parsed =~ m/CA:TRUE/) {
	system("cp \"$cafile\" \"$CAPATH/ca.crt\"");
	system("cp \"$cafile\" \"$CAPUBLICPATH/ca.crt\"");
	print "Copied new CA certificate.\n";
	exit;
} else {
	print "Provided file is not a CA certificate. Exiting.\n";
	exit;
}