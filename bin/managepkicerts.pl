#!/usr/bin/perl

# 
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
#
# $Id: managepkicerts.pl 239 2014-11-11 17:37:03Z loopkin $
#

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/managepkicerts.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NikoSoft::I18N::en;
use NSConfig;
use NikoSoft::PKI::InternalEntities;

sub print_usage {
		print <<__USG__;
Usage: managepkicerts.pl [ --help ] [ --tsa [ tsa_oid ] ]

(Re) generates the default Apache web server certificates. Works only on RedHat/CentOS.

	--help : displays this help message
	--tsa : initializes as well the included TimeStamp Authority, with optional tsa_oid TimeStamping Policy OID
	
__USG__
}

unless (-f "/etc/redhat-release") {
	print_usage();
	exit;
}

if ($ARGV[0] eq '--help') {
	print_usage();
	exit;
}

my $tsa_oid='';
if ($ARGV[0] eq '--tsa') {
	$tsa_oid=$ARGV[1]||'1.3.6.1.4.1.4146.2.2';
}

my $entities=NikoSoft::PKI::InternalEntities->new();

$entities->gen_internal_entities($tsa_oid,1);

print "\nNow restarting apache.\n";
system("service httpd restart");
system("chown $APACHEUSER $CAPATH/*");
system("chown $APACHEUSER $CAPATH/private/*");

exit;