#!/usr/bin/perl
# 
## (c) 2010-2018 Alexandre Aufrere - http://ReTrust.me <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
#
# $Id: manageconf.pl 425 2018-01-18 00:21:58Z loopkin $
#

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/manageconf.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NikoSoft::I18N::en;
use NSConfig;
use NikoSoft::PKI::Config;

sub print_usage {
		print <<__USG__;
Usage: manageconf.pl [ --help ] [ --dump ] [ --set name value ]

Manages the configuration at low level

	--help : displays this help message


__USG__
}


my $config=NikoSoft::PKI::Config->new();

if ($ARGV[0] eq "--dump") {
	my $conf=$config->data();
	my @names = sort( keys (%{$conf}));
	foreach my $name (@names) {
		print $name.'='.$conf->{$name}."\n";
	}
} elsif ($ARGV[0] eq "--set") {
	my $name = $ARGV[1];
	my $value = $ARGV[2];
	$config->set_data($name, $value, '');
	$config->save();
	system("chown $APACHEUSER $CAPATH/*.sqlite");	
} else {
	print_usage();
}

exit 0;
