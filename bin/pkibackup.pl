#!/usr/bin/perl


## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
#
# $Id: pkibackup.pl 368 2015-10-14 20:47:29Z loopkin $
#

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/pkibackup.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NSConfig;
use NikoSoft::PKI::Utils qw(utf8_print time_to_asn1_time mylock myunlock);

sub print_usage {
		print <<__USG__;
Usage: pkibackup.pl [--secure] 

	--secure: backs up also PKI secrets. if omitted, only public information is backed up

	This script returns the full path to the tar.gz file containing the backup of the PKI.
	
__USG__
}

my $secure=0;
$secure=1 if ($ARGV[0] eq '--secure');
if ($ARGV[0] eq '--help') {
	print_usage();
	exit;
}

my $filename="backup_".time_to_asn1_time(gmtime(time));

if ($TYPE eq 'enterprise') {
	# PGSQL in enterprise mode only
	my $pgrun="$INSTALLDIR/var/dbrun";
	system("/usr/bin/pg_dump -h '$pgrun' -U pki pkica > $TMPDIR/$filename.pki.sql");
} else {
	system("/usr/bin/sqlite3 $CAPATH/pkidb.sqlite .dump > $TMPDIR/$filename.pki.sql");
}
system("/usr/bin/sqlite3 $CAPATH/pkiconfdb.sqlite .dump > $TMPDIR/$filename.pkiconf.sql");

my $files="$TMPDIR/$filename.pki.sql $TMPDIR/$filename.pkiconf.sql $CAPATH/ca.crt $CAPATH/ca.csr $CAPATH/ca.crl $CAPATH/crlnumber $CAPATH/sign.crt $CAPATH/tsa.crt $CAPATH/tsaserial $CAPATH/scep.crt $CAPATH/web.crt";
$files.=" $CAPATH/private/ca.key $CAPATH/private/sign.key $CAPATH/private/tsa.key $CAPATH/private/scep.key $CAPATH/private/web.key $APACHECONF/pkiusers" if ($secure);
$files.=" $CAPATH/logdb.sqlite" unless ($TYPE eq 'enterprise');

system("tar czf $TMPDIR/$filename.tar.gz $files 2>/dev/null");

unlink("$TMPDIR/$filename.pki.sql");
unlink("$TMPDIR/$filename.pkiconf.sql");

print "$TMPDIR/$filename.tar.gz\n";