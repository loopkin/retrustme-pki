#!/usr/bin/perl

## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## - Software is provided "AS IS", without any warranty whatsoever
## - It is explicitely forbidden to use, distribute, copy, reproduce, redistribute or give without 
## explicit written consent of the author
## - Such consent could lead to agree to the terms of a separate license agreement.
##
#
# $Id: services.pl 394 2017-04-12 08:27:45Z loopkin $
#

=head1 HELP

launch services.pl --help for more information

Manages startup/shutdown of Apache and PostgreSQL, part of "Enterprise" version

=cut

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/services.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NikoSoft::Service::PostgreSQL;
use NikoSoft::PKI::InternalEntities;

sub print_usage {
		print <<__USG__;
Usage: services.pl start stop status [verbose]

__USG__
}


my $command = $ARGV[0];
my $verbose = $ARGV[1];

unless ($command) { print_usage; exit; }

print "\n" if ($verbose);

sub _service_check {
	my ($service, $name)=@_;
	my $ret=0;
	system("/sbin/service $service status >/dev/null 2>&1");
	if ($?==0) {
		print "$name:\t\tRunning.\n" if ($verbose);
		$ret=1;
	} else {
		print "$name:\t\tNot Running.\n" if ($verbose);
		$ret=0;
	}
	return $ret;
}

sub _service_start {
	my ($service, $name)=@_;
	my $ret=0;
	system("/sbin/service $service start >/dev/null 2>&1");
	if ($?==0) {
		print "Starting $name:\t\tSuccess.\n" if ($verbose);
		$ret=1;
	} else {
		print "Starting $name:\t\tFailure.\n" if ($verbose);
		$ret=0;
	}
	return $ret;	 
}

sub _service_stop {
	my ($service, $name)=@_;
	my $ret=0;
	system("/sbin/service $service stop >/dev/null 2>&1");
	if ($?==0) {
		print "Stopping $name:\t\tSuccess.\n" if ($verbose);
		$ret=1;
	} else {
		print "Stopping $name:\t\tFailure.\n" if ($verbose);
		$ret=0;
	}
	return $ret;	 
}

my $pgservice=NikoSoft::Service::PostgreSQL->new();

my $ret=0;
if ($command eq 'start') {
	$ret=$pgservice->start();
	if ($ret) {
		print "Starting PostgreSQL DB (postgres):\t\tSuccess.\n" if ($verbose);
		my $intentities=NikoSoft::PKI::InternalEntities->new();
		$intentities->copy_www_certificate();
		$ret=_service_start('httpd','Apache Web Server (httpd)');
	} else {
		print "Starting PostgreSQL DB (postgres):\t\tFailure.\n" if ($verbose);		
	}
}

if ($command eq 'stop') {
	$ret=_service_stop('httpd','Apache Web Server (httpd)');
	if ($ret) {
		print "Stopping PostgreSQL DB (postgres):\t\tSuccess.\n" if ($verbose);
		$ret=$pgservice->stop();
	} else {
		print "Stopping PostgreSQL DB (postgres):\t\tFailure.\n" if ($verbose);		
	}
}

if ($command eq 'status') {
	if ($pgservice->status()) {
		print "PostgreSQL DB (postgres):\t\tRunning.\n" if ($verbose);
		$ret=1;
	} else {
		print "PostgreSQL DB (postgres):\t\tNot Running.\n" if ($verbose);
		$ret=0;
	}
	$ret+=_service_check('httpd','Apache Web Server (httpd)');
	$ret+=_service_check('ntpd','Network Time Daemon (ntpd)');
	$ret+=_service_check('crond','Periodic tasks  (crond)');
	$ret = 0 if ($ret != 4);
	print "All services are running.\n" if ($ret);
}

if ($command eq '--help') {
	$ret=1;
	print_usage();
}

$ret=!$ret;
exit $ret;
