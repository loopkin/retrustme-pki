#!/usr/bin/perl

## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
#
# $Id: cronjob.pl 368 2015-10-14 20:47:29Z loopkin $
#

=head1 HELP

launch cronjob.pl --help for more information

Manages cron tasks of the PKI

=cut

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/cronjob.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NSConfig;
use NikoSoft::PKI::CA;
use NikoSoft::PKI::CADB;
use NikoSoft::PKI::Config;
use NikoSoft::PKI::Log;
use NikoSoft::PKI::SCEP;
use NikoSoft::PKI::ACME;


sub print_usage {
		print <<__USG__;
Usage: cronjob.pl --daily | --hourly

__USG__
}

if ((scalar(@ARGV)>0) && ($ARGV[0] eq '--daily')) {
	if ($ENV{USER} eq 'root') {
		print "Relaunching command as $APACHEUSER user\n";
		system("su - $APACHEUSER -s /bin/bash -c \"$INSTALLDIR/bin/cronjob.pl --daily\"");
		exit;
	}
	my $config=NikoSoft::PKI::Config->new();
	exit unless ($config->data('dailycron'));
	my $log=NikoSoft::PKI::Log->new();
	my $ca=NikoSoft::PKI::CA->new($config,$log);
	$ca->set_output_mode(1);
	$ca->initiate_ops();
	$ca->generate_crl();
	system("rm -f $CAPUBLICPATH/*.p12");
	system("rm -f $TMPDIR/*.crt $TMPDIR/*.csr $TMPDIR/*.spr $TMPDIR/*.key $TMPDIR/*.gz $TMPDIR/*.p12");
	$ca->expiration_notice_and_flag();
	NikoSoft::PKI::CADB::Optimize();
  my $acme=NikoSoft::PKI::ACME->new($config,$log);
  $acme->change_nonce();
  my $scep=NikoSoft::PKI::SCEP->new($config,$log);
	$scep->change_challenge() unless ($config->data('static_scep'));
	# In case of cert auth, we need to copy CRL at the right place (working mostly with RHEL)
	if ($AUTH_MODE eq 'CERT') {
		system("/bin/cp $CAPATH/ca.crl /etc/pki/tls/certs/pki.crl >/dev/null 2>&1") if (-d "/etc/pki/tls/certs");
	}
	exit;
}

if ((scalar(@ARGV)>0) && ($ARGV[0] eq '--hourly')) {
	# In case of cert auth, we need to copy CRL at the right place (working mostly with RHEL)
	if ($AUTH_MODE eq 'CERT') {
		system("/bin/cp $CAPATH/ca.crl /etc/pki/tls/certs/pki.crl >/dev/null 2>&1") if (-d "/etc/pki/tls/certs");
	}
	my $config=NikoSoft::PKI::Config->new();
	exit if ($config->data('crlonrevoke'));
	my $log=NikoSoft::PKI::Log->new();
	my $ca=NikoSoft::PKI::CA->new($config,$log);
	$ca->set_output_mode(1);
	$ca->initiate_ops();
	$ca->generate_crl();
	exit;
}

print_usage();
exit;
