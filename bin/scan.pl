#!/usr/bin/perl

## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
#
# $Id: scan.pl 406 2017-04-29 23:07:09Z loopkin $
#

=head1 HELP

launch scan.pl --help for more information

scans the network for unmanaged certificates

=cut

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/scan.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NSConfig;
use NikoSoft::PKI::Utils qw(sendmail);
use NikoSoft::PKI::Config;

my $mail=$ARGV[0];
my $iprange=$ARGV[1];

sub print_usage {
                print <<__USG__;
Usage: scan.pl email ip_range

Scans ip_range for untrusted certificates on port 443, and emails the result to email

email:    email address
ip_range: IP Range in 192.168.0.0/24 form

__USG__
}

if (($mail eq '--help')||($mail!~m/\@/)||(!($iprange=~m/[0-9\.\/]/))) {
	print_usage();
	exit;
}

sub get_cert {                                                                                                                              
	my ($ip)=@_;                                                                                                                        
	my $cert=`echo "Q" | openssl s_client -host $ip -port 443 2>/dev/null | openssl x509 2>/dev/null`;                                  
	return $cert;                                                                                                                       
}                                                                                                                                           
                                                                                                                                            
sub verify_cert {
	my ($cert)=@_;
	my $ret=system("echo \"$cert\" | openssl verify -CAfile $TMPDIR/ca_crl.pem -crl_check >/dev/null 2>&1");
	return $ret;
}                                                                                                                                           

sub print_cert {
	my ($cert)=@_;
	my $print_cert=`echo "$cert" | openssl x509 -subject -issuer -noout`;
	$print_cert=~s/\n/ /g;
	return $print_cert;
}

my $rnd='';
for (my $i=0;$i<4;$i++) {
        $rnd.=''.int(rand(9));
}
# time seed...
my @time=localtime();
my $sec=$time[0];
my $filename="nmap_tmp_$rnd$sec.txt";

# First, the NMAP port scanning
system("/usr/bin/nmap -sV -n -p 443 -oG $TMPDIR/$filename --open $iprange >/dev/null 2>&1");

# Then, building the PEM file for checking CA and non revocation
system("cp $CAPATH/ca.crt $TMPDIR/ca_crl.pem");
system("cat $CAPATH/ca.crl >>$TMPDIR/ca_crl.pem");

# Finally inspect the resulkt of NMAP port scanning
open(IN, "$TMPDIR/$filename");
my @scans=<IN>;
close IN;
unlink("$TMPDIR/$filename");
my $csv='"IP";"STATUS";"DETAILS"'."\n";
for (my $i=1; $i<$#scans; $i=$i+2) {
	#Host: 192.168.0.18 ()   Status: Up
	my $ip=$scans[$i];
	$ip=~s/^Host\:\ ([0-9\.]+)\ .*/$1/;
	$ip=~s/\n//;
	my $cert=get_cert($ip);
	if ($cert) {
		my $pr_crt=print_cert($cert);
		my $vrfy_crt=verify_cert($cert);
		if ($vrfy_crt>0) {
			$csv.="\"$ip\";\"CERTIFICATE INVALID\";\"$vrfy_crt $pr_crt\"\n";
		} else {
			$csv.="\"$ip\";\"CERTIFICATE OK\";\"$pr_crt\"\n";
		}
	} else {
		$csv.="\"$ip\";\"NO CERTIFICATE\";\"\"\n";
	}
}

my $config=NikoSoft::PKI::Config->new();
sendmail($mail,"Your scan results for $iprange",$csv,$config);

exit;
