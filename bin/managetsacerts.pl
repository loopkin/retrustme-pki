#!/usr/bin/perl

# 
## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
#
# $Id: managetsacerts.pl 379 2015-12-27 17:36:01Z loopkin $
#

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/managetsacerts.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NikoSoft::I18N::en;
use NSConfig;
use NikoSoft::PKI::TSA;
use NikoSoft::PKI::Utils qw(str2file);
use NikoSoft::PKI::Config;

sub print_usage {
		print <<__USG__;
Usage: managetsacerts.pl [ --help ] [ --genkeytsa [ tsa_oid ] ] [ --importtsa tsa_cert_file ]

(Re) generates the TSA key and certificates.

	--help : displays this help message
	--genkeytsa : initializes the TimeStamp Authority KeyPair, with optional tsa_oid TimeStamping Policy OID. Returns CSR.
	--importtsa : imports TSA certificate from tsa_cert_file

WARNING: --genkeytsa will always generate a new key pair

__USG__
}

unless (-f "/etc/redhat-release") {
	print_usage();
	exit;
}

if ($ARGV[0] eq '--help') {
	print_usage();
	exit;
}

my $tsa_oid='';
if ($ARGV[0] eq '--genkeytsa') {
	$tsa_oid=$ARGV[1]||'1.3.6.1.4.1.4146.2.2';
	my $config=NikoSoft::PKI::Config->new();
	my $tsa=NikoSoft::PKI::TSA->new($config);
	$tsa->initiate_ops() or exit();
	my $csr=$tsa->gen_tsa_key_csr($tsa_oid, 1);
	str2file('/tmp/tsa.csr',$csr);
	print "Copied TSA CSR in /tmp/tsa.csr";
} elsif ($ARGV[0] eq '--importtsa') {
	my $tsafile=$ARGV[1];
	system("cp \"$tsafile\" \"$CAPATH/tsa.crt\"");
	print "Copied new TSA certificate.\n";

} else {
	print_usage();
}
exit;
