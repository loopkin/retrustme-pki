#!/usr/bin/perl

## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## - Software is provided "AS IS", without any warranty whatsoever
## - It is explicitely forbidden to use, distribute, copy, reproduce, redistribute or give without 
## explicit written consent of the author
## - Such consent could lead to agree to the terms of a license agreement.
##
#
# $Id: support-tools.pl 366 2015-10-09 19:15:14Z loopkin $
#

=head1 HELP

launch support-tools.pl --help for more information

Manages support tools, part of "Enterprise" version

=cut

BEGIN {
	use Cwd qw(abs_path);
	our $NSPATH=abs_path($0);
	$NSPATH=~s/support-tools.pl//;
	$NSPATH=~s/bin\///;
	push @INC, "$NSPATH/lib";
	push @INC, "$NSPATH/etc";
	our $LANG="en";
}

use strict;
use NSConfig;
use NikoSoft::PKI::Utils qw(time_to_asn1_time);

sub print_usage {
		print <<__USG__;
Usage: support-tools.pl [ --gentar |  --help ];

	--gentar: generates a tarball to send to support team, containing logs and other relevant PUBLIC information.

__USG__
}


my $command = $ARGV[0];

unless ($command) { print_usage; exit; }

my $ret=0;
if ($command eq '--gentar') {
	my $filename="support_tools_".time_to_asn1_time(gmtime(time));
	my $files="$CAPATH/ca.crt $CAPATH/ca.crl $CAPATH/sign.crt $CAPATH/tsa.crt $CAPATH/scep.crt $CAPATH/pki.log /var/log/httpd/error_log /var/log/httpd/access_log";
	system("tar czf $TMPDIR/$filename.tar.gz $files 2>/dev/null");
	print "Please send to the support team the following file: $TMPDIR/$filename.tar.gz\n";
	exit;
}

print_usage;
exit;