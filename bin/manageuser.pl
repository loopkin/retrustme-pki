#!/usr/bin/perl

## (c) 2010-2014 Alexandre Aufrere <loopkin@nikosoft.net>
## Published under the terms of GPL version 3 (as of 29 June 2007)
## see http://www.gnu.org/licenses/gpl.html
#
# $Id: manageuser.pl 224 2014-11-02 16:18:26Z loopkin $
#

=head1 NAME

manageuser.pl

=cut

BEGIN {
	use Cwd qw(abs_path);
	our $path=abs_path($0);
	$path=~s/manageuser.pl//;
	$path=~s/bin//;
}

use lib ("$path/etc","$path/lib");

use strict;
use NSConfig;
use NikoSoft::PKI::CryptoTools qw (get_random);
use NikoSoft::PKI::Utils qw (validate_data);

=head1 DESCRIPTION

Usage: manageuser.pl add|remove|modify|list username

	add: adds a NikoSoft PKI user with 'username' and 'password'
	remove: removes the NikoSoft PKI user 'username'
	modify: change 'password' of user 'username'
	list:	list users

=cut

sub print_usage {
		print <<__USG__;
Usage: manageuser.pl add|remove|modify|list username

	add: adds a NikoSoft PKI user with 'username' and 'password'
	remove: removes the NikoSoft PKI user 'username'
	modify: change 'password' of user 'username'
	list:	list users

__USG__
}

sub crypt_password {
	my $user=shift;
	my $pwd=shift;
	# my $mdpwd=`echo -n "sa1ted12$pwd" | $OPENSSL dgst -sha1 -binary | $OPENSSL base64 -nopad`;
	# $mdpwd=~s/[^\ ]*\ ([a-f0-9]+)$/$1/;
	my $random=get_random(6);
	my $mdpwd=crypt($pwd,"\$6\$$random\$");
	$mdpwd=~s/\n//g;
	return $mdpwd."\n";
}

sub list_users {
	my @users=();
	open(IN, "$APACHECONF/pkiusers");
	foreach my $user (<IN>)  {
		$user=~s/^([a-z0-9]+):.*/$1/;
		$user=~s/\n//g;
		push @users,$user;
	}
	close IN;
	return @users;
}

sub remove_user {
	my $username=shift;
	open(IN, "$APACHECONF/pkiusers");
	my @inusers=<IN>;
	close IN;	
	open(OUT, ">$APACHECONF/pkiusers");
	foreach my $user (@inusers)  {
		print OUT $user unless ($user=~m/^$username/);
	}
	close OUT;
}

if (scalar(@ARGV)<1) {
	print_usage();
	exit;
}

my $action=$ARGV[0];
my $username=$ARGV[1];
my $password='';

if (($action eq 'add') || ($action eq 'modify')) {
	print "Enter new password: ";
	$password=readline(*STDIN);
	$password=~s/\n//g;
	unless (validate_data($password,'password')) {
		print "Password too weak or too short: at least 6 characters (including both letters and numbers) are needed.\n";
		exit;
	}
}

if ($action eq 'add') {
	if (!$username) { print_usage(); exit; }
	if (!$password) { print_usage(); exit; }
	if (grep(/$username/,list_users)) { print "User $username already exists\n"; exit; }
	my $mdpwd=crypt_password($username,$password);
	open(OUT, ">>$APACHECONF/pkiusers");
	print OUT "$username:$mdpwd";
	close OUT;
	if ($username eq 'admin') {
		$mdpwd=~s/\n//g;
		system("/usr/sbin/useradd -p '$mdpwd' admin");
	}
	exit;
}

if ($action eq 'modify') {
	if (!$username) { print_usage(); exit; }
	if (!$password) { print_usage(); exit; }
	if (!grep(/$username/,list_users)) { print "User $username does not exist\n"; exit; }
	remove_user($username);
	my $mdpwd=crypt_password($username,$password);
	open(OUT, ">>$APACHECONF/pkiusers");
	print OUT "$username:$mdpwd";
	close OUT;
	if ($username eq 'admin') {
		$mdpwd=~s/\n//g;
		system("/usr/sbin/usermod -p '$mdpwd' admin");
	}
	exit;
}

if ($action eq 'remove') {
	if (!$username) { print_usage(); exit; }
	if ($username eq 'admin') {
		print "Removal of user 'admin' is forbidden.\n";
		exit;
	}
	remove_user($username);
	exit;
}

if ($action eq 'list') {
	foreach (list_users) {
		print $_."\n";
	}	
	exit;
}

print_usage();